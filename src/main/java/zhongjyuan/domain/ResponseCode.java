package zhongjyuan.domain;

/**
 * @className: ResponseCode
 * @description: 响应编码. 实现{@link IResponseCode}
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午12:06:32
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum ResponseCode implements IResponseCode, IEnum {

	/**
	 * 系统异常[0x00]
	 */
	ERROR(0x00, "系统异常."),

	/**
	 * 操作成功[0x01]
	 */
	SUCCEED(0x01, "操作成功."),

	/**
	 * 操作失败[0x02]
	 */
	FAILURE(0x02, "操作失败."),

	/**
	 * 未知错误[0x03]
	 */
	UNKONWN(0x03, "未知错误."),

	/**
	 * 超时[0x04]
	 */
	TIMEOUT(0x04, "超时异常."),

	/**
	 * 无效对象[0x05]
	 */
	DISABLE(0x05, "无效对象."),

	/**
	 * 过期对象[0x06]
	 */
	EXPIRED(0x06, "过期对象."),

	/**
	 * 维护中[0x07]
	 */
	MAINTAIN(0x07, "维护中..."),

	/**
	 * 对象不存在[0x08]
	 */
	NOT_FOUND(0x08, "对象不存在."),

	/**
	 * 参数错误[0x09]
	 */
	PARAM_ERROR(0x09, "参数错误."),

	/**
	 * 对象不可用[0x0A]
	 */
	UNAVAILABLE(0x0A, "对象不可用."),

	/**
	 * IP限制[0x0B]
	 */
	IP_NOT_ALLOW(0x0B, "IP限制."),

	/**
	 * 无访问权限[0x0C]
	 */
	UNAUTHORIZED(0x0C, "无访问权限."),

	/**
	 * 参数错误[0x0D]
	 */
	PARAMETER_ERROR(0x0D, "参数错误."),

	/**
	 * 上传失败[0x0E]
	 */
	UPLOAD_ERRPR(0x0E, "上传失败."),

	/**
	 * 上传失败,文件过大[0x0F]
	 */
	UPLOAD_SIZE_ERROR(0x0F, "上传失败,文件过大."),

	/**
	 * 资源不存在[0x10]
	 */
	RESOURCE_NOT_FOUND(0x10, "资源不存在."),

	/**
	 * 错误的Method-Ttype[0x11]
	 */
	METHOD_ERROR(0x11, "错误的Method-Ttype。"),

	/**
	 * 错误的Content-Type[0x12]
	 */
	MEDIA_TYPE_ERROR(0x12, "错误的Content-Type。"),

	/**
	 * 唯一约束冲突[0x13]
	 */
	DUPLICATE_KEY_ERROR(0x13, "唯一约束冲突:{0},请检查数据."),

	/**
	 * 服务异常[0x14]
	 */
	SERVER_ERROR(0x14, "服务异常."),

	/**
	 * 服务维护中[0x15]
	 */
	SERVER_MAINTAIN(0x15, "服务维护中..."),

	/**
	 * 服务不存在[0x16]
	 */
	SERVER_NOT_FOUND(0x16, "服务不存在."),

	/**
	 * 服务不可用[0x17]
	 */
	SERVER_UNAVAILABLE(0x17, "服务不可用."),

	/**
	 * 服务无访问权限[0x18]
	 */
	SERVER_NOT_PERMISSION(0x18, "服务无访问权限."),

	/**
	 * 密码错误[0x19]
	 */
	PASSWORD_WRONG(0x19, "密码错误."),

	/**
	 * 密码过期[0x1A]
	 */
	PASSWORD_EXPIRE(0x1A, "密码过期."),

	/**
	 * 密码修改[0x1B]
	 */
	PASSWORD_MODIFY(0x1B, "密码修改."),

	;

	final Long code;

	final String message;

	ResponseCode(int code, String message) {
		this.code = Long.valueOf(code);
		this.message = message;
	}

	@Override
	public Long getCode() {
		return this.code;
	}

	@Override
	public String getMessage() {
		return this.message;
	}
}
