package zhongjyuan.domain;

/**
 * @className: IModel
 * @description: 模型对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:56:03
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IModel extends zhongjyuan {

}
