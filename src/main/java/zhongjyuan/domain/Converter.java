package zhongjyuan.domain;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

import zhongjyuan.domain.utils.EntityUtil;

/**
 * @className: Converter
 * @description: 转换对象. 实现{@link ITool}
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午12:04:03
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Converter implements ITool {

	private static final long serialVersionUID = 1L;

	public static <T extends IEntity, V extends IModel> T createVoToEntity(V createVO, Supplier<T> target) {
		T entity = target.get();
		try {
			/*
			 * 对象转换：[注意] 实体处理放在第一位
			 */
			EntityUtil.setCreation(entity);

			copyProperties(entity, createVO);

			/*
			 * 特殊属性处理: 1、子级 2、关联 3、......
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return entity;
	}

	public static <T extends IEntity, V extends IModel> T updateVoToEntity(V updateVO, Supplier<T> target) {
		T entity = target.get();
		try {
			/*
			 * 对象转换：[注意] 实体处理放在第一位
			 */
			EntityUtil.setEdition(entity);

			copyProperties(entity, updateVO);

			/*
			 * 特殊属性处理: 1、子级 2、关联 3、......
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return entity;
	}

	public static <T extends IEntity, V extends IModel> V entityToVo(T entity, Supplier<V> target) {
		V vo = target.get();
		try {
			/*
			 * 对象转换：[注意] 实体处理放在第一位
			 */
			copyProperties(vo, entity);

			/*
			 * 特殊属性处理: 1、子级 2、关联 3、......
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return vo;
	}

	public static <T extends IEntity, V extends IModel> List<V> entityToVos(List<T> entities, Supplier<V> target) {
		List<V> vos = new ArrayList<>();

		try {
			/*
			 * 对象转换: [注意] 实体处理放在第一位
			 */
			for (T entity : entities) {
				V vo = target.get();

				copyProperties(vo, entity);

				/*
				 * 特殊属性处理: 1、子级 2、关联 3、......
				 */

				vos.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vos;
	}

	public static <T extends IEntity, V extends IModel> V entityToBriefVo(T entity, Supplier<V> target) {
		V vo = target.get();
		try {
			/*
			 * 对象转换: [注意] 实体处理放在第一位
			 */
			copyProperties(vo, entity);

			/*
			 * 特殊属性处理: 1、子级 2、关联 3、......
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return vo;
	}

	public static <T extends IEntity, V extends IModel> List<V> entityToBriefVos(List<T> entities, Supplier<V> target) {
		List<V> vos = new ArrayList<>();

		try {
			/*
			 * 对象转换: [注意] 实体处理放在第一位
			 */
			for (T entity : entities) {
				V vo = target.get();

				copyProperties(vo, entity);

				/*
				 * 特殊属性处理: 1、子级 2、关联 3、......
				 */

				vos.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vos;
	}

	public static <T extends IEntity, V extends IModel> List<V> entityToPageVos(List<T> entities, Supplier<V> target) {
		List<V> vos = new ArrayList<>();

		try {
			/*
			 * 对象转换: [注意] 实体处理放在第一位
			 */
			for (T entity : entities) {
				V vo = target.get();

				copyProperties(vo, entity);

				/*
				 * 特殊属性处理: 1、子级 2、关联 3、......
				 */

				vos.add(vo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vos;
	}

	public static void copyProperties(Object destination, Object source) {
		try {
			BeanUtils.copyProperties(destination, source);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Object copyProperties(Object destination, Object... sources) throws IllegalAccessException, InvocationTargetException {
		for (Object source : sources) {
			copyProperties(destination, source);
		}
		return destination;
	}

	public static <T> T copyProperties(Class<T> destinationClass, Object... sources) throws IllegalAccessException, InvocationTargetException {
		T target = getTargetInstance(destinationClass);
		for (Object source : sources) {
			copyProperties(target, source);
		}
		return target;
	}

	public static void copyPropertiesIgnoreNull(Object destination, Object source) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		PropertyDescriptor[] descriptors = PropertyUtils.getPropertyDescriptors(source);
		Arrays.stream(descriptors).filter(descriptor -> PropertyUtils.isReadable(source, descriptor.getName())).filter(descriptor -> PropertyUtils.isWriteable(destination, descriptor.getName())).forEach(descriptor -> {
			try {
				Object value = PropertyUtils.getSimpleProperty(source, descriptor.getName());
				if (value != null) {
					PropertyUtils.setSimpleProperty(destination, descriptor.getName(), value);
				}
			} catch (Exception e) {
				// 处理异常
			}
		});
	}

	public static <T> T copyPropertiesIgnoreNull(Class<T> destinationClass, Object... sources) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		T target = getTargetInstance(destinationClass);
		for (Object source : sources) {
			copyPropertiesIgnoreNull(target, source);
		}
		return target;
	}

	public static <S, T> List<T> copyListProperties(Supplier<T> target, List<S> sources) throws IllegalAccessException, InvocationTargetException {
		if (sources == null) {
			return new ArrayList<T>(0);
		}

		List<T> list = new ArrayList<>(sources.size());
		for (S source : sources) {
			T t = target.get();
			copyProperties(t, source);
			list.add(t);
		}
		return list;
	}

	public static Map<String, Object> convertToMap(Object object) {
		Map<String, Object> result = new LinkedHashMap<String, Object>();

		Class<?> clazz = object.getClass();
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			String fieldName = field.getName();
			Object fieldValue = null;
			try {
				fieldValue = field.get(object);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				fieldValue = null;
			}
			result.put(fieldName, fieldValue);
		}

		return result;
	}

	/**
	 * @title convertToMap
	 * @author zhongjyuan
	 * @description 转换Map[将集合对象某个属性转成Map集]
	 * @param <K> Map键 - 泛型对象
	 * @param <V> Map值 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Map<K,V>
	 * @throws
	 * 
	 *         <pre>
	 *         List<ZhongjyuanDTO> zhongjyuanList = zhongjyuanService.selectList();
	 *         Map<Integer, ZhongjyuanDTO> userIdMap = BeanUtil.convertToMap(zhongjyuanList, "userId");
	 *         </pre>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> convertToMap(List<V> objects, String key) {
		Map<K, V> map = new HashMap<K, V>();

		if (objects == null)
			return map;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}
			field.setAccessible(true);
			for (Object object : objects) {
				map.put((K) field.get(object), (V) object);
			}
		} catch (Exception e) {
			return map;
		}
		return map;
	}

	/**
	 * @title convertToMapList
	 * @author zhongjyuan
	 * @description 转换Map[将集合对象某个属性转成Map集]
	 * @param <K> Map键 - 泛型对象
	 * @param <V> Map值 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Map<K,List<V>>
	 * @throws
	 * 
	 *         <pre>
	 *         List<ShopDTO> shopList = shopService.queryShops();
	 *         Map<Integer, List<ShopDTO>> city2Shops = BeanUtil.convertToMapList(shopList, "cityId");
	 *         </pre>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, List<V>> convertToMapList(List<V> objects, String key) {
		Map<K, List<V>> map = new HashMap<K, List<V>>();

		if (objects == null)
			return map;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}
			field.setAccessible(true);
			for (Object object : objects) {
				K k = (K) field.get(object);
				map.computeIfAbsent(k, k1 -> new ArrayList<>());
				map.get(k).add((V) object);
			}
		} catch (Exception e) {
			return map;
		}
		return map;
	}

	/**
	 * @title convertToSet
	 * @author zhongjyuan
	 * @description 转换Set[将集合对象某个属性转成集合]
	 * @param <K> 集合对象 - 泛型对象
	 * @param objects 对象集合
	 * @param key 对象属性名
	 * @return Set<K>
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public static <K> Set<K> convertToSet(List<?> objects, String key) {
		Set<K> set = new HashSet<K>();

		if (objects == null)
			return set;

		try {
			Class<?> clazz = objects.get(0).getClass();
			Field field = deepFindField(clazz, key);
			if (field == null) {
				throw new IllegalArgumentException("Could not find the key");
			}

			field.setAccessible(true);
			for (Object object : objects) {
				set.add((K) field.get(object));
			}
		} catch (Exception e) {
			return set;
		}
		return set;
	}

	/**
	 * @title setProperty
	 * @author zhongjyuan
	 * @description 设置属性值
	 * @param object 对象
	 * @param fieldName 属性名称
	 * @param value 属性值
	 * @throws
	 */
	public static void setProperty(Object object, String fieldName, Object value) {
		try {
			Field field = deepFindField(object.getClass(), fieldName);
			if (field != null) {
				field.setAccessible(true);
				field.set(object, value);
			}
		} catch (Exception e) {

		}
	}

	/**
	 * @title describeObject
	 * @author zhongjyuan
	 * @description JavaBean 转map<String,Object>
	 * @param <T>
	 * @param bean
	 * @return Map<String,Object>
	 * @throws
	 */
	public static <T> Map<String, Object> describeObject(T bean) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (bean != null) {
			BeanMap beanMap = new BeanMap(bean);
			for (Object key : beanMap.keySet()) {
				map.put(key.toString(), beanMap.get(key));
			}
			//去掉多余属性class
			map.remove("class");
		}
		return map;
	}

	/**
	 * @title mapToJavaBean
	 * @author zhongjyuan
	 * @description Map转JavaBean
	 * @param <T>
	 * @param sourceData
	 * @param targetClass
	 * @return T
	 * @throws
	 */
	public static <T> T mapToJavaBean(Map<String, ? extends Object> sourceData, Class<T> targetClass) {
		T destClasss = getTargetInstance(targetClass);
		try {
			org.apache.commons.beanutils.BeanUtils.populate(destClasss, sourceData);
		} catch (Exception e) {

		}
		return destClasss;
	}

	private static <T> T getTargetInstance(Class<T> targetClass) {
		try {
			return targetClass.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @title deepFindField
	 * @author zhongjyuan
	 * @description 递归查找字段
	 * @param clazz 对象类型
	 * @param key 属性名称
	 * @return Field
	 * @throws
	 */
	private static Field deepFindField(Class<?> clazz, String key) {
		Field field = null;
		while (!clazz.getName().equals(Object.class.getName())) {
			try {
				field = clazz.getDeclaredField(key);
				if (field != null) {
					break;
				}
			} catch (Exception e) {
				clazz = clazz.getSuperclass();
			}
		}
		return field;
	}
}
