package zhongjyuan.domain;

import java.time.LocalDateTime;

public abstract class AbstractTableEntity extends AbstractEntity implements ITableEntity {

	private static final long serialVersionUID = 1L;

	protected Long id;

	protected Long rowIndex;

	protected Boolean isEnabled;

	protected Boolean isDeleted;

	protected String description;

	protected Long creatorId;

	protected String creatorName;

	protected LocalDateTime createTime;

	protected Long updatorId;

	protected String updatorName;

	protected LocalDateTime updateTime;

	public AbstractTableEntity() {

	}

	public AbstractTableEntity(Long id) {
		this.id = id;
	}

	public AbstractTableEntity(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getRowIndex() {
		return this.rowIndex;
	}

	@Override
	public void setRowIndex(Long rowIndex) {
		this.rowIndex = rowIndex;
	}

	@Override
	public Boolean getIsEnabled() {
		return this.isEnabled;
	}

	@Override
	public void setIsEnabled(Boolean enable) {
		this.isEnabled = enable;
	}

	@Override
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	@Override
	public void setIsDeleted(Boolean delete) {
		this.isDeleted = delete;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Long getCreatorId() {
		return this.creatorId;
	}

	@Override
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	@Override
	public String getCreatorName() {
		return this.creatorName;
	}

	@Override
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	@Override
	public LocalDateTime getCreateTime() {
		return this.createTime;
	}

	@Override
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	@Override
	public Long getUpdatorId() {
		return this.updatorId;
	}

	@Override
	public void setUpdatorId(Long updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String getUpdatorName() {
		return this.updatorName;
	}

	@Override
	public void setUpdatorName(String updatorName) {
		this.updatorName = updatorName;
	}

	@Override
	public LocalDateTime getUpdateTime() {
		return this.updateTime;
	}

	@Override
	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}
}
