package zhongjyuan.domain;

/**
 * @className: TenantModel
 * @description: 租户模型的类，扩展了 {@link AbstractAttributeModel} 类并实现了 {@link IAttributeModel} 接口。
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午2:01:38
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TenantModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String code;

	private String name;

	private String type;

	private String domain;

	private Integer rowIndex;

	/**
	 * 获取租户ID
	 * 
	 * @return 租户ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置租户ID
	 * 
	 * @param id 租户ID
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取租户编码
	 * 
	 * @return 租户编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 设置租户编码
	 * 
	 * @param code 租户编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 获取租户名称
	 * 
	 * @return 租户名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置租户名称
	 * 
	 * @param name 租户名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取租户类型
	 * 
	 * @return 租户类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * 设置租户类型
	 * 
	 * @param type 租户类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 获取租户域名
	 * 
	 * @return 租户域名
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * 设置租户域名
	 * 
	 * @param domain 租户域名
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * 获取行索引
	 * 
	 * @return 行索引
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * 设置行索引
	 * 
	 * @param rowIndex 行索引
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
