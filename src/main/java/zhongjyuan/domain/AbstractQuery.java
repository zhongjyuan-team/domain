package zhongjyuan.domain;

/**
 * @className: AbstractQuery
 * @description: 抽象条件类，实现了 {@link IQuery} 接口。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:58:43
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public abstract class AbstractQuery implements IQuery {

	private static final long serialVersionUID = 1L;

}
