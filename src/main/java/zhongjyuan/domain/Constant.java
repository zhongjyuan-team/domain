package zhongjyuan.domain;

/**
 * @className: Constant
 * @description: 常量对象. 实现{@link IConstant}
 * @author: zhongjyuan
 * @date: 2022年10月24日 下午4:36:50
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Constant implements IConstant {

	private static final long serialVersionUID = 1L;

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:51:42
	 * @fields ASC_SORT_TYPE_INT : TODO: 升序
	 */
	public static final String ASC_SORT_TYPE_INT = "0";

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:47:51
	 * @fields ASC_SORT_TYPE : TODO: 升序
	 */
	public static final String ASC_SORT_TYPE = "asc";

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:48:02
	 * @fields ASCENDING_SORT_TYPE : TODO: 升序
	 */
	public static final String ASCENDING_SORT_TYPE = "ascending";

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:52:08
	 * @fields DESC_SORT_TYPE_INT : TODO: 降序
	 */
	public static final String DESC_SORT_TYPE_INT = "1";

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:48:07
	 * @fields DESC_SORT_TYPE : TODO: 降序
	 */
	public static final String DESC_SORT_TYPE = "desc";

	/**
	 * @author zhongjyuan
	 * @data 2021年3月3日下午2:48:13
	 * @fields DESCENDING_SORT_TYPE : TODO: 降序
	 */
	public static final String DESCENDING_SORT_TYPE = "descending";

	/**
	 * @fields UNKNOWN_L: 未知[unknown]
	 */
	public static final String UNKNOWN_L = "unknown";

	/**
	 * @fields UNKNOWN_U: 未知[UNKNOWN]
	 */
	public static final String UNKNOWN_U = "UNKNOWN";

	/**
	 * @fields UNKNOWN_U_A: 未知[Unknown]
	 */
	public static final String UNKNOWN_U_A = "Unknown";

	/**
	 * @fields ANONYMOUS_L: 匿名[anonymous]
	 */
	public static final String ANONYMOUS_L = "anonymous";

	/**
	 * @fields ANONYMOUS_U: 匿名[ANONYMOUS]
	 */
	public static final String ANONYMOUS_U = "ANONYMOUS";

	/**
	 * @fields ANONYMOUS_U_A: 匿名[Anonymous]
	 */
	public static final String ANONYMOUS_U_A = "Anonymous";

	/**
	 * @fields ID_L: 主键[id]
	 */
	public static final String ID_L = "id";

	/**
	 * @fields ID_U: 主键[ID]
	 */
	public static final String ID_U = "ID";

	/**
	 * @fields ID_U_A: 主键[Id]
	 */
	public static final String ID_U_A = "Id";

	/**
	 * @fields NO_L: 编号[no]
	 */
	public static final String NO_L = "no";

	/**
	 * @fields NO_U: 编号[NO]
	 */
	public static final String NO_U = "NO";

	/**
	 * @fields NO_U_A: 编号[No]
	 */
	public static final String NO_U_A = "No";

	/**
	 * @fields CODE_L: 编码[code]
	 */
	public static final String CODE_L = "code";

	/**
	 * @fields CODE_U: 编码[CODE]
	 */
	public static final String CODE_U = "CODE";

	/**
	 * @fields CODE_U_A: 编码[Code]
	 */
	public static final String CODE_U_A = "Code";

	/**
	 * @fields KEY_L: 键[key]
	 */
	public static final String KEY_L = "key";

	/**
	 * @fields KEY_U: 键[KEY]
	 */
	public static final String KEY_U = "KEY";

	/**
	 * @fields KEY_U_A: 键[Key]
	 */
	public static final String KEY_U_A = "Key";

	/**
	 * @fields VALUE_L: 值[value]
	 */
	public static final String VALUE_L = "value";

	/**
	 * @fields VALUE_U: 值[VALUE]
	 */
	public static final String VALUE_U = "VALUE";

	/**
	 * @fields VALUE_U_A: 值[Value]
	 */
	public static final String VALUE_U_A = "Value";

	/**
	 * @fields NAME_L: 名称[name]
	 */
	public static final String NAME_L = "name";

	/**
	 * @fields NAME_U: 名称[NAME]
	 */
	public static final String NAME_U = "NAME";

	/**
	 * @fields NAME_U_A: 名称[Name]
	 */
	public static final String NAME_U_A = "Name";

	/**
	 * @fields ENAME_L: 英文名称[ename]
	 */
	public static final String ENAME_L = "ename";

	/**
	 * @fields ENAME_U: 英文名称[ENAME]
	 */
	public static final String ENAME_U = "ENAME";

	/**
	 * @fields ENAME_U_A: 英文名称[EName]
	 */
	public static final String ENAME_U_A = "EName";

	/**
	 * @fields CNAME_L: 繁体名称[cname]
	 */
	public static final String CNAME_L = "cname";

	/**
	 * @fields CNAME_U: 繁体名称[CNAME]
	 */
	public static final String CNAME_U = "CNAME";

	/**
	 * @fields CNAME_U_A: 繁体名称[CName]
	 */
	public static final String CNAME_U_A = "CName";

	/**
	 * @fields SHORT_NAME_L: 简称[shortname]
	 */
	public static final String SHORT_NAME_L = "shortname";

	/**
	 * @fields SHORT_NAME_L_A: 简称[shortName]
	 */
	public static final String SHORT_NAME_L_A = "shortName";

	/**
	 * @fields SHORT_NAME_HUMP_L_A: 简称[short_name]
	 */
	public static final String SHORT_NAME_HUMP_L_A = "short_name";

	/**
	 * @fields SHORT_NAME_U: 简称[SHORTNAME]
	 */
	public static final String SHORT_NAME_U = "SHORTNAME";

	/**
	 * @fields SHORT_NAME_U_A: 简称[ShortName]
	 */
	public static final String SHORT_NAME_U_A = "ShortName";

	/**
	 * @fields SHORT_NAME_HUMP_U_A: 简称[SHORT_NAME]
	 */
	public static final String SHORT_NAME_HUMP_U_A = "SHORT_NAME";

	/**
	 * @fields ALIAS_NAME_L: 别称[aliasname]
	 */
	public static final String ALIAS_NAME_L = "aliasname";

	/**
	 * @fields ALIAS_NAME_L_A: 别称[aliasName]
	 */
	public static final String ALIAS_NAME_L_A = "aliasName";

	/**
	 * @fields ALIAS_NAME_HUMP_L_A: 别称[alias_name]
	 */
	public static final String ALIAS_NAME_HUMP_L_A = "alias_name";

	/**
	 * @fields ALIAS_NAME_U: 别称[ALIASNAME]
	 */
	public static final String ALIAS_NAME_U = "ALIASNAME";

	/**
	 * @fields ALIAS_NAME_U_A: 别称[AliasName]
	 */
	public static final String ALIAS_NAME_U_A = "AliasName";

	/**
	 * @fields ALIAS_NAME_HUMP_U_A: 别称[ALIAS_NAME]
	 */
	public static final String ALIAS_NAME_HUMP_U_A = "ALIAS_NAME";

	/**
	 * @fields FULL_NAME_L: 全称[fullname]
	 */
	public static final String FULL_NAME_L = "fullname";

	/**
	 * @fields FULL_NAME_L_A: 全称[fullName]
	 */
	public static final String FULL_NAME_L_A = "fullName";

	/**
	 * @fields FULL_NAME_HUMP_L_A: 全称[full_name]
	 */
	public static final String FULL_NAME_HUMP_L_A = "full_name";

	/**
	 * @fields FULL_NAME_U: 全称[FULLNAME]
	 */
	public static final String FULL_NAME_U = "FULLNAME";

	/**
	 * @fields FULL_NAME_U_A: 全称[FullName]
	 */
	public static final String FULL_NAME_U_A = "FullName";

	/**
	 * @fields FULL_NAME_HUMP_U_A: 全称[FULL_NAME]
	 */
	public static final String FULL_NAME_HUMP_U_A = "FULL_NAME";

	/**
	 * @fields ACCOUNT_L: 账号[account]
	 */
	public static final String ACCOUNT_L = "account";

	/**
	 * @fields ACCOUNT_U: 账号[ACCOUNT]
	 */
	public static final String ACCOUNT_U = "ACCOUNT";

	/**
	 * @fields ACCOUNT_U_A: 账号[Account]
	 */
	public static final String ACCOUNT_U_A = "Account";

	/**
	 * @fields SEX_L: 性别[sex]
	 */
	public static final String SEX_L = "sex";

	/**
	 * @fields SEX_U: 性别[SEX]
	 */
	public static final String SEX_U = "SEX";

	/**
	 * @fields SEX_U_A: 性别[Sex]
	 */
	public static final String SEX_U_A = "Sex";

	/**
	 * @fields ICON_L: 头像[icon]
	 */
	public static final String ICON_L = "icon";

	/**
	 * @fields ICON_U: 头像[ICON]
	 */
	public static final String ICON_U = "ICON";

	/**
	 * @fields ICON_U_A: 头像[Icon]
	 */
	public static final String ICON_U_A = "Icon";

	/**
	 * @fields EMAIL_L: 邮箱[email]
	 */
	public static final String EMAIL_L = "email";

	/**
	 * @fields EMAIL_U: 邮箱[EMAIL]
	 */
	public static final String EMAIL_U = "EMAIL";

	/**
	 * @fields EMAIL_U_A: 邮箱[Email]
	 */
	public static final String EMAIL_U_A = "Email";

	/**
	 * @fields MOBBILE_L: 手机号码[mobile]
	 */
	public static final String MOBBILE_L = "mobile";

	/**
	 * @fields MOBBILE_U: 手机号码[MOBILE]
	 */
	public static final String MOBBILE_U = "MOBILE";

	/**
	 * @fields MOBBILE_U_A: 手机号码[Mobile]
	 */
	public static final String MOBBILE_U_A = "Mobile";

	/**
	 * @fields TELEPHONE_L: 电话号码[telephone]
	 */
	public static final String TELEPHONE_L = "telephone";

	/**
	 * @fields TELEPHONE_U: 电话号码[TELEPHONE]
	 */
	public static final String TELEPHONE_U = "TELEPHONE";

	/**
	 * @fields TELEPHONE_U_A: 电话号码[Telephone]
	 */
	public static final String TELEPHONE_U_A = "Telephone";

	/**
	 * @fields TYPE_L: 类型[type]
	 */
	public static final String TYPE_L = "type";

	/**
	 * @fields TYPE_U: 类型[TYPE]
	 */
	public static final String TYPE_U = "TYPE";

	/**
	 * @fields TYPE_U_A: 类型[Type]
	 */
	public static final String TYPE_U_A = "Type";

	/**
	 * @fields GROUP_L: 分组[group]
	 */
	public static final String GROUP_L = "group";

	/**
	 * @fields GROUP_U: 分组[GROUP]
	 */
	public static final String GROUP_U = "GROUP";

	/**
	 * @fields GROUP_U_A: 分组[Group]
	 */
	public static final String GROUP_U_A = "Group";

	/**
	 * @fields PARENT_ID_L: 父级主键[parentid]
	 */
	public static final String PARENT_ID_L = "parentid";

	/**
	 * @fields PARENT_ID_L_A: 父级主键[parentId]
	 */
	public static final String PARENT_ID_L_A = "parentId";

	/**
	 * @fields PARENT_ID_HUMP_L_A: 父级主键[parent_id]
	 */
	public static final String PARENT_ID_HUMP_L_A = "parent_id";

	/**
	 * @fields PARENT_ID_U: 父级主键[PARENTID]
	 */
	public static final String PARENT_ID_U = "PARENTID";

	/**
	 * @fields PARENT_ID_U_A: 父级主键[ParentId]
	 */
	public static final String PARENT_ID_U_A = "ParentId";

	/**
	 * @fields PARENT_ID_HUMP_U_A: 父级主键[PARENT_ID]
	 */
	public static final String PARENT_ID_HUMP_U_A = "PARENT_ID";

	/**
	 * @fields PARENT_NO_L: 父级编号[parentno]
	 */
	public static final String PARENT_NO_L = "parentno";

	/**
	 * @fields PARENT_NO_L_A: 父级编号[parentNo]
	 */
	public static final String PARENT_NO_L_A = "parentNo";

	/**
	 * @fields PARENT_NO_HUMP_L_A: 父级编号[parent_no]
	 */
	public static final String PARENT_NO_HUMP_L_A = "parent_no";

	/**
	 * @fields PARENT_NO_U: 父级编号[PARENTNO]
	 */
	public static final String PARENT_NO_U = "PARENTNO";

	/**
	 * @fields PARENT_NO_U_A: 父级编号[ParentNo]
	 */
	public static final String PARENT_NO_U_A = "ParentNo";

	/**
	 * @fields PARENT_NO_HUMP_U_A: 父级编号[PARENT_NO]
	 */
	public static final String PARENT_NO_HUMP_U_A = "PARENT_NO";

	/**
	 * @fields PARENT_CODE_L: 父级编码[parentcode]
	 */
	public static final String PARENT_CODE_L = "parentcode";

	/**
	 * @fields PARENT_CODE_L_A: 父级编码[parentCode]
	 */
	public static final String PARENT_CODE_L_A = "parentCode";

	/**
	 * @fields PARENT_CODE_HUMP_L_A: 父级编码[parent_code]
	 */
	public static final String PARENT_CODE_HUMP_L_A = "parent_code";

	/**
	 * @fields PARENT_CODE_U: 父级编码[PARENTCODE]
	 */
	public static final String PARENT_CODE_U = "PARENTCODE";

	/**
	 * @fields PARENT_CODE_U_A: 父级编码[ParentCode]
	 */
	public static final String PARENT_CODE_U_A = "ParentCode";

	/**
	 * @fields PARENT_CODE_HUMP_U_A: 父级编码[PARENT_CODE]
	 */
	public static final String PARENT_CODE_HUMP_U_A = "PARENT_CODE";

	/**
	 * @fields PARENT_NAME_L: 父级名称[parentname]
	 */
	public static final String PARENT_NAME_L = "parentname";

	/**
	 * @fields PARENT_NAME_L_A: 父级名称[parentName]
	 */
	public static final String PARENT_NAME_L_A = "parentName";

	/**
	 * @fields PARENT_NAME_HUMP_L_A: 父级名称[parent_name]
	 */
	public static final String PARENT_NAME_HUMP_L_A = "parent_name";

	/**
	 * @fields PARENT_NAME_U: 父级名称[PARENTNAME]
	 */
	public static final String PARENT_NAME_U = "PARENTNAME";

	/**
	 * @fields PARENT_NAME_U_A: 父级名称[ParentName]
	 */
	public static final String PARENT_NAME_U_A = "ParentName";

	/**
	 * @fields PARENT_NAME_HUMP_U_A: 父级名称[PARENT_NAME]
	 */
	public static final String PARENT_NAME_HUMP_U_A = "PARENT_NAME";

	/**
	 * @fields LEVEL_L: 层级码[level]
	 */
	public static final String LEVEL_L = "level";

	/**
	 * @fields LEVEL_U: 层级码[LEVEL]
	 */
	public static final String LEVEL_U = "LEVEL";

	/**
	 * @fields LEVEL_U_A: 层级码[Level]
	 */
	public static final String LEVEL_U_A = "Level";

	/**
	 * @fields PATH_L: 全路径[path]
	 */
	public static final String PATH_L = "path";

	/**
	 * @fields PATH_U: 全路径[PATH]
	 */
	public static final String PATH_U = "PATH";

	/**
	 * @fields PATH_U_A: 全路径[Path]
	 */
	public static final String PATH_U_A = "Path";

	/**
	 * @fields OUTLINE_L: 结构码[outline]
	 */
	public static final String OUTLINE_L = "outline";

	/**
	 * @fields OUTLINE_U: 结构码[OUTLINE]
	 */
	public static final String OUTLINE_U = "OUTLINE";

	/**
	 * @fields OUTLINE_U_A: 结构码[Outline]
	 */
	public static final String OUTLINE_U_A = "Outline";

	/**
	 * @fields IS_LEAF_L: 是否叶子[isleaf]
	 */
	public static final String IS_LEAF_L = "isleaf";

	/**
	 * @fields IS_LEAF_L_A: 是否叶子[isLeaf]
	 */
	public static final String IS_LEAF_L_A = "isLeaf";

	/**
	 * @fields IS_LEAF_HUMP_L_A: 是否叶子[is_leaf]
	 */
	public static final String IS_LEAF_HUMP_L_A = "is_leaf";

	/**
	 * @fields IS_LEAF_U: 是否叶子[ISLEAF]
	 */
	public static final String IS_LEAF_U = "ISLEAF";

	/**
	 * @fields IS_LEAF_U_A: 是否叶子[IsLeaf]
	 */
	public static final String IS_LEAF_U_A = "IsLeaf";

	/**
	 * @fields IS_LEAF_HUMP_U_A: 是否叶子[IS_LEAF]
	 */
	public static final String IS_LEAF_HUMP_U_A = "IS_LEAF";

	/**
	 * @fields CORP_ID_L: 公司主键[corpid]
	 */
	public static final String CORP_ID_L = "corpid";

	/**
	 * @fields CORP_ID_L_A: 公司主键[corpId]
	 */
	public static final String CORP_ID_L_A = "corpId";

	/**
	 * @fields CORP_ID_HUMP_L_A: 公司主键[corp_id]
	 */
	public static final String CORP_ID_HUMP_L_A = "corp_id";

	/**
	 * @fields CORP_ID_U: 公司主键[CORPID]
	 */
	public static final String CORP_ID_U = "CORPID";

	/**
	 * @fields CORP_ID_U_A: 公司主键[CorpId]
	 */
	public static final String CORP_ID_U_A = "CorpId";

	/**
	 * @fields CORP_ID_HUMP_U_A: 公司主键[CORP_ID]
	 */
	public static final String CORP_ID_HUMP_U_A = "CORP_ID";

	/**
	 * @fields CORP_NO_L: 公司编号[corpno]
	 */
	public static final String CORP_NO_L = "corpno";

	/**
	 * @fields CORP_NO_L_A: 公司编号[corpNo]
	 */
	public static final String CORP_NO_L_A = "corpNo";

	/**
	 * @fields CORP_NO_HUMP_L_A: 公司编号[corp_no]
	 */
	public static final String CORP_NO_HUMP_L_A = "corp_no";

	/**
	 * @fields CORP_NO_U: 公司编号[CORPNO]
	 */
	public static final String CORP_NO_U = "CORPNO";

	/**
	 * @fields CORP_NO_U_A: 公司编号[CorpNo]
	 */
	public static final String CORP_NO_U_A = "CorpNo";

	/**
	 * @fields CORP_NO_HUMP_U_A: 公司编号[CORP_NO]
	 */
	public static final String CORP_NO_HUMP_U_A = "CORP_NO";

	/**
	 * @fields CORP_NAME_L: 公司名称[corpname]
	 */
	public static final String CORP_NAME_L = "corpname";

	/**
	 * @fields CORP_NAME_L_A: 公司名称[corpName]
	 */
	public static final String CORP_NAME_L_A = "corpName";

	/**
	 * @fields CORP_NAME_HUMP_L_A: 公司名称[corp_name]
	 */
	public static final String CORP_NAME_HUMP_L_A = "corp_name";

	/**
	 * @fields CORP_NAME_U: 公司名称[CORPNAME]
	 */
	public static final String CORP_NAME_U = "CORPNAME";

	/**
	 * @fields CORP_NAME_U_A: 公司名称[CorpName]
	 */
	public static final String CORP_NAME_U_A = "CorpName";

	/**
	 * @fields CORP_NAME_HUMP_U_A: 公司名称[CORP_NAME]
	 */
	public static final String CORP_NAME_HUMP_U_A = "CORP_NAME";

	/**
	 * @fields DEPT_ID_L: 部门主键[deptid]
	 */
	public static final String DEPT_ID_L = "deptid";

	/**
	 * @fields DEPT_ID_L_A: 部门主键[deptId]
	 */
	public static final String DEPT_ID_L_A = "deptId";

	/**
	 * @fields DEPT_ID_HUMP_L_A: 部门主键[dept_id]
	 */
	public static final String DEPT_ID_HUMP_L_A = "dept_id";

	/**
	 * @fields DEPT_ID_U: 部门主键[DEPTID]
	 */
	public static final String DEPT_ID_U = "DEPTID";

	/**
	 * @fields DEPT_ID_U_A: 部门主键[DeptId]
	 */
	public static final String DEPT_ID_U_A = "DeptId";

	/**
	 * @fields DEPT_ID_HUMP_U_A: 部门主键[DEPT_ID]
	 */
	public static final String DEPT_ID_HUMP_U_A = "DEPT_ID";

	/**
	 * @fields DEPT_NO_L: 部门编号[deptno]
	 */
	public static final String DEPT_NO_L = "deptno";

	/**
	 * @fields DEPT_NO_L_A: 部门编号[deptNo]
	 */
	public static final String DEPT_NO_L_A = "deptNo";

	/**
	 * @fields DEPT_NO_HUMP_L_A: 部门编号[dept_no]
	 */
	public static final String DEPT_NO_HUMP_L_A = "dept_no";

	/**
	 * @fields DEPT_NO_U: 部门编号[DEPTNO]
	 */
	public static final String DEPT_NO_U = "DEPTNO";

	/**
	 * @fields DEPT_NO_U_A: 部门编号[DeptNo]
	 */
	public static final String DEPT_NO_U_A = "DeptNo";

	/**
	 * @fields DEPT_NO_HUMP_U_A: 部门编号[DEPT_NO]
	 */
	public static final String DEPT_NO_HUMP_U_A = "DEPT_NO";

	/**
	 * @fields DEPT_NAME_L: 部门名称[deptname]
	 */
	public static final String DEPT_NAME_L = "deptname";

	/**
	 * @fields DEPT_NAME_L_A: 部门名称[deptName]
	 */
	public static final String DEPT_NAME_L_A = "deptName";

	/**
	 * @fields DEPT_NAME_HUMP_L_A: 部门名称[dept_name]
	 */
	public static final String DEPT_NAME_HUMP_L_A = "dept_name";

	/**
	 * @fields DEPT_NAME_U: 部门名称[DEPTNAME]
	 */
	public static final String DEPT_NAME_U = "DEPTNAME";

	/**
	 * @fields DEPT_NAME_U_A: 部门名称[DeptName]
	 */
	public static final String DEPT_NAME_U_A = "DeptName";

	/**
	 * @fields DEPT_NAME_HUMP_U_A: 部门名称[DEPT_NAME]
	 */
	public static final String DEPT_NAME_HUMP_U_A = "DEPT_NAME";

	/**
	 * @fields POSITION_ID_L: 职业主键[positionid]
	 */
	public static final String POSITION_ID_L = "positionid";

	/**
	 * @fields POSITION_ID_L_A: 职业主键[positionId]
	 */
	public static final String POSITION_ID_L_A = "positionId";

	/**
	 * @fields POSITION_ID_HUMP_L_A: 职业主键[position_id]
	 */
	public static final String POSITION_ID_HUMP_L_A = "position_id";

	/**
	 * @fields POSITION_ID_U: 职业主键[POSITIONID]
	 */
	public static final String POSITION_ID_U = "POSITIONID";

	/**
	 * @fields POSITION_ID_U_A: 职业主键[PositionId]
	 */
	public static final String POSITION_ID_U_A = "PositionId";

	/**
	 * @fields POSITION_ID_HUMP_U_A: 职业主键[POSITION_ID]
	 */
	public static final String POSITION_ID_HUMP_U_A = "POSITION_ID";

	/**
	 * @fields POSITION_NO_L: 职业编号[positionno]
	 */
	public static final String POSITION_NO_L = "positionno";

	/**
	 * @fields POSITION_NO_L_A: 职业编号[positionNo]
	 */
	public static final String POSITION_NO_L_A = "positionNo";

	/**
	 * @fields POSITION_NO_HUMP_L_A: 职业编号[position_no]
	 */
	public static final String POSITION_NO_HUMP_L_A = "position_no";

	/**
	 * @fields POSITION_NO_U: 职业编号[POSITIONNO]
	 */
	public static final String POSITION_NO_U = "POSITIONNO";

	/**
	 * @fields POSITION_NO_U_A: 职业编号[PositionNo]
	 */
	public static final String POSITION_NO_U_A = "PositionNo";

	/**
	 * @fields POSITION_NO_HUMP_U_A: 职业编号[POSITION_NO]
	 */
	public static final String POSITION_NO_HUMP_U_A = "POSITION_NO";

	/**
	 * @fields POSITION_NAME_L: 职业名称[positionname]
	 */
	public static final String POSITION_NAME_L = "positionname";

	/**
	 * @fields POSITION_NAME_L_A: 职业名称[positionName]
	 */
	public static final String POSITION_NAME_L_A = "positionName";

	/**
	 * @fields POSITION_NAME_HUMP_L_A: 职业名称[position_name]
	 */
	public static final String POSITION_NAME_HUMP_L_A = "position_name";

	/**
	 * @fields POSITION_NAME_U: 职业名称[POSITIONNAME]
	 */
	public static final String POSITION_NAME_U = "POSITIONNAME";

	/**
	 * @fields POSITION_NAME_U_A: 职业名称[PositionName]
	 */
	public static final String POSITION_NAME_U_A = "PositionName";

	/**
	 * @fields POSITION_NAME_HUMP_U_A: 职业名称[POSITION_NAME]
	 */
	public static final String POSITION_NAME_HUMP_U_A = "POSITION_NAME";

	/**
	 * @fields STATION_ID_L: 岗位主键[stationid]
	 */
	public static final String STATION_ID_L = "stationid";

	/**
	 * @fields STATION_ID_L_A: 岗位主键[stationId]
	 */
	public static final String STATION_ID_L_A = "stationId";

	/**
	 * @fields STATION_ID_HUMP_L_A: 岗位主键[station_id]
	 */
	public static final String STATION_ID_HUMP_L_A = "station_id";

	/**
	 * @fields STATION_ID_U: 岗位主键[STATIONID]
	 */
	public static final String STATION_ID_U = "STATIONID";

	/**
	 * @fields STATION_ID_U_A: 岗位主键[StationId]
	 */
	public static final String STATION_ID_U_A = "StationId";

	/**
	 * @fields STATION_ID_HUMP_U_A: 岗位主键[STATION_ID]
	 */
	public static final String STATION_ID_HUMP_U_A = "STATION_ID";

	/**
	 * @fields STATION_NO_L: 岗位编号[stationno]
	 */
	public static final String STATION_NO_L = "stationno";

	/**
	 * @fields STATION_NO_L_A: 岗位编号[stationNo]
	 */
	public static final String STATION_NO_L_A = "stationNo";

	/**
	 * @fields STATION_NO_HUMP_L_A: 岗位编号[station_no]
	 */
	public static final String STATION_NO_HUMP_L_A = "station_no";

	/**
	 * @fields STATION_NO_U: 岗位编号[STATIONNO]
	 */
	public static final String STATION_NO_U = "STATIONNO";

	/**
	 * @fields STATION_NO_U_A: 岗位编号[StationNo]
	 */
	public static final String STATION_NO_U_A = "StationNo";

	/**
	 * @fields STATION_NO_HUMP_U_A: 岗位编号[STATION_NO]
	 */
	public static final String STATION_NO_HUMP_U_A = "STATION_NO";

	/**
	 * @fields STATION_NAME_L: 岗位名称[stationname]
	 */
	public static final String STATION_NAME_L = "stationname";

	/**
	 * @fields STATION_NAME_L_A: 岗位名称[stationName]
	 */
	public static final String STATION_NAME_L_A = "stationName";

	/**
	 * @fields STATION_NAME_HUMP_L_A: 岗位名称[station_name]
	 */
	public static final String STATION_NAME_HUMP_L_A = "station_name";

	/**
	 * @fields STATION_NAME_U: 岗位名称[STATIONNAME]
	 */
	public static final String STATION_NAME_U = "STATIONNAME";

	/**
	 * @fields STATION_NAME_U_A: 岗位名称[StationName]
	 */
	public static final String STATION_NAME_U_A = "StationName";

	/**
	 * @fields STATION_NAME_HUMP_U_A: 岗位名称[STATION_NAME]
	 */
	public static final String STATION_NAME_HUMP_U_A = "STATION_NAME";

	/**
	 * @fields EMPLOYEE_ID_L: 员工主键[employeeid]
	 */
	public static final String EMPLOYEE_ID_L = "employeeid";

	/**
	 * @fields EMPLOYEE_ID_L_A: 员工主键[employeeId]
	 */
	public static final String EMPLOYEE_ID_L_A = "employeeId";

	/**
	 * @fields EMPLOYEE_ID_HUMP_L_A: 员工主键[employee_id]
	 */
	public static final String EMPLOYEE_ID_HUMP_L_A = "employee_id";

	/**
	 * @fields EMPLOYEE_ID_U: 员工主键[EMPLOYEEID]
	 */
	public static final String EMPLOYEE_ID_U = "EMPLOYEEID";

	/**
	 * @fields EMPLOYEE_ID_U_A: 员工主键[EmployeeId]
	 */
	public static final String EMPLOYEE_ID_U_A = "EmployeeId";

	/**
	 * @fields EMPLOYEE_ID_HUMP_U_A: 员工主键[EMPLOYEE_ID]
	 */
	public static final String EMPLOYEE_ID_HUMP_U_A = "EMPLOYEE_ID";

	/**
	 * @fields EMPLOYEE_NO_L: 员工编号[employeeno]
	 */
	public static final String EMPLOYEE_NO_L = "employeeno";

	/**
	 * @fields EMPLOYEE_NO_L_A: 员工编号[employeeNo]
	 */
	public static final String EMPLOYEE_NO_L_A = "employeeNo";

	/**
	 * @fields EMPLOYEE_NO_HUMP_L_A: 员工编号[employee_no]
	 */
	public static final String EMPLOYEE_NO_HUMP_L_A = "employee_no";

	/**
	 * @fields EMPLOYEE_NO_U: 员工编号[EMPLOYEENO]
	 */
	public static final String EMPLOYEE_NO_U = "EMPLOYEENO";

	/**
	 * @fields EMPLOYEE_NO_U_A: 员工编号[EmployeeNo]
	 */
	public static final String EMPLOYEE_NO_U_A = "EmployeeNo";

	/**
	 * @fields EMPLOYEE_NO_HUMP_U_A: 员工编号[EMPLOYEE_NO]
	 */
	public static final String EMPLOYEE_NO_HUMP_U_A = "EMPLOYEE_NO";

	/**
	 * @fields EMPLOYEE_NAME_L: 员工名称[employeename]
	 */
	public static final String EMPLOYEE_NAME_L = "employeename";

	/**
	 * @fields EMPLOYEE_NAME_L_A: 员工名称[employeeName]
	 */
	public static final String EMPLOYEE_NAME_L_A = "employeeName";

	/**
	 * @fields EMPLOYEE_NAME_HUMP_L_A: 员工名称[employee_name]
	 */
	public static final String EMPLOYEE_NAME_HUMP_L_A = "employee_name";

	/**
	 * @fields EMPLOYEE_NAME_U: 员工名称[EMPLOYEENAME]
	 */
	public static final String EMPLOYEE_NAME_U = "EMPLOYEENAME";

	/**
	 * @fields EMPLOYEE_NAME_U_A: 员工名称[EmployeeName]
	 */
	public static final String EMPLOYEE_NAME_U_A = "EmployeeName";

	/**
	 * @fields EMPLOYEE_NAME_HUMP_U_A: 员工名称[EMPLOYEE_NAME]
	 */
	public static final String EMPLOYEE_NAME_HUMP_U_A = "EMPLOYEE_NAME";

	/**
	 * @fields USER_ID_L: 用户主键[userid]
	 */
	public static final String USER_ID_L = "userid";

	/**
	 * @fields USER_ID_L_A: 用户主键[userId]
	 */
	public static final String USER_ID_L_A = "userId";

	/**
	 * @fields USER_ID_HUMP_L_A: 用户主键[user_id]
	 */
	public static final String USER_ID_HUMP_L_A = "user_id";

	/**
	 * @fields USER_ID_U: 用户主键[USERID]
	 */
	public static final String USER_ID_U = "USERID";

	/**
	 * @fields USER_ID_U_A: 用户主键[UserId]
	 */
	public static final String USER_ID_U_A = "UserId";

	/**
	 * @fields USER_ID_HUMP_U_A: 用户主键[USER_ID]
	 */
	public static final String USER_ID_HUMP_U_A = "USER_ID";

	/**
	 * @fields USER_ACCOUNT_L: 用户账号[useraccount]
	 */
	public static final String USER_ACCOUNT_L = "useraccount";

	/**
	 * @fields USER_ACCOUNT_L_A: 用户账号[userAccount]
	 */
	public static final String USER_ACCOUNT_L_A = "userAccount";

	/**
	 * @fields USER_ACCOUNT_HUMP_L_A: 用户账号[user_account]
	 */
	public static final String USER_ACCOUNT_HUMP_L_A = "user_account";

	/**
	 * @fields USER_ACCOUNT_U: 用户账号[USERACCOUNT]
	 */
	public static final String USER_ACCOUNT_U = "USERACCOUNT";

	/**
	 * @fields USER_ACCOUNT_U_A: 用户账号[UserAccount]
	 */
	public static final String USER_ACCOUNT_U_A = "UserAccount";

	/**
	 * @fields USER_ACCOUNT_HUMP_U_A: 用户账号[USER_ACCOUNT]
	 */
	public static final String USER_ACCOUNT_HUMP_U_A = "USER_ACCOUNT";

	/**
	 * @fields USER_NAME_L: 用户名称[username]
	 */
	public static final String USER_NAME_L = "username";

	/**
	 * @fields USER_NAME_L_A: 用户名称[userName]
	 */
	public static final String USER_NAME_L_A = "userName";

	/**
	 * @fields USER_NAME_HUMP_L_A: 用户名称[user_name]
	 */
	public static final String USER_NAME_HUMP_L_A = "user_name";

	/**
	 * @fields USER_NAME_U: 用户名称[USERNAME]
	 */
	public static final String USER_NAME_U = "USERNAME";

	/**
	 * @fields USER_NAME_U_A: 用户名称[UserName]
	 */
	public static final String USER_NAME_U_A = "UserName";

	/**
	 * @fields USER_NAME_HUMP_U_A: 用户名称[USER_NAME]
	 */
	public static final String USER_NAME_HUMP_U_A = "USER_NAME";

	/**
	 * @fields TENANT_ID_L: 租户主键[tenantid]
	 */
	public static final String TENANT_ID_L = "tenantid";

	/**
	 * @fields TENANT_ID_L_A: 租户主键[tenantId]
	 */
	public static final String TENANT_ID_L_A = "tenantId";

	/**
	 * @fields TENANT_ID_HUMP_L_A: 租户主键[tenant_id]
	 */
	public static final String TENANT_ID_HUMP_L_A = "tenant_id";

	/**
	 * @fields TENANT_ID_U: 租户主键[TENANTID]
	 */
	public static final String TENANT_ID_U = "TENANTID";

	/**
	 * @fields TENANT_ID_U_A: 租户主键[TenantId]
	 */
	public static final String TENANT_ID_U_A = "TenantId";

	/**
	 * @fields TENANT_ID_HUMP_U_A: 租户主键[TENANT_ID]
	 */
	public static final String TENANT_ID_HUMP_U_A = "TENANT_ID";

	/**
	 * @fields TENANT_CODE_L: 租户编码[tenantcode]
	 */
	public static final String TENANT_CODE_L = "tenantcode";

	/**
	 * @fields TENANT_CODE_L_A: 租户编码[tenantCode]
	 */
	public static final String TENANT_CODE_L_A = "tenantCode";

	/**
	 * @fields TENANT_CODE_HUMP_L_A: 租户编码[tenant_code]
	 */
	public static final String TENANT_CODE_HUMP_L_A = "tenant_code";

	/**
	 * @fields TENANT_CODE_U: 租户编码[TENANTCODE]
	 */
	public static final String TENANT_CODE_U = "TENANTCODE";

	/**
	 * @fields TENANT_CODE_U_A: 租户编码[TenantCode]
	 */
	public static final String TENANT_CODE_U_A = "TenantCode";

	/**
	 * @fields TENANT_CODE_HUMP_U_A: 租户编码[TENANT_CODE]
	 */
	public static final String TENANT_CODE_HUMP_U_A = "TENANT_CODE";

	/**
	 * @fields TENANT_NAME_L: 租户名称[tenantname]
	 */
	public static final String TENANT_NAME_L = "tenantname";

	/**
	 * @fields TENANT_NAME_L_A: 租户名称[tenantName]
	 */
	public static final String TENANT_NAME_L_A = "tenantName";

	/**
	 * @fields TENANT_NAME_HUMP_L_A: 租户名称[tenant_name]
	 */
	public static final String TENANT_NAME_HUMP_L_A = "tenant_name";

	/**
	 * @fields TENANT_NAME_U: 租户名称[TENANTNAME]
	 */
	public static final String TENANT_NAME_U = "TENANTNAME";

	/**
	 * @fields TENANT_NAME_U_A: 租户名称[TenantName]
	 */
	public static final String TENANT_NAME_U_A = "TenantName";

	/**
	 * @fields TENANT_NAME_HUMP_U_A: 租户名称[TENANT_NAME]
	 */
	public static final String TENANT_NAME_HUMP_U_A = "TENANT_NAME";

	/**
	 * @fields SYSTEM_ID_L: 系统主键[systemid]
	 */
	public static final String SYSTEM_ID_L = "systemid";

	/**
	 * @fields SYSTEM_ID_L_A: 系统主键[systemId]
	 */
	public static final String SYSTEM_ID_L_A = "systemId";

	/**
	 * @fields SYSTEM_ID_HUMP_L_A: 系统主键[system_id]
	 */
	public static final String SYSTEM_ID_HUMP_L_A = "system_id";

	/**
	 * @fields SYSTEM_ID_U: 系统主键[SYSTEMID]
	 */
	public static final String SYSTEM_ID_U = "SYSTEMID";

	/**
	 * @fields SYSTEM_ID_U_A: 系统主键[SystemId]
	 */
	public static final String SYSTEM_ID_U_A = "SystemId";

	/**
	 * @fields SYSTEM_ID_HUMP_U_A: 系统主键[SYSTEM_ID]
	 */
	public static final String SYSTEM_ID_HUMP_U_A = "SYSTEM_ID";

	/**
	 * @fields SYSTEM_CODE_L: 系统编码[systemcode]
	 */
	public static final String SYSTEM_CODE_L = "systemcode";

	/**
	 * @fields SYSTEM_CODE_L_A: 系统编码[systemCode]
	 */
	public static final String SYSTEM_CODE_L_A = "systemCode";

	/**
	 * @fields SYSTEM_CODE_HUMP_L_A: 系统编码[system_code]
	 */
	public static final String SYSTEM_CODE_HUMP_L_A = "system_code";

	/**
	 * @fields SYSTEM_CODE_U: 系统编码[SYSTEMCODE]
	 */
	public static final String SYSTEM_CODE_U = "SYSTEMCODE";

	/**
	 * @fields SYSTEM_CODE_U_A: 系统编码[SystemCode]
	 */
	public static final String SYSTEM_CODE_U_A = "SystemCode";

	/**
	 * @fields SYSTEM_CODE_HUMP_U_A: 系统编码[SYSTEM_CODE]
	 */
	public static final String SYSTEM_CODE_HUMP_U_A = "SYSTEM_CODE";

	/**
	 * @fields SYSTEM_NAME_L: 系统名称[systemname]
	 */
	public static final String SYSTEM_NAME_L = "systemname";

	/**
	 * @fields SYSTEM_NAME_L_A: 系统名称[systemName]
	 */
	public static final String SYSTEM_NAME_L_A = "systemName";

	/**
	 * @fields SYSTEM_NAME_HUMP_L_A: 系统名称[system_name]
	 */
	public static final String SYSTEM_NAME_HUMP_L_A = "system_name";

	/**
	 * @fields SYSTEM_NAME_U: 系统名称[SYSTEMNAME]
	 */
	public static final String SYSTEM_NAME_U = "SYSTEMNAME";

	/**
	 * @fields SYSTEM_NAME_U_A: 系统名称[SystemName]
	 */
	public static final String SYSTEM_NAME_U_A = "SystemName";

	/**
	 * @fields SYSTEM_NAME_HUMP_U_A: 系统名称[SYSTEM_NAME]
	 */
	public static final String SYSTEM_NAME_HUMP_U_A = "SYSTEM_NAME";

	/**
	 * @fields MODULE_ID_L: 模块主键[moduleid]
	 */
	public static final String MODULE_ID_L = "moduleid";

	/**
	 * @fields MODULE_ID_L_A: 模块主键[moduleId]
	 */
	public static final String MODULE_ID_L_A = "moduleId";

	/**
	 * @fields MODULE_ID_HUMP_L_A: 模块主键[module_id]
	 */
	public static final String MODULE_ID_HUMP_L_A = "module_id";

	/**
	 * @fields MODULE_ID_U: 模块主键[MODULEID]
	 */
	public static final String MODULE_ID_U = "MODULEID";

	/**
	 * @fields MODULE_ID_U_A: 模块主键[ModuleId]
	 */
	public static final String MODULE_ID_U_A = "ModuleId";

	/**
	 * @fields MODULE_ID_HUMP_U_A: 模块主键[MODULE_ID]
	 */
	public static final String MODULE_ID_HUMP_U_A = "MODULE_ID";

	/**
	 * @fields MODULE_CODE_L: 模块编码[modulecode]
	 */
	public static final String MODULE_CODE_L = "modulecode";

	/**
	 * @fields MODULE_CODE_L_A: 模块编码[moduleCode]
	 */
	public static final String MODULE_CODE_L_A = "moduleCode";

	/**
	 * @fields MODULE_CODE_HUMP_L_A: 模块编码[module_code]
	 */
	public static final String MODULE_CODE_HUMP_L_A = "module_code";

	/**
	 * @fields MODULE_CODE_U: 模块编码[MODULECODE]
	 */
	public static final String MODULE_CODE_U = "MODULECODE";

	/**
	 * @fields MODULE_CODE_U_A: 模块编码[ModuleCode]
	 */
	public static final String MODULE_CODE_U_A = "ModuleCode";

	/**
	 * @fields MODULE_CODE_HUMP_U_A: 模块编码[MODULE_CODE]
	 */
	public static final String MODULE_CODE_HUMP_U_A = "MODULE_CODE";

	/**
	 * @fields MODULE_NAME_L: 模块名称[modulename]
	 */
	public static final String MODULE_NAME_L = "modulename";

	/**
	 * @fields MODULE_NAME_L_A: 模块名称[moduleName]
	 */
	public static final String MODULE_NAME_L_A = "moduleName";

	/**
	 * @fields MODULE_NAME_HUMP_L_A: 模块名称[module_name]
	 */
	public static final String MODULE_NAME_HUMP_L_A = "module_name";

	/**
	 * @fields MODULE_NAME_U: 模块名称[MODULENAME]
	 */
	public static final String MODULE_NAME_U = "MODULENAME";

	/**
	 * @fields MODULE_NAME_U_A: 模块名称[ModuleName]
	 */
	public static final String MODULE_NAME_U_A = "ModuleName";

	/**
	 * @fields MODULE_NAME_HUMP_U_A: 模块名称[MODULE_NAME]
	 */
	public static final String MODULE_NAME_HUMP_U_A = "MODULE_NAME";

	/**
	 * @fields MODEL_ID_L: 模型主键[modelid]
	 */
	public static final String MODEL_ID_L = "modelid";

	/**
	 * @fields MODEL_ID_L_A: 模型主键[modelId]
	 */
	public static final String MODEL_ID_L_A = "modelId";

	/**
	 * @fields MODEL_ID_HUMP_L_A: 模型主键[model_id]
	 */
	public static final String MODEL_ID_HUMP_L_A = "model_id";

	/**
	 * @fields MODEL_ID_U: 模型主键[MODELID]
	 */
	public static final String MODEL_ID_U = "MODELID";

	/**
	 * @fields MODEL_ID_U_A: 模型主键[ModelId]
	 */
	public static final String MODEL_ID_U_A = "ModelId";

	/**
	 * @fields MODEL_ID_HUMP_U_A: 模型主键[MODEL_ID]
	 */
	public static final String MODEL_ID_HUMP_U_A = "MODEL_ID";

	/**
	 * @fields MODEL_CODE_L: 模块编码[modelcode]
	 */
	public static final String MODEL_CODE_L = "modelcode";

	/**
	 * @fields MODEL_CODE_L_A: 模块编码[modelCode]
	 */
	public static final String MODEL_CODE_L_A = "modelCode";

	/**
	 * @fields MODEL_CODE_HUMP_L_A: 模块编码[model_code]
	 */
	public static final String MODEL_CODE_HUMP_L_A = "model_code";

	/**
	 * @fields MODEL_CODE_U: 模块编码[MODELCODE]
	 */
	public static final String MODEL_CODE_U = "MODELCODE";

	/**
	 * @fields MODEL_CODE_U_A: 模块编码[ModelCode]
	 */
	public static final String MODEL_CODE_U_A = "ModelCode";

	/**
	 * @fields MODEL_CODE_HUMP_U_A: 模块编码[MODEL_CODE]
	 */
	public static final String MODEL_CODE_HUMP_U_A = "MODEL_CODE";

	/**
	 * @fields MODEL_NAME_L: 模块名称[modelname]
	 */
	public static final String MODEL_NAME_L = "modelname";

	/**
	 * @fields MODEL_NAME_L_A: 模块名称[modelName]
	 */
	public static final String MODEL_NAME_L_A = "modelName";

	/**
	 * @fields MODEL_NAME_HUMP_L_A: 模块名称[model_name]
	 */
	public static final String MODEL_NAME_HUMP_L_A = "model_name";

	/**
	 * @fields MODEL_NAME_U: 模块名称[MODELNAME]
	 */
	public static final String MODEL_NAME_U = "MODELNAME";

	/**
	 * @fields MODEL_NAME_U_A: 模块名称[ModelName]
	 */
	public static final String MODEL_NAME_U_A = "ModelName";

	/**
	 * @fields MODEL_NAME_HUMP_U_A: 模块名称[MODEL_NAME]
	 */
	public static final String MODEL_NAME_HUMP_U_A = "MODEL_NAME";

	/**
	 * @fields APP_ID_L: 应用主键[appid]
	 */
	public static final String APP_ID_L = "appid";

	/**
	 * @fields APP_ID_L_A: 应用主键[appId]
	 */
	public static final String APP_ID_L_A = "appId";

	/**
	 * @fields APP_ID_HUMP_L_A: 应用主键[app_id]
	 */
	public static final String APP_ID_HUMP_L_A = "app_id";

	/**
	 * @fields APP_ID_U: 应用主键[APPID]
	 */
	public static final String APP_ID_U = "APPID";

	/**
	 * @fields APP_ID_U_A: 应用主键[AppId]
	 */
	public static final String APP_ID_U_A = "AppId";

	/**
	 * @fields APP_ID_HUMP_U_A: 应用主键[APP_ID]
	 */
	public static final String APP_ID_HUMP_U_A = "APP_ID";

	/**
	 * @fields APP_CODE_L: 应用编码[appcode]
	 */
	public static final String APP_CODE_L = "appcode";

	/**
	 * @fields APP_CODE_L_A: 应用编码[appCode]
	 */
	public static final String APP_CODE_L_A = "appCode";

	/**
	 * @fields APP_CODE_HUMP_L_A: 应用编码[app_code]
	 */
	public static final String APP_CODE_HUMP_L_A = "app_code";

	/**
	 * @fields APP_CODE_U: 应用编码[APPCODE]
	 */
	public static final String APP_CODE_U = "APPCODE";

	/**
	 * @fields APP_CODE_U_A: 应用编码[AppCode]
	 */
	public static final String APP_CODE_U_A = "AppCode";

	/**
	 * @fields APP_CODE_HUMP_U_A: 应用编码[APP_CODE]
	 */
	public static final String APP_CODE_HUMP_U_A = "APP_CODE";

	/**
	 * @fields APP_NAME_L: 应用名称[appname]
	 */
	public static final String APP_NAME_L = "appname";

	/**
	 * @fields APP_NAME_L_A: 应用名称[appName]
	 */
	public static final String APP_NAME_L_A = "appName";

	/**
	 * @fields APP_NAME_HUMP_L_A: 应用名称[app_name]
	 */
	public static final String APP_NAME_HUMP_L_A = "app_name";

	/**
	 * @fields APP_NAME_U: 应用名称[APPNAME]
	 */
	public static final String APP_NAME_U = "APPNAME";

	/**
	 * @fields APP_NAME_U_A: 应用名称[AppName]
	 */
	public static final String APP_NAME_U_A = "AppName";

	/**
	 * @fields APP_NAME_HUMP_U_A: 应用名称[APP_NAME]
	 */
	public static final String APP_NAME_HUMP_U_A = "APP_NAME";

	/**
	 * @fields PAGE_ID_L: 页面主键[pageid]
	 */
	public static final String PAGE_ID_L = "pageid";

	/**
	 * @fields PAGE_ID_L_A: 页面主键[pageId]
	 */
	public static final String PAGE_ID_L_A = "pageId";

	/**
	 * @fields PAGE_ID_HUMP_L_A: 页面主键[page_id]
	 */
	public static final String PAGE_ID_HUMP_L_A = "page_id";

	/**
	 * @fields PAGE_ID_U: 页面主键[PAGEID]
	 */
	public static final String PAGE_ID_U = "PAGEID";

	/**
	 * @fields PAGE_ID_U_A: 页面主键[PageId]
	 */
	public static final String PAGE_ID_U_A = "PageId";

	/**
	 * @fields PAGE_ID_HUMP_U_A: 页面主键[PAGE_ID]
	 */
	public static final String PAGE_ID_HUMP_U_A = "PAGE_ID";

	/**
	 * @fields PAGE_CODE_L: 页面编码[pagecode]
	 */
	public static final String PAGE_CODE_L = "pagecode";

	/**
	 * @fields PAGE_CODE_L_A: 页面编码[pageCode]
	 */
	public static final String PAGE_CODE_L_A = "pageCode";

	/**
	 * @fields PAGE_CODE_HUMP_L_A: 页面编码[page_code]
	 */
	public static final String PAGE_CODE_HUMP_L_A = "page_code";

	/**
	 * @fields PAGE_CODE_U: 页面编码[PAGECODE]
	 */
	public static final String PAGE_CODE_U = "PAGECODE";

	/**
	 * @fields PAGE_CODE_U_A: 页面编码[PageCode]
	 */
	public static final String PAGE_CODE_U_A = "PageCode";

	/**
	 * @fields PAGE_CODE_HUMP_U_A: 页面编码[PAGE_CODE]
	 */
	public static final String PAGE_CODE_HUMP_U_A = "PAGE_CODE";

	/**
	 * @fields PAGE_NAME_L: 页面名称[pagename]
	 */
	public static final String PAGE_NAME_L = "pagename";

	/**
	 * @fields PAGE_NAME_L_A: 页面名称[pageName]
	 */
	public static final String PAGE_NAME_L_A = "pageName";

	/**
	 * @fields PAGE_NAME_HUMP_L_A: 页面名称[page_name]
	 */
	public static final String PAGE_NAME_HUMP_L_A = "page_name";

	/**
	 * @fields PAGE_NAME_U: 页面名称[PAGENAME]
	 */
	public static final String PAGE_NAME_U = "PAGENAME";

	/**
	 * @fields PAGE_NAME_U_A: 页面名称[PageName]
	 */
	public static final String PAGE_NAME_U_A = "PageName";

	/**
	 * @fields PAGE_NAME_HUMP_U_A: 页面名称[PAGE_NAME]
	 */
	public static final String PAGE_NAME_HUMP_U_A = "PAGE_NAME";

	/**
	 * @fields CONTEXT_ID_L: 上下文主键[contextid]
	 */
	public static final String CONTEXT_ID_L = "contextid";

	/**
	 * @fields CONTEXT_ID_L_A: 上下文主键[contextId]
	 */
	public static final String CONTEXT_ID_L_A = "contextId";

	/**
	 * @fields CONTEXT_ID_HUMP_L_A: 上下文主键[context_id]
	 */
	public static final String CONTEXT_ID_HUMP_L_A = "context_id";

	/**
	 * @fields CONTEXT_ID_U: 上下文主键[CONTEXTID]
	 */
	public static final String CONTEXT_ID_U = "CONTEXTID";

	/**
	 * @fields CONTEXT_ID_U_A: 上下文主键[ContextId]
	 */
	public static final String CONTEXT_ID_U_A = "ContextId";

	/**
	 * @fields CONTEXT_ID_HUMP_U_A: 上下文主键[CONTEXT_ID]
	 */
	public static final String CONTEXT_ID_HUMP_U_A = "CONTEXT_ID";

	/**
	 * @fields EXECUTION_ID_L: 执行主键[executionid]
	 */
	public static final String EXECUTION_ID_L = "executionid";

	/**
	 * @fields EXECUTION_ID_L_A: 执行主键[executionId]
	 */
	public static final String EXECUTION_ID_L_A = "executionId";

	/**
	 * @fields EXECUTION_ID_HUMP_L_A: 执行主键[execution_id]
	 */
	public static final String EXECUTION_ID_HUMP_L_A = "execution_id";

	/**
	 * @fields EXECUTION_ID_U: 执行主键[EXECUTIONID]
	 */
	public static final String EXECUTION_ID_U = "EXECUTIONID";

	/**
	 * @fields EXECUTION_ID_U_A: 执行主键[ExecutionId]
	 */
	public static final String EXECUTION_ID_U_A = "ExecutionId";

	/**
	 * @fields EXECUTION_ID_HUMP_U_A: 执行主键[EXECUTION_ID]
	 */
	public static final String EXECUTION_ID_HUMP_U_A = "EXECUTION_ID";

	/**
	 * @fields IS_SYSTEM_L: 是否系统[issystem]
	 */
	public static final String IS_SYSTEM_L = "issystem";

	/**
	 * @fields IS_SYSTEM_L_A: 是否系统[isSystem]
	 */
	public static final String IS_SYSTEM_L_A = "isSystem";

	/**
	 * @fields IS_SYSTEM_HUMP_L_A: 是否系统[is_system]
	 */
	public static final String IS_SYSTEM_HUMP_L_A = "is_system";

	/**
	 * @fields IS_SYSTEM_U: 是否系统[ISSYSTEM]
	 */
	public static final String IS_SYSTEM_U = "ISSYSTEM";

	/**
	 * @fields IS_SYSTEM_U_A: 是否系统[IsSystem]
	 */
	public static final String IS_SYSTEM_U_A = "IsSystem";

	/**
	 * @fields IS_SYSTEM_HUMP_U_A: 是否系统[IS_SYSTEM]
	 */
	public static final String IS_SYSTEM_HUMP_U_A = "IS_SYSTEM";

	/**
	 * @fields IS_EXTERNAL_L: 是否外部[isexternal]
	 */
	public static final String IS_EXTERNAL_L = "isexternal";

	/**
	 * @fields IS_EXTERNAL_L_A: 是否外部[isExternal]
	 */
	public static final String IS_EXTERNAL_L_A = "isExternal";

	/**
	 * @fields IS_EXTERNAL_HUMP_L_A: 是否外部[is_external]
	 */
	public static final String IS_EXTERNAL_HUMP_L_A = "is_external";

	/**
	 * @fields IS_EXTERNAL_U: 是否外部[ISEXTERNAL]
	 */
	public static final String IS_EXTERNAL_U = "ISEXTERNAL";

	/**
	 * @fields IS_EXTERNAL_U_A: 是否外部[IsExternal]
	 */
	public static final String IS_EXTERNAL_U_A = "IsExternal";

	/**
	 * @fields IS_EXTERNAL_HUMP_U_A: 是否外部[IS_EXTERNAL]
	 */
	public static final String IS_EXTERNAL_HUMP_U_A = "IS_EXTERNAL";

	/**
	 * @fields IS_INTERNAL_L: 是否内部[isinternal]
	 */
	public static final String IS_INTERNAL_L = "isinternal";

	/**
	 * @fields IS_INTERNAL_L_A: 是否内部[isInternal]
	 */
	public static final String IS_INTERNAL_L_A = "isInternal";

	/**
	 * @fields IS_INTERNAL_HUMP_L_A: 是否内部[is_internal]
	 */
	public static final String IS_INTERNAL_HUMP_L_A = "is_internal";

	/**
	 * @fields IS_INTERNAL_U: 是否内部[ISINTERNAL]
	 */
	public static final String IS_INTERNAL_U = "ISINTERNAL";

	/**
	 * @fields IS_INTERNAL_U_A: 是否内部[IsInternal]
	 */
	public static final String IS_INTERNAL_U_A = "IsInternal";

	/**
	 * @fields IS_INTERNAL_HUMP_U_A: 是否内部[IS_INTERNAL]
	 */
	public static final String IS_INTERNAL_HUMP_U_A = "IS_INTERNAL";

	/**
	 * @fields IS_BUSINESS_L: 是否业务[isbusiness]
	 */
	public static final String IS_BUSINESS_L = "isbusiness";

	/**
	 * @fields IS_BUSINESS_L_A: 是否业务[isBusiness]
	 */
	public static final String IS_BUSINESS_L_A = "isBusiness";

	/**
	 * @fields IS_BUSINESS_HUMP_L_A: 是否业务[is_business]
	 */
	public static final String IS_BUSINESS_HUMP_L_A = "is_business";

	/**
	 * @fields IS_BUSINESS_U: 是否业务[ISBUSINESS]
	 */
	public static final String IS_BUSINESS_U = "ISBUSINESS";

	/**
	 * @fields IS_BUSINESS_U_A: 是否业务[IsBusiness]
	 */
	public static final String IS_BUSINESS_U_A = "IsBusiness";

	/**
	 * @fields IS_BUSINESS_HUMP_U_A: 是否业务[IS_BUSINESS]
	 */
	public static final String IS_BUSINESS_HUMP_U_A = "IS_BUSINESS";

	/**
	 * @fields IS_LOCK_L: 是否锁定[islock]
	 */
	public static final String IS_LOCK_L = "islock";

	/**
	 * @fields IS_LOCK_L_A: 是否锁定[isLock]
	 */
	public static final String IS_LOCK_L_A = "isLock";

	/**
	 * @fields IS_LOCK_HUMP_L_A: 是否锁定[is_lock]
	 */
	public static final String IS_LOCK_HUMP_L_A = "is_lock";

	/**
	 * @fields IS_LOCK_U: 是否锁定[ISLOCK]
	 */
	public static final String IS_LOCK_U = "ISLOCK";

	/**
	 * @fields IS_LOCK_U_A: 是否锁定[IsLock]
	 */
	public static final String IS_LOCK_U_A = "IsLock";

	/**
	 * @fields IS_LOCK_HUMP_U_A: 是否锁定[IS_LOCK]
	 */
	public static final String IS_LOCK_HUMP_U_A = "IS_LOCK";

	/**
	 * @fields IS_ENABLED_L: 是否有效[isenabled]
	 */
	public static final String IS_ENABLED_L = "isenabled";

	/**
	 * @fields IS_ENABLED_L_A: 是否有效[isEnabled]
	 */
	public static final String IS_ENABLED_L_A = "isEnabled";

	/**
	 * @fields IS_ENABLED_HUMP_L_A: 是否有效[is_enabled]
	 */
	public static final String IS_ENABLED_HUMP_L_A = "is_enabled";

	/**
	 * @fields IS_ENABLED_U: 是否有效[ISENABLED]
	 */
	public static final String IS_ENABLED_U = "ISENABLED";

	/**
	 * @fields IS_ENABLED_U_A: 是否有效[IsEnabled]
	 */
	public static final String IS_ENABLED_U_A = "IsEnabled";

	/**
	 * @fields IS_ENABLED_HUMP_U_A: 是否有效[IS_ENABLED]
	 */
	public static final String IS_ENABLED_HUMP_U_A = "IS_ENABLED";

	/**
	 * @fields IS_DELETED_L: 是否删除[isdeleted]
	 */
	public static final String IS_DELETED_L = "isdeleted";

	/**
	 * @fields IS_DELETED_L_A: 是否删除[isDeleted]
	 */
	public static final String IS_DELETED_L_A = "isDeleted";

	/**
	 * @fields IS_DELETED_HUMP_L_A: 是否删除[is_deleted]
	 */
	public static final String IS_DELETED_HUMP_L_A = "is_deleted";

	/**
	 * @fields IS_DELETED_U: 是否删除[ISDELETED]
	 */
	public static final String IS_DELETED_U = "ISDELETED";

	/**
	 * @fields IS_DELETED_U_A: 是否删除[IsDeleted]
	 */
	public static final String IS_DELETED_U_A = "IsDeleted";

	/**
	 * @fields IS_DELETED_HUMP_U_A: 是否删除[IS_DELETED]
	 */
	public static final String IS_DELETED_HUMP_U_A = "IS_DELETED";

	/**
	 * @fields STATUS_L: 状态[status]
	 */
	public static final String STATUS_L = "status";

	/**
	 * @fields STATUS_U: 状态[STATUS]
	 */
	public static final String STATUS_U = "STATUS";

	/**
	 * @fields STATUS_U_A: 状态[Status]
	 */
	public static final String STATUS_U_A = "Status";

	/**
	 * @fields REMARK_L: 说明[remark]
	 */
	public static final String REMARK_L = "remark";

	/**
	 * @fields REMARK_U: 说明[REMARK]
	 */
	public static final String REMARK_U = "REMARK";

	/**
	 * @fields REMARK_U_A: 说明[Remark]
	 */
	public static final String REMARK_U_A = "Remark";

	/**
	 * @fields DESCIPTION_L: 描述[description]
	 */
	public static final String DESCIPTION_L = "description";

	/**
	 * @fields DESCIPTION_U: 描述[DESCIPTION]
	 */
	public static final String DESCIPTION_U = "DESCIPTION";

	/**
	 * @fields DESCIPTION_U_A: 描述[Description]
	 */
	public static final String DESCIPTION_U_A = "Description";

	/**
	 * @fields ROW_INDEX_L: 行号[rowindex]
	 */
	public static final String ROW_INDEX_L = "rowindex";

	/**
	 * @fields ROW_INDEX_L_A: 行号[rowIndex]
	 */
	public static final String ROW_INDEX_L_A = "rowIndex";

	/**
	 * @fields ROW_INDEX_HUMP_L_A: 行号[row_index]
	 */
	public static final String ROW_INDEX_HUMP_L_A = "row_index";

	/**
	 * @fields ROW_INDEX_U: 行号[ROWINDEX]
	 */
	public static final String ROW_INDEX_U = "ROWINDEX";

	/**
	 * @fields ROW_INDEX_U_A: 行号[RowIndex]
	 */
	public static final String ROW_INDEX_U_A = "RowIndex";

	/**
	 * @fields ROW_INDEX_HUMP_U_A: 行号[ROW_INDEX]
	 */
	public static final String ROW_INDEX_HUMP_U_A = "ROW_INDEX";

	/**
	 * @fields CREATOR_ID_L: 创建者主键[creatorid]
	 */
	public static final String CREATOR_ID_L = "creatorid";

	/**
	 * @fields CREATOR_ID_L_A: 创建者主键[creatorId]
	 */
	public static final String CREATOR_ID_L_A = "creatorId";

	/**
	 * @fields CREATOR_ID_HUMP_L_A: 创建者主键[creator_id]
	 */
	public static final String CREATOR_ID_HUMP_L_A = "creator_id";

	/**
	 * @fields CREATOR_ID_U: 创建者主键[CREATORID]
	 */
	public static final String CREATOR_ID_U = "CREATORID";

	/**
	 * @fields CREATOR_ID_U_A: 创建者主键[CreatorId]
	 */
	public static final String CREATOR_ID_U_A = "CreatorId";

	/**
	 * @fields CREATOR_ID_HUMP_U_A: 创建者主键[CREATOR_ID]
	 */
	public static final String CREATOR_ID_HUMP_U_A = "CREATOR_ID";

	/**
	 * @fields CREATOR_NAME_L: 创建者名称[creatorname]
	 */
	public static final String CREATOR_NAME_L = "creatorname";

	/**
	 * @fields CREATOR_NAME_L_A: 创建者名称[creatorName]
	 */
	public static final String CREATOR_NAME_L_A = "creatorName";

	/**
	 * @fields CREATOR_NAME_HUMP_L_A: 创建者名称[creator_name]
	 */
	public static final String CREATOR_NAME_HUMP_L_A = "creator_name";

	/**
	 * @fields CREATOR_NAME_U: 创建者名称[CREATORNAME]
	 */
	public static final String CREATOR_NAME_U = "CREATORNAME";

	/**
	 * @fields CREATOR_NAME_U_A: 创建者名称[CreatorName]
	 */
	public static final String CREATOR_NAME_U_A = "CreatorName";

	/**
	 * @fields CREATOR_NAME_HUMP_U_A: 创建者名称[CREATOR_NAME]
	 */
	public static final String CREATOR_NAME_HUMP_U_A = "CREATOR_NAME";

	/**
	 * @fields CREATE_TIME_L: 创建时间[createtime]
	 */
	public static final String CREATE_TIME_L = "createtime";

	/**
	 * @fields CREATE_TIME_L_A: 创建时间[createTime]
	 */
	public static final String CREATE_TIME_L_A = "createTime";

	/**
	 * @fields CREATE_TIME_HUMP_L_A: 创建时间[create_time]
	 */
	public static final String CREATE_TIME_HUMP_L_A = "create_time";

	/**
	 * @fields CREATE_TIME_U: 创建时间[CREATETIME]
	 */
	public static final String CREATE_TIME_U = "CREATETIME";

	/**
	 * @fields CREATE_TIME_U_A: 创建时间[CreateTime]
	 */
	public static final String CREATE_TIME_U_A = "CreateTime";

	/**
	 * @fields CREATE_TIME_HUMP_U_A: 创建时间[CREATE_TIME]
	 */
	public static final String CREATE_TIME_HUMP_U_A = "CREATE_TIME";

	/**
	 * @fields UPDATOR_ID_L: 更新者主键[updatorid]
	 */
	public static final String UPDATOR_ID_L = "updatorid";

	/**
	 * @fields UPDATOR_ID_L_A: 更新者主键[updatorId]
	 */
	public static final String UPDATOR_ID_L_A = "updatorId";

	/**
	 * @fields UPDATOR_ID_HUMP_L_A: 更新者主键[updator_id]
	 */
	public static final String UPDATOR_ID_HUMP_L_A = "updator_id";

	/**
	 * @fields UPDATOR_ID_U: 更新者主键[UPDATORID]
	 */
	public static final String UPDATOR_ID_U = "UPDATORID";

	/**
	 * @fields UPDATOR_ID_U_A: 更新者主键[UpdatorId]
	 */
	public static final String UPDATOR_ID_U_A = "UpdatorId";

	/**
	 * @fields UPDATOR_ID_HUMP_U_A: 更新者主键[UPDATOR_ID]
	 */
	public static final String UPDATOR_ID_HUMP_U_A = "UPDATOR_ID";

	/**
	 * @fields UPDATOR_NAME_L: 更新者名称[updatorname]
	 */
	public static final String UPDATOR_NAME_L = "updatorname";

	/**
	 * @fields UPDATOR_NAME_L_A: 更新者名称[updatorName]
	 */
	public static final String UPDATOR_NAME_L_A = "updatorName";

	/**
	 * @fields UPDATOR_NAME_HUMP_L_A: 更新者名称[updator_name]
	 */
	public static final String UPDATOR_NAME_HUMP_L_A = "updator_name";

	/**
	 * @fields UPDATOR_NAME_U: 更新者名称[UPDATORNAME]
	 */
	public static final String UPDATOR_NAME_U = "UPDATORNAME";

	/**
	 * @fields UPDATOR_NAME_U_A: 更新者名称[UpdatorName]
	 */
	public static final String UPDATOR_NAME_U_A = "UpdatorName";

	/**
	 * @fields UPDATOR_NAME_HUMP_U_A: 更新者名称[UPDATOR_NAME]
	 */
	public static final String UPDATOR_NAME_HUMP_U_A = "UPDATOR_NAME";

	/**
	 * @fields UPDATE_TIME_L: 更新时间[updatetime]
	 */
	public static final String UPDATE_TIME_L = "updatetime";

	/**
	 * @fields UPDATE_TIME_L_A: 更新时间[updateTime]
	 */
	public static final String UPDATE_TIME_L_A = "updateTime";

	/**
	 * @fields UPDATE_TIME_HUMP_L_A: 更新时间[update_time]
	 */
	public static final String UPDATE_TIME_HUMP_L_A = "update_time";

	/**
	 * @fields UPDATE_TIME_U: 更新时间[UPDATETIME]
	 */
	public static final String UPDATE_TIME_U = "UPDATETIME";

	/**
	 * @fields UPDATE_TIME_U_A: 更新时间[UpdateTime]
	 */
	public static final String UPDATE_TIME_U_A = "UpdateTime";

	/**
	 * @fields UPDATE_TIME_HUMP_U_A: 更新时间[UPDATE_TIME]
	 */
	public static final String UPDATE_TIME_HUMP_U_A = "UPDATE_TIME";
}
