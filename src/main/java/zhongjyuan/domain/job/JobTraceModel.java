package zhongjyuan.domain.job;

import java.time.LocalDateTime;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: JobTraceModel
 * @description: 任务跟踪对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:54:20
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class JobTraceModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields taskId: 任务主键
	 */
	protected String taskId;

	/**
	 * @fields taskCode: 任务编码
	 */
	protected String taskCode;

	/**
	 * @fields taskName: 任务名称
	 */
	protected String taskName;

	/**
	 * @fields groupId: 分组主键
	 */
	protected String groupId;

	/**
	 * @fields groupNo: 分组编号
	 */
	protected String groupNo;

	/**
	 * @fields groupName: 分组名称
	 */
	protected String groupName;

	/**
	 * @fields startTime: 开始时间
	 */
	protected LocalDateTime startTime;

	/**
	 * @fields finishTime: 完成时间
	 */
	protected LocalDateTime finishTime;

	/**
	 * @fields nextTime: 下次时间
	 */
	protected LocalDateTime nextTime;

	/**
	 * @fields result: 执行结果
	 */
	protected String result;

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取主键
	 * @return 主键
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置主键 
	 * @param id 主键
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getTaskId
	 * @author: zhongjyuan
	 * @description: 获取任务主键
	 * @return 任务主键
	 * @throws
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @title: setTaskId
	 * @author: zhongjyuan
	 * @description: 设置任务主键
	 * @param taskId 任务主键
	 * @throws
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * @title: getTaskCode
	 * @author: zhongjyuan
	 * @description: 获取任务编码
	 * @return 任务编码
	 * @throws
	 */
	public String getTaskCode() {
		return taskCode;
	}

	/**
	 * @title: setTaskCode
	 * @author: zhongjyuan
	 * @description: 设置任务编码
	 * @param taskCode 任务编码
	 * @throws
	 */
	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	/**
	 * @title: getTaskName
	 * @author: zhongjyuan
	 * @description: 获取任务名称
	 * @return 任务名称
	 * @throws
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @title: setTaskName
	 * @author: zhongjyuan
	 * @description: 设置任务名称
	 * @param taskName 任务名称
	 * @throws
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @title: getGroupId
	 * @author: zhongjyuan
	 * @description: 获取分组主键
	 * @return 分组主键
	 * @throws
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @title: setGroupId
	 * @author: zhongjyuan
	 * @description: 设置分组主键
	 * @param groupId 分组主键
	 * @throws
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @title: getGroupNo
	 * @author: zhongjyuan
	 * @description: 获取分组编号
	 * @return 分组编号
	 * @throws
	 */
	public String getGroupNo() {
		return groupNo;
	}

	/**
	 * @title: setGroupNo
	 * @author: zhongjyuan
	 * @description: 设置分组编号
	 * @param groupNo 分组编号
	 * @throws
	 */
	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	/**
	 * @title: getGroupName
	 * @author: zhongjyuan
	 * @description: 获取分组名称
	 * @return 分组名称
	 * @throws
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @title: setGroupName
	 * @author: zhongjyuan
	 * @description: 设置分组名称
	 * @param groupName 分组名称
	 * @throws
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @title: getStartTime
	 * @author: zhongjyuan
	 * @description: 获取开始时间
	 * @return 开始时间
	 * @throws
	 */
	public LocalDateTime getStartTime() {
		return startTime;
	}

	/**
	 * @title: setStartTime
	 * @author: zhongjyuan
	 * @description: 设置开始时间
	 * @param startTime 开始时间
	 * @throws
	 */
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	/**
	 * @title: getFinishTime
	 * @author: zhongjyuan
	 * @description: 获取完成时间
	 * @return 完成时间
	 * @throws
	 */
	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	/**
	 * @title: setFinishTime
	 * @author: zhongjyuan
	 * @description: 设置完成时间
	 * @param finishTime 完成时间
	 * @throws
	 */
	public void setFinishTime(LocalDateTime finishTime) {
		this.finishTime = finishTime;
	}

	/**
	 * @title: getNextTime
	 * @author: zhongjyuan
	 * @description: 获取下一次时间
	 * @return 下一次时间
	 * @throws
	 */
	public LocalDateTime getNextTime() {
		return nextTime;
	}

	/**
	 * @title: setNextTime
	 * @author: zhongjyuan
	 * @description: 设置下一次时间
	 * @param nextTime 下一次时间
	 * @throws
	 */
	public void setNextTime(LocalDateTime nextTime) {
		this.nextTime = nextTime;
	}

	/**
	 * @title: getResult
	 * @author: zhongjyuan
	 * @description: 获取结果
	 * @return 结果
	 * @throws
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @title: setResult
	 * @author: zhongjyuan
	 * @description: 设置结果
	 * @param result 结果
	 * @throws
	 */
	public void setResult(String result) {
		this.result = result;
	}
}
