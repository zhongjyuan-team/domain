package zhongjyuan.domain.job;

import java.time.LocalDateTime;
import java.util.List;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.ConfigurationModel;
import zhongjyuan.domain.IModel;

/**
 * @className: JobModel
 * @description: 工作模型对象
 * @author: zhongjyuan
 * @date: 2023年2月3日 下午2:51:28
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class JobModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields type: 类型[1:CLASS;2:EXTERNAL_API;3:REST_API]
	 */
	protected Integer type;

	/**
	 * @fields beanClass: 执行调用类(类型1)
	 */
	protected String beanClass;

	/**
	 * @fields serverCode: 执行调用服务(类型2)
	 */
	protected String serverCode;

	/**
	 * @fields method: 执行调用方式(类型2)
	 */
	protected String method;

	/**
	 * @fields remotePath: 执行调用路径(类型2)
	 */
	protected String remotePath;

	/**
	 * @fields remoteUrl: 执行调用地址(类型3)
	 */
	protected String remoteUrl;

	/**
	 * @fields cronExpression: 调度表达式
	 */
	protected String cronExpression;

	/**
	 * @fields groupId: 分组主键
	 */
	protected String groupId;

	/**
	 * @fields groupNo: 分组编号
	 */
	protected String groupNo;

	/**
	 * @fields groupName: 分组名称
	 */
	protected String groupName;

	/**
	 * @fields status: 状态[1:未执行;2:执行中;3:等待执行;4:执行异常;5:未启用]
	 */
	protected Integer status;

	/**
	 * @fields isSystem: 是否系统
	 */
	protected Boolean isSystem;

	/**
	 * @fields isEnabled: 是否有效
	 */
	protected Boolean isEnabled;

	/**
	 * @fields isDeleted: 是否删除
	 */
	protected Boolean isDeleted;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @fields description: 描述
	 */
	protected String description;

	/**
	 * @fields creator: 创建者
	 */
	protected String creator;

	/**
	 * @fields creatorName: 创建者名称
	 */
	protected String creatorName;

	/**
	 * @fields createTime: 创建时间
	 */
	protected LocalDateTime createTime;

	/**
	 * @fields updator: 更新者
	 */
	protected String updator;

	/**
	 * @fields updatorName: 更新者名称
	 */
	protected String updatorName;

	/**
	 * @fields updateTime: 更新时间
	 */
	protected LocalDateTime updateTime;

	/**
	 * @fields configurations: 配置集合
	 */
	protected List<ConfigurationModel> configurations;

	/**
	 * @title:  getId
	 * @description: 获取主键
	 * @return: 主键
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title:  setId
	 * @description: 设置主键
	 * @param id 主键
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title:  getCode
	 * @description: 获取编码
	 * @return: 编码
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title:  setCode
	 * @description: 设置编码
	 * @param code 编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title:  getName
	 * @description: 获取名称
	 * @return: 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title:  setName
	 * @description: 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title:  getType
	 * @description: 获取类型
	 * @return: 类型[1:CLASS;2:EXTERNAL_API;3:REST_API]
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @title:  setType
	 * @description: 设置类型
	 * @param type 类型[1:CLASS;2:EXTERNAL_API;3:REST_API]
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @title:  getBeanClass
	 * @description: 获取执行调用类
	 * @return: 执行调用类
	 */
	public String getBeanClass() {
		return beanClass;
	}

	/**
	 * @title:  setBeanClass
	 * @description: 设置执行调用类
	 * @param beanClass 执行调用类
	 */
	public void setBeanClass(String beanClass) {
		this.beanClass = beanClass;
	}

	/**
	 * @title:  getServerCode
	 * @description: 获取执行调用服务
	 * @return: 执行调用服务
	 */
	public String getServerCode() {
		return serverCode;
	}

	/**
	 * @title:  setServerCode
	 * @description: 设置执行调用服务
	 * @param serverCode 执行调用服务
	 */
	public void setServerCode(String serverCode) {
		this.serverCode = serverCode;
	}

	/**
	 * @title:  getMethod
	 * @description: 获取执行调用方式
	 * @return: 执行调用方式
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @title:  setMethod
	 * @description: 设置执行调用方式
	 * @param method 执行调用方式
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @title:  getRemotePath
	 * @description: 获取执行调用路径
	 * @return: 执行调用路径
	 */
	public String getRemotePath() {
		return remotePath;
	}

	/**
	 * @title:  setRemotePath
	 * @description: 设置执行调用路径
	 * @param remotePath 执行调用路径
	 */
	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}

	/**
	 * @title:  getRemoteUrl
	 * @description: 获取执行调用地址
	 * @return: 执行调用地址
	 */
	public String getRemoteUrl() {
		return remoteUrl;
	}

	/**
	 * @title:  setRemoteUrl
	 * @description: 设置执行调用地址
	 * @param remoteUrl 执行调用地址
	 */
	public void setRemoteUrl(String remoteUrl) {
		this.remoteUrl = remoteUrl;
	}

	/**
	 * @title:  getCronExpression
	 * @description: 获取调度表达式
	 * @return: 调度表达式
	 */
	public String getCronExpression() {
		return cronExpression;
	}

	/**
	 * @title:  setCronExpression
	 * @description: 设置调度表达式
	 * @param cronExpression 调度表达式
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	/**
	 * @title:  getGroupId
	 * @description: 获取分组主键
	 * @return: 分组主键
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @title:  setGroupId
	 * @description: 设置分组主键
	 * @param groupId 分组主键
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @title:  getGroupNo
	 * @description: 获取分组编码
	 * @return: 分组编码
	 */
	public String getGroupNo() {
		return groupNo;
	}

	/**
	 * @title:  setGroupNo
	 * @description: 设置分组编码
	 * @param groupNo 分组编码
	 */
	public void setGroupNo(String groupNo) {
		this.groupNo = groupNo;
	}

	/**
	 * @title:  getGroupName
	 * @description: 获取分组名称
	 * @return: 分组名称
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @title:  setGroupName
	 * @description: 设置分组名称
	 * @param groupName 分组名称
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @title:  getStatus
	 * @description: 获取状态
	 * @return: 状态[1:未执行;2:执行中;3:等待执行;4:执行异常;5:未启用]
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @title:  setStatus
	 * @description: 设置状态
	 * @param status 状态[1:未执行;2:执行中;3:等待执行;4:执行异常;5:未启用]
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @title:  getIsSystem
	 * @description: 是否系统
	 * @return: 是否系统
	 */
	public Boolean getIsSystem() {
		return isSystem;
	}

	/**
	 * @title:  setIsSystem
	 * @description: 设置是否系统
	 * @param isSystem 是否系统
	 */
	public void setIsSystem(Boolean isSystem) {
		this.isSystem = isSystem;
	}

	/**
	 * @title:  getIsEnabled
	 * @description: 是否启用
	 * @return: 是否启用
	 */
	public Boolean getIsEnabled() {
		return isEnabled;
	}

	/**
	 * @title:  setIsEnabled
	 * @description: 设置是否启用
	 * @param isEnabled 是否启用
	 */
	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @title:  getIsDeleted
	 * @description: 是否移除
	 * @return: 是否移除
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @title:  setIsDeleted
	 * @description: 设置是否移除
	 * @param isDeleted 是否移除
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @title:  getRowIndex
	 * @description: 获取行号
	 * @return: 行号
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title:  setRowIndex
	 * @description: 设置行号
	 * @param rowIndex 行号
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title:  getDescription
	 * @description: 获取描述
	 * @return: 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title:  setDescription
	 * @description: 设置描述
	 * @param description 描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @title:  getCreator
	 * @description: 获取创建者主键
	 * @return: 创建者主键
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @title:  setCreator
	 * @description: 设置创建者主键
	 * @param creator 创建者主键
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @title:  getCreatorName
	 * @description: 获取创建者名称
	 * @return: 创建者名称
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @title:  setCreatorName
	 * @description: 设置创建者名称
	 * @param creatorName 创建者名称
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @title:  getCreateTime
	 * @description: 获取创建时间
	 * @return: 创建时间
	 */
	public LocalDateTime getCreateTime() {
		return createTime;
	}

	/**
	 * @title:  setCreateTime
	 * @description: 设置创建时间
	 * @param createTime 创建时间
	 */
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	/**
	 * @title:  getUpdator
	 * @description: 获取更新者主键
	 * @return: 更新者主键
	 */
	public String getUpdator() {
		return updator;
	}

	/**
	 * @title:  setUpdator
	 * @description: 设置更新者主键
	 * @param updator 更新者主键
	 */
	public void setUpdator(String updator) {
		this.updator = updator;
	}

	/**
	 * @title:  getUpdatorName
	 * @description: 获取更新者名称
	 * @return: 更新者名称
	 */
	public String getUpdatorName() {
		return updatorName;
	}

	/**
	 * @title:  setUpdatorName
	 * @description: 设置更新者名称
	 * @param updatorName 更新者名称
	 */
	public void setUpdatorName(String updatorName) {
		this.updatorName = updatorName;
	}

	/**
	 * @title:  getUpdateTime
	 * @description: 获取更新时间
	 * @return: 更新时间
	 */
	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	/**
	 * @title:  setUpdateTime
	 * @description: 设置更新时间
	 * @param updateTime 更新时间
	 */
	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @title:  getConfigurations
	 * @description: 获取配置集
	 * @return: 配置集
	 */
	public List<ConfigurationModel> getConfigurations() {
		return configurations;
	}

	/**
	 * @title:  setConfigurations
	 * @description: 设置配置集
	 * @param configurations 配置集
	 */
	public void setConfigurations(List<ConfigurationModel> configurations) {
		this.configurations = configurations;
	}

	/**
	 * @title: setConfiguration
	 * @author: zhongjyuan
	 * @description: 设置配置
	 * @param configuration 配置对象
	 * @throws
	 */
	public void setConfiguration(ConfigurationModel configuration) {
		configurations.add(configuration);
	}
}
