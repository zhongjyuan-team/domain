package zhongjyuan.domain.job;

import zhongjyuan.domain.IModel;

/**
 * @className: JobStatus
 * @description: 任务状态枚举对象
 * @author: zhongjyuan
 * @date: 2020年3月28日 下午3:59:14
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum JobStatus implements IModel {

	/**
	 * @fields UNEXECUTED: 未执行
	 */
	UNEXECUTED(1, "未执行"),

	/**
	 * @fields RUNNING: 执行中
	 */
	RUNNING(2, "执行中"),

	/**
	 * @fields WSITING: 等待执行
	 */
	WSITING(3, "等待执行"),

	/**
	 * @fields EXCEPTION: 执行异常
	 */
	EXCEPTION(4, "执行异常"),

	/**
	 * @fields DISABLE: 未启用
	 */
	DISABLE(5, "未启用"),

	;

	/**
	 * @fields code: 编码
	 */
	private Integer code;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @title: JobStatus
	 * @author: zhongjyuan
	 * @description: 任务状态枚举对象
	 * @param code 编码
	 * @param name 名称
	 * @throws
	 */
	JobStatus(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}
}
