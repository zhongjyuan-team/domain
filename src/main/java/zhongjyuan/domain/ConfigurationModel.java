package zhongjyuan.domain;

/**
 * @className: Configuration
 * @description: 动态配置对象
 * @author: zhongjyuan
 * @date: 2022年4月7日 下午6:10:33
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ConfigurationModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields key: 键
	 */
	protected String key;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields type: 类型[1:单行文本|2:整数|3:小数|4:时间|5:多行文本|6:boolean|7:radio|8:checkbox|9:combobox|10:subform]
	 */
	protected String type = "1";

	/**
	 * @fields value: 值
	 */
	protected String value;

	/**
	 * @fields example: 示例
	 */
	protected String example;

	/**
	 * @fields options: 选项集
	 */
	protected String options;

	/**
	 * @fields template: 表单模板
	 */
	protected String template;

	/**
	 * @fields full: 独占一行[默认false]
	 */
	protected boolean full = false;

	/**
	 * @fields required: 是否必要[默认false]
	 */
	protected boolean required = false;

	/**
	 * @fields multiple: 多选/多条[默认false]
	 */
	protected boolean multiple = false;

	/**
	 * @fields hidden: 是否隐藏[默认false]
	 */
	protected boolean hidden = false;

	/**
	 * @fields hiddenCondition: 隐藏条件
	 */
	protected String hiddenCondition = "";

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @throws
	 */
	public ConfigurationModel() {

	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param full 是否独占一行
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, boolean full, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.full = full;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param full 是否独占一行
	 * @param required 是否必要
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, boolean full, boolean required, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.full = full;
		this.required = required;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param full 是否独占一行
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, boolean full, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.full = full;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param full 是否独占一行
	 * @param required 是否必要
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, boolean full, boolean required, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.full = full;
		this.required = required;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param full 是否独占一行
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, boolean full, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.full = full;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param full 是否独占一行
	 * @param required 是否必要
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, boolean full, boolean required, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.full = full;
		this.required = required;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param example 示例
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, String example, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.example = example;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param example 示例
	 * @param full 是否独占一行
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, String example, boolean full, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.example = example;
		this.full = full;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title Configuration
	 * @author zhongjyuan
	 * @description 动态配置对象
	 * @param code 编码
	 * @param name 名称
	 * @param type 类型
	 * @param value 值
	 * @param example 示例
	 * @param full 是否独占一行
	 * @param required 是否必要
	 * @param rowIndex 行号
	 * @throws
	 */
	public ConfigurationModel(String code, String name, String type, String value, String example, boolean full, boolean required, Integer rowIndex) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.value = value;
		this.example = example;
		this.full = full;
		this.required = required;
		this.rowIndex = rowIndex;
	}

	/**
	 * @title: getKey
	 * @author: zhongjyuan
	 * @description: 获取键
	 * @return 键
	 * @throws
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title: setKey
	 * @author: zhongjyuan
	 * @description: 设置键
	 * @param key 键
	 * @throws
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws 
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getType
	 * @author: zhongjyuan
	 * @description: 获取类型
	 * @return 类型
	 * @throws
	 */
	public String getType() {
		return type;
	}

	/**
	 * @title: setType
	 * @author: zhongjyuan
	 * @description: 设置类型
	 * @param type 类型
	 * @throws
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @title: getValue
	 * @author: zhongjyuan
	 * @description: 获取值
	 * @return 值
	 * @throws
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @title: setValue
	 * @author: zhongjyuan
	 * @description: 设置值
	 * @param value 值
	 * @throws
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @title: getExample
	 * @author: zhongjyuan
	 * @description: 获取示例
	 * @return 示例
	 * @throws
	 */
	public String getExample() {
		return example;
	}

	/**
	 * @title: setExample
	 * @author: zhongjyuan
	 * @description: 设置示例
	 * @param example 示例
	 * @throws
	 */
	public void setExample(String example) {
		this.example = example;
	}

	/**
	 * @title: getOptions
	 * @author: zhongjyuan
	 * @description: 获取选项集
	 * @return 选项集
	 * @throws
	 */
	public String getOptions() {
		return options;
	}

	/**
	 * @title: setOptions
	 * @author: zhongjyuan
	 * @description: 设置选项集
	 * @param options 选项集
	 * @throws
	 */
	public void setOptions(String options) {
		this.options = options;
	}

	/**
	 * @title: getTemplate
	 * @author: zhongjyuan
	 * @description: 获取模版
	 * @return 模版
	 * @throws
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @title: setTemplate
	 * @author: zhongjyuan
	 * @description: 设置模版
	 * @param template 模版
	 * @throws
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @title: isFull
	 * @author: zhongjyuan
	 * @description: 是否整行
	 * @return 是否整行
	 * @throws
	 */
	public boolean isFull() {
		return full;
	}

	/**
	 * @title: setFull
	 * @author: zhongjyuan
	 * @description: 设置是否整行
	 * @param full 是否整行
	 * @throws
	 */
	public void setFull(boolean full) {
		this.full = full;
	}

	/**
	 * @title: isRequired
	 * @author: zhongjyuan
	 * @description: 是否必要
	 * @return 是否必要
	 * @throws
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @title: setRequired
	 * @author: zhongjyuan
	 * @description: 设置是否必要
	 * @param required 是否必要
	 * @throws
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * @title: isMultiple
	 * @author: zhongjyuan
	 * @description: 是否多条
	 * @return 是否多条
	 * @throws
	 */
	public boolean isMultiple() {
		return multiple;
	}

	/**
	 * @title: setMultiple
	 * @author: zhongjyuan
	 * @description: 设置是否多条
	 * @param multiple 是否多条
	 * @throws
	 */
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	/**
	 * @title: isHidden
	 * @author: zhongjyuan
	 * @description: 是否隐藏
	 * @return 是否隐藏
	 * @throws
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * @title: setHidden
	 * @author: zhongjyuan
	 * @description: 设置是否隐藏
	 * @param hidden 是否隐藏
	 * @throws
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * @title: getHiddenCondition
	 * @author: zhongjyuan
	 * @description: 获取隐藏条件
	 * @return 隐藏条件
	 * @throws
	 */
	public String getHiddenCondition() {
		return hiddenCondition;
	}

	/**
	 * @title: setHiddenCondition
	 * @author: zhongjyuan
	 * @description: 设置隐藏条件
	 * @param hiddenCondition 隐藏条件
	 * @throws
	 */
	public void setHiddenCondition(String hiddenCondition) {
		this.hiddenCondition = hiddenCondition;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @className: Builder
	 * @description: 动态配置构造者
	 * @author: zhongjyuan
	 * @date: 2022年12月2日 下午2:31:37
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class Builder {

		/**
		 * @fields key: 键
		 */
		private String key;

		/**
		 * @fields code: 编码
		 */
		private String code;

		/**
		 * @fields name: 名称
		 */
		private String name;

		/**
		 * @fields type: 类型[1:单行文本|2:整数|3:小数|4:时间|5:多行文本|6:boolean|7:radio|8:checkbox|9:combobox|10:subform]
		 */
		private String type = "1";

		/**
		 * @fields value: 值
		 */
		private String value;

		/**
		 * @fields example: 示例
		 */
		private String example;

		/**
		 * @fields options: 选项集
		 */
		private String options;

		/**
		 * @fields template: 表单模板
		 */
		private String template;

		/**
		 * @fields full: 独占一行[默认false]
		 */
		private boolean full = false;

		/**
		 * @fields required: 是否必要[默认false]
		 */
		private boolean required = false;

		/**
		 * @fields multiple: 多选/多条[默认false]
		 */
		private boolean multiple = false;

		/**
		 * @fields hidden: 是否隐藏[默认false]
		 */
		private boolean hidden = false;

		/**
		 * @fields hiddenCondition: 隐藏条件
		 */
		private String hiddenCondition = "";

		/**
		 * @fields rowIndex: 行号
		 */
		private Integer rowIndex;

		/**
		 * @title: setKey
		 * @author: zhongjyuan
		 * @description: 设置键
		 * @param key 键
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setKey(String key) {
			this.key = key;
			return this;
		}

		/**
		 * @title: setCode
		 * @author: zhongjyuan
		 * @description: 设置编码
		 * @param code 编码
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setCode(String code) {
			this.code = code;
			return this;
		}

		/**
		 * @title: setName
		 * @author: zhongjyuan
		 * @description: 设置名称
		 * @param name 名称 
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		/**
		 * @title: setType
		 * @author: zhongjyuan
		 * @description: 设置类型
		 * @param type 类型
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setType(String type) {
			this.type = type;
			return this;
		}

		/**
		 * @title: setValue
		 * @author: zhongjyuan
		 * @description: 设置值
		 * @param value 值
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setValue(String value) {
			this.value = value;
			return this;
		}

		/**
		 * @title: setExample
		 * @author: zhongjyuan
		 * @description: 设置示例
		 * @param example 示例
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setExample(String example) {
			this.example = example;
			return this;
		}

		/**
		 * @title: setOptions
		 * @author: zhongjyuan
		 * @description: 设置选项集
		 * @param options 选项集
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setOptions(String options) {
			this.options = options;
			return this;
		}

		/**
		 * @title: setTemplate
		 * @author: zhongjyuan
		 * @description: 设置模版
		 * @param template 模版
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setTemplate(String template) {
			this.template = template;
			return this;
		}

		/**
		 * @title: setFull
		 * @author: zhongjyuan
		 * @description: 设置整行
		 * @param full 整行
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setFull(boolean full) {
			this.full = full;
			return this;
		}

		/**
		 * @title: setRequired
		 * @author: zhongjyuan
		 * @description: 设置必要
		 * @param required 必要
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setRequired(boolean required) {
			this.required = required;
			return this;
		}

		/**
		 * @title: setMultiple
		 * @author: zhongjyuan
		 * @description: 设置多条
		 * @param multiple 多条
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setMultiple(boolean multiple) {
			this.multiple = multiple;
			return this;
		}

		/**
		 * @title: setHidden
		 * @author: zhongjyuan
		 * @description: 设置隐藏
		 * @param hidden 隐藏
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setHidden(boolean hidden) {
			this.hidden = hidden;
			return this;
		}

		/**
		 * @title: setHiddenCondition
		 * @author: zhongjyuan
		 * @description: 设置隐藏条件
		 * @param hiddenCondition 隐藏条件
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setHiddenCondition(String hiddenCondition) {
			this.hiddenCondition = hiddenCondition;
			return this;
		}

		/**
		 * @title: setRowIndex
		 * @author: zhongjyuan
		 * @description: 设置行号
		 * @param rowIndex 行号
		 * @return 动态配置构造者
		 * @throws
		 */
		public Builder setRowIndex(Integer rowIndex) {
			this.rowIndex = rowIndex;
			return this;
		}

		/**
		 * @title: build
		 * @author: zhongjyuan
		 * @description: 建造
		 * @return 动态配置构造者
		 * @throws
		 */
		public ConfigurationModel build() {
			ConfigurationModel configuration = new ConfigurationModel();

			configuration.setKey(key);
			configuration.setCode(code);
			configuration.setName(name);
			configuration.setType(type);
			configuration.setValue(value);

			configuration.setExample(example);
			configuration.setOptions(options);
			configuration.setTemplate(template);

			configuration.setFull(full);
			configuration.setRequired(required);
			configuration.setMultiple(multiple);

			configuration.setHidden(hidden);
			configuration.setHiddenCondition(hiddenCondition);

			configuration.setRowIndex(rowIndex);

			return configuration;
		}
	}
}
