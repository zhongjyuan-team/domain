package zhongjyuan.domain.message;

import java.time.LocalDateTime;

import zhongjyuan.domain.AbstractAttributeModel;
import zhongjyuan.domain.IAttributeModel;

/**
 * @className: InformationModel
 * @description: 信息模型对象
 * @author: zhongjyuan
 * @date: 2022年11月22日 下午3:02:34
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class InformationModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields title: 标题
	 */
	protected String title;

	/**
	 * @fields summary: 摘要
	 */
	protected String summary;

	/**
	 * @fields typeId: 消息类型主键
	 */
	protected String typeId;

	/**
	 * @fields typeCode: 消息类型编码
	 */
	protected String typeCode;

	/**
	 * @fields typeName: 消息类型名称
	 */
	protected String typeName;

	/**
	 * @fields dealTypeId: 处理类型主键
	 */
	protected String dealTypeId;

	/**
	 * @fields dealTypeCode: 处理类型编码
	 */
	protected String dealTypeCode;

	/**
	 * @fields dealTypeName: 处理类型名称
	 */
	protected String dealTypeName;

	/**
	 * @fields userId: 用户主键
	 */
	protected String userId;

	/**
	 * @fields userName: 用户名称
	 */
	protected String userName;

	/**
	 * @fields userAccount: 用户账号
	 */
	protected String userAccount;

	/**
	 * @fields toDealUrl: 待办地址[PC端]
	 */
	protected String toDealUrl;

	/**
	 * @fields toDealMobileUrl: 待办地址[Mobile端]
	 */
	protected String toDealMobileUrl;

	/**
	 * @fields doneUrl: 已办地址[PC端]
	 */
	protected String doneUrl;

	/**
	 * @fields doneMobileUrl: 已办地址[Mobile端]
	 */
	protected String doneMobileUrl;

	/**
	 * @fields toReadUrl: 待阅地址[PC端]
	 */
	protected String toReadUrl;

	/**
	 * @fields toReadMobileUrl: 待阅地址[Mobile端]
	 */
	protected String toReadMobileUrl;

	/**
	 * @fields readUrl: 已阅地址[PC端]
	 */
	protected String readUrl;

	/**
	 * @fields readMobileUrl: 已阅地址[Mobile端]
	 */
	protected String readMobileUrl;

	/**
	 * @fields browseUrl: 预览地址[PC端]
	 */
	protected String browseUrl;

	/**
	 * @fields browseMobileUrl: 预览地址[Mobile端]
	 */
	protected String browseMobileUrl;

	/**
	 * @fields systemId: 系统主键
	 */
	protected String systemId;

	/**
	 * @fields systemCode: 系统编码
	 */
	protected String systemCode;

	/**
	 * @fields systemName: 系统名称
	 */
	protected String systemName;

	/**
	 * @fields moduleId: 模块主键
	 */
	protected String moduleId;

	/**
	 * @fields moduleCode: 模块编码
	 */
	protected String moduleCode;

	/**
	 * @fields moduleName: 模块名称
	 */
	protected String moduleName;

	/**
	 * @fields businessId: 业务主键
	 */
	protected String businessId;

	/**
	 * @fields businessCode: 业务编码
	 */
	protected String businessCode;

	/**
	 * @fields businessName: 业务名称
	 */
	protected String businessName;

	/**
	 * @fields tenantId: 租户主键
	 */
	protected String tenantId;

	/**
	 * @fields tenantCode: 租户编码
	 */
	protected String tenantCode;

	/**
	 * @fields tenantName: 租户名称
	 */
	protected String tenantName;

	/**
	 * @fields creatorId: 创建主键
	 */
	protected String creatorId;

	/**
	 * @fields creatorName: 创建名称
	 */
	protected String creatorName;

	/**
	 * @fields createTime: 创建时间
	 */
	protected LocalDateTime createTime;

	/**
	 * @fields finishTime: 完成时间
	 */
	protected LocalDateTime finishTime;

	/**
	 * @title:  getId
	 * @description: 获取唯一标识
	 * @return: 唯一标识
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title:  setId
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title:  getTitle
	 * @description: 获取标题
	 * @return: 标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @title:  setTitle
	 * @description: 设置标题
	 * @param title 标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @title:  getSummary
	 * @description: 获取摘要
	 * @return: 摘要
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @title:  setSummary
	 * @description: 设置摘要
	 * @param summary 摘要
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @title:  getTypeId
	 * @description: 获取类型唯一标识
	 * @return: 类型唯一标识
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @title:  setTypeId
	 * @description: 设置类型唯一标识
	 * @param typeId 类型唯一标识
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @title:  getTypeCode
	 * @description: 获取类型编码
	 * @return: 类型编码
	 */
	public String getTypeCode() {
		return typeCode;
	}

	/**
	 * @title:  setTypeCode
	 * @description: 设置类型编码
	 * @param typeCode 类型编码
	 */
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	/**
	 * @title:  getTypeName
	 * @description: 获取类型名称
	 * @return: 类型名称
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @title:  setTypeName
	 * @description: 设置类型名称
	 * @param typeName 类型名称
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @title:  getDealTypeId
	 * @description: 获取处理类型唯一标识
	 * @return: 处理类型唯一标识
	 */
	public String getDealTypeId() {
		return dealTypeId;
	}

	/**
	 * @title:  setDealTypeId
	 * @description: 设置处理类型唯一标识
	 * @param dealTypeId 处理类型唯一标识
	 */
	public void setDealTypeId(String dealTypeId) {
		this.dealTypeId = dealTypeId;
	}

	/**
	 * @title:  getDealTypeCode
	 * @description: 获取处理类型编码
	 * @return: 处理类型编码
	 */
	public String getDealTypeCode() {
		return dealTypeCode;
	}

	/**
	 * @title:  setDealTypeCode
	 * @description: 设置处理类型编码
	 * @param dealTypeCode 处理类型编码
	 */
	public void setDealTypeCode(String dealTypeCode) {
		this.dealTypeCode = dealTypeCode;
	}

	/**
	 * @title:  getDealTypeName
	 * @description: 获取处理类型名称
	 * @return: 处理类型名称
	 */
	public String getDealTypeName() {
		return dealTypeName;
	}

	/**
	 * @title:  setDealTypeName
	 * @description: 设置处理类型名称
	 * @param dealTypeName 处理类型名称
	 */
	public void setDealTypeName(String dealTypeName) {
		this.dealTypeName = dealTypeName;
	}

	/**
	 * @title:  getUserId
	 * @description: 获取用户唯一标识
	 * @return: 用户唯一标识
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @title:  setUserId
	 * @description: 设置用户唯一标识
	 * @param userId 用户唯一标识
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @title:  getUserName
	 * @description: 获取用户名称
	 * @return: 用户名称
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @title:  setUserName
	 * @description: 设置用户名称
	 * @param userName 用户名称
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @title:  getUserAccount
	 * @description: 获取用户账号
	 * @return: 用户账号
	 */
	public String getUserAccount() {
		return userAccount;
	}

	/**
	 * @title:  setUserAccount
	 * @description: 设置用户账号
	 * @param userAccount 用户账号
	 */
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	/**
	 * @title:  getToDealUrl
	 * @description: 获取PC待办路径
	 * @return: PC待办路径
	 */
	public String getToDealUrl() {
		return toDealUrl;
	}

	/**
	 * @title:  setToDealUrl
	 * @description: 设置PC待办路径
	 * @param toDealUrl PC待办路径
	 */
	public void setToDealUrl(String toDealUrl) {
		this.toDealUrl = toDealUrl;
	}

	/**
	 * @title:  getToDealMobileUrl
	 * @description: 获取Mobile待办路径
	 * @return: Mobile待办路径
	 */
	public String getToDealMobileUrl() {
		return toDealMobileUrl;
	}

	/**
	 * @title:  setToDealMobileUrl
	 * @description: 设置Mobile待办路径
	 * @param toDealMobileUrl Mobile待办路径
	 */
	public void setToDealMobileUrl(String toDealMobileUrl) {
		this.toDealMobileUrl = toDealMobileUrl;
	}

	/**
	 * @title:  getDoneUrl
	 * @description: 获取PC已办路径
	 * @return: PC已办路径
	 */
	public String getDoneUrl() {
		return doneUrl;
	}

	/**
	 * @title:  setDoneUrl
	 * @description: 设置PC已办路径
	 * @param doneUrl PC已办路径
	 */
	public void setDoneUrl(String doneUrl) {
		this.doneUrl = doneUrl;
	}

	/**
	 * @title:  getDoneMobileUrl
	 * @description: 获取Mobile已办地址
	 * @return: Mobile已办地址
	 */
	public String getDoneMobileUrl() {
		return doneMobileUrl;
	}

	/**
	 * @title:  setDoneMobileUrl
	 * @description: 设置Mobile已办地址
	 * @param doneMobileUrl Mobile已办地址
	 */
	public void setDoneMobileUrl(String doneMobileUrl) {
		this.doneMobileUrl = doneMobileUrl;
	}

	/**
	 * @title:  getToReadUrl
	 * @description: 获取PC待阅路径
	 * @return: PC待阅路径
	 */
	public String getToReadUrl() {
		return toReadUrl;
	}

	/**
	 * @title:  setToReadUrl
	 * @description: 设置PC待阅路径
	 * @param toReadUrl PC待阅路径
	 */
	public void setToReadUrl(String toReadUrl) {
		this.toReadUrl = toReadUrl;
	}

	/**
	 * @title:  getToReadMobileUrl
	 * @description: 获取Mobile待阅路径
	 * @return: Mobile待阅路径
	 */
	public String getToReadMobileUrl() {
		return toReadMobileUrl;
	}

	/**
	 * @title:  setToReadMobileUrl
	 * @description: 设置Mobile待阅路径
	 * @param toReadMobileUrl Mobile待阅路径
	 */
	public void setToReadMobileUrl(String toReadMobileUrl) {
		this.toReadMobileUrl = toReadMobileUrl;
	}

	/**
	 * @title:  getReadUrl
	 * @description: 获取PC已阅路径
	 * @return: PC已阅路径
	 */
	public String getReadUrl() {
		return readUrl;
	}

	/**
	 * @title:  setReadUrl
	 * @description: 设置PC已阅路径
	 * @param readUrl PC已阅路径
	 */
	public void setReadUrl(String readUrl) {
		this.readUrl = readUrl;
	}

	/**
	 * @title:  getReadMobileUrl
	 * @description: 获取Mobile已阅路径
	 * @return: Mobile已阅路径
	 */
	public String getReadMobileUrl() {
		return readMobileUrl;
	}

	/**
	 * @title:  setReadMobileUrl
	 * @description: 设置Mobile已阅路径
	 * @param readMobileUrl Mobile已阅路径
	 */
	public void setReadMobileUrl(String readMobileUrl) {
		this.readMobileUrl = readMobileUrl;
	}

	/**
	 * @title:  getBrowseUrl
	 * @description: 获取PC预览路径
	 * @return: PC预览路径
	 */
	public String getBrowseUrl() {
		return browseUrl;
	}

	/**
	 * @title:  setBrowseUrl
	 * @description: 设置PC预览路径
	 * @param browseUrl PC预览路径
	 */
	public void setBrowseUrl(String browseUrl) {
		this.browseUrl = browseUrl;
	}

	/**
	 * @title:  getBrowseMobileUrl
	 * @description: 获取Mobile预览路径
	 * @return: Mobile预览路径
	 */
	public String getBrowseMobileUrl() {
		return browseMobileUrl;
	}

	/**
	 * @title:  setBrowseMobileUrl
	 * @description: 设置Mobile预览路径
	 * @param browseMobileUrl Mobile预览路径
	 */
	public void setBrowseMobileUrl(String browseMobileUrl) {
		this.browseMobileUrl = browseMobileUrl;
	}

	/**
	 * @title:  getSystemId
	 * @description: 获取系统唯一标识
	 * @return: 系统唯一标识
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @title:  setSystemId
	 * @description: 设置系统唯一标识
	 * @param systemId 系统唯一标识
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @title:  getSystemCode
	 * @description: 获取系统编码
	 * @return: 系统编码
	 */
	public String getSystemCode() {
		return systemCode;
	}

	/**
	 * @title:  setSystemCode
	 * @description: 设置系统编码
	 * @param systemCode 系统编码
	 */
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	/**
	 * @title:  getSystemName
	 * @description: 获取系统名称
	 * @return: 系统名称
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @title:  setSystemName
	 * @description: 设置系统名称
	 * @param systemName 系统名称
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @title:  getModuleId
	 * @description: 获取模块唯一标识
	 * @return: 模块唯一标识
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @title:  setModuleId
	 * @description: 设置模块唯一标识
	 * @param moduleId 模块唯一标识
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @title:  getModuleCode
	 * @description: 获取模块编码
	 * @return: 模块编码
	 */
	public String getModuleCode() {
		return moduleCode;
	}

	/**
	 * @title:  setModuleCode
	 * @description: 设置模块编码
	 * @param moduleCode 模块编码
	 */
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	/**
	 * @title:  getModuleName
	 * @description: 获取模块名称
	 * @return: 模块名称
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * @title:  setModuleName
	 * @description: 设置模块名称
	 * @param moduleName 模块名称
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * @title:  getBusinessId
	 * @description: 获取业务唯一标识
	 * @return: 业务唯一标识
	 */
	public String getBusinessId() {
		return businessId;
	}

	/**
	 * @title:  setBusinessId
	 * @description: 设置业务唯一标识
	 * @param businessId 业务唯一标识
	 */
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	/**
	 * @title:  getBusinessCode
	 * @description: 获取业务编码
	 * @return: 业务编码
	 */
	public String getBusinessCode() {
		return businessCode;
	}

	/**
	 * @title:  setBusinessCode
	 * @description: 设置业务编码
	 * @param businessCode 业务编码
	 */
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

	/**
	 * @title:  getBusinessName
	 * @description: 获取业务名称
	 * @return: 业务名称
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @title:  setBusinessName
	 * @description: 设置业务名称
	 * @param businessName 业务名称
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @title:  getTenantId
	 * @description: 获取租户唯一标识
	 * @return: 租户唯一标识
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @title:  setTenantId
	 * @description: 设置租户唯一标识
	 * @param tenantId 租户唯一标识
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @title:  getTenantCode
	 * @description: 获取租户编码
	 * @return: 租户编码
	 */
	public String getTenantCode() {
		return tenantCode;
	}

	/**
	 * @title:  setTenantCode
	 * @description: 设置租户编码
	 * @param tenantCode 租户编码
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	/**
	 * @title:  getTenantName
	 * @description: 获取租户名称
	 * @return: 租户名称
	 */
	public String getTenantName() {
		return tenantName;
	}

	/**
	 * @title:  setTenantName
	 * @description: 设置租户名称
	 * @param tenantName 租户名称
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	/**
	 * @title:  getCreatorId
	 * @description: 获取创建者唯一标识
	 * @return: 创建者唯一标识
	 */
	public String getCreatorId() {
		return creatorId;
	}

	/**
	 * @title:  setCreatorId
	 * @description: 设置创建者唯一标识
	 * @param creatorId 创建者唯一标识
	 */
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	/**
	 * @title:  getCreatorName
	 * @description: 获取创建者名称
	 * @return: 创建者名称
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @title:  setCreatorName
	 * @description: 设置创建者名称
	 * @param creatorName 创建者名称
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @title:  getCreateTime
	 * @description: 获取创建时间
	 * @return: 创建时间
	 */
	public LocalDateTime getCreateTime() {
		return createTime;
	}

	/**
	 * @title:  setCreateTime
	 * @description: 设置创建时间
	 * @param createTime 创建时间
	 */
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	/**
	 * @title:  getFinishTime
	 * @description: 获取完成时间
	 * @return: 完成时间
	 */
	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	/**
	 * @title:  setFinishTime
	 * @description: 设置完成时间
	 * @param finishTime 完成时间
	 */
	public void setFinishTime(LocalDateTime finishTime) {
		this.finishTime = finishTime;
	}
}
