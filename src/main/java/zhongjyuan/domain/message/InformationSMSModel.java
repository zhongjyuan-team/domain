package zhongjyuan.domain.message;

import zhongjyuan.domain.AbstractAttributeModel;
import zhongjyuan.domain.IAttributeModel;

/**
 * @className: InformationSMSModel
 * @description: 短信信息对象
 * @author: zhongjyuan
 * @date: 2022年12月9日 上午11:14:47
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class InformationSMSModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields sign: 签名
	 */
	protected String sign;

	/**
	 * @fields type: 类型
	 */
	protected String type;

	/**
	 * @fields mobile: 手机号
	 */
	protected String mobile;

	/**
	 * @fields content: 正文
	 */
	protected String content;

	/**
	 * @fields template: 模版
	 */
	protected String template;

	/**
	 * @title:  getSign
	 * @description: 获取签名
	 * @return: 签名
	 */
	public String getSign() {
		return sign;
	}

	/**
	 * @title:  setSign
	 * @description: 设置签名
	 * @param sign 签名
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}

	/**
	 * @title:  getType
	 * @description: 获取类型
	 * @return: 类型
	 */
	public String getType() {
		return type;
	}

	/**
	 * @title:  setType
	 * @description: 设置类型
	 * @param type 类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @title:  getMobile
	 * @description: 获取手机号
	 * @return: 手机号
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @title:  setMobile
	 * @description: 设置手机号
	 * @param mobile 手机号
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @title:  getContent
	 * @description: 获取正文
	 * @return: 正文
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @title:  setContent
	 * @description: 设置正文
	 * @param content 正文
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @title:  getTemplate
	 * @description: 获取模版
	 * @return: 模版
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @title:  setTemplate
	 * @description: 设置模版
	 * @param template 模版
	 */
	public void setTemplate(String template) {
		this.template = template;
	}
}
