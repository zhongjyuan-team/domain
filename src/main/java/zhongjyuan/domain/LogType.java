package zhongjyuan.domain;

/**
 * @className: LogType
 * @description: 日志类型. 实现{@link IEnum}
 * @author: zhongjyuan
 * @date: 2023年1月4日 上午11:38:21
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum LogType implements IEnum {

	/**
	 * 访问日志
	 */
	ACCESS,

	/**
	 * 操作日志
	 */
	OPERATE
}
