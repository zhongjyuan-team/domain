package zhongjyuan.domain;

/**
 * @className: AccountModel
 * @description: 用户账户信息的模型。拓展{@link AbstractAttributeModel}类; 实现{@link IAttributeModel}接口
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午12:33:00
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class AccountModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String account;

	private String nickName;

	private String sex;

	private String email;

	private String mobile;

	private String telephone;

	/**
	 * 获取账户ID。
	 * @return 账户ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置账户ID。
	 * @param id 要设置的账户ID
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取用户的全名。
	 * @return 用户的全名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置用户的全名。
	 * @param name 要设置的用户全名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获取用户的昵称。
	 * @return 用户的昵称
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * 设置用户的昵称。
	 * @param nickName 要设置的用户昵称
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * 获取用户的账户名。
	 * @return 用户的账户名
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * 设置用户的账户名。
	 * @param account 要设置的用户账户名
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * 获取用户的性别。
	 * @return 用户的性别
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * 设置用户的性别。
	 * @param sex 要设置的用户性别
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * 获取用户的电子邮箱地址。
	 * @return 用户的电子邮箱地址
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * 设置用户的电子邮箱地址。
	 * @param email 要设置的用户电子邮箱地址
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 获取用户的手机号码。
	 * @return 用户的手机号码
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * 设置用户的手机号码。
	 * @param mobile 要设置的用户手机号码
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * 获取用户的电话号码。
	 * @return 用户的电话号码
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * 设置用户的电话号码。
	 * @param telephone 要设置的用户电话号码
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
}
