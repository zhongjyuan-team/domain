package zhongjyuan.domain.license;

import java.time.LocalDateTime;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: AppLicenseModel
 * @description: 应用License授权模型对象
 * @author: zhongjyuan
 * @date: 2022年10月26日 上午9:46:12
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class AppLicenseModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields appId: 应用主键
	 */
	private String appId;

	/**
	 * @fields appCode: 应用编码
	 */
	private String appCode;

	/**
	 * @fields appName: 应用名称
	 */
	private String appName;

	/**
	 * @fields maxAccessUser: 最大授权用户数
	 */
	private Integer maxAccessUser;

	/**
	 * @fields maxAccessMenu: 最大授权菜单数
	 */
	private Integer maxAccessMenu;

	/**
	 * @fields maxAccessRole: 最大授权角色数
	 */
	private Integer maxAccessRole;

	/**
	 * @fields maxAccessModule: 最大授权模块数
	 */
	private Integer maxAccessModule;

	/**
	 * @fields maxAccessModel: 最大授权模型数
	 */
	private Integer maxAccessModel;

	/**
	 * @fields maxAccessPage: 最大授权页面数
	 */
	private Integer maxAccessPage;

	/**
	 * @fields maxAccessProject: 最大授权项目数
	 */
	private Integer maxAccessProject;

	/**
	 * @fields maxAccessStructure: 最大授权架构数
	 */
	private Integer maxAccessStructure;

	/**
	 * @fields maxAccessDataSource: 最大授权数据源数
	 */
	private Integer maxAccessDataSource;

	/**
	 * @fields maxConcurrencyUser: 最大并发用户数
	 */
	private Integer maxConcurrencyUser;

	/**
	 * @fields remindDate: 提醒开始时间
	 */
	private LocalDateTime remindDate;

	/**
	 * @fields expiredDate: 失效开始时间
	 */
	private LocalDateTime expiredDate;

	/**
	 * @fields shutdownDate: 停止开始时间
	 */
	private LocalDateTime shutdownDate;

	/**
	 * @title LicenseApp
	 * @author zhongjyuan
	 * @description License授权应用模型对象
	 * @throws
	 */
	public AppLicenseModel() {

	}

	/**
	 * @title LicenseApp
	 * @author zhongjyuan
	 * @description License授权应用模型对象
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @throws
	 */
	public AppLicenseModel(String appId, String appCode, String appName) {
		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);
	}

	/**
	 * @title LicenseApp
	 * @author zhongjyuan
	 * @description License授权应用模型对象
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param maxAccessUser 最大授权用户数
	 * @param maxAccessMenu 最大授权菜单数
	 * @param maxAccessRole 最大授权角色数
	 * @param maxAccessModule 最大授权模块数
	 * @param maxAccessModel 最大授权模型数
	 * @param maxAccessPage 最大授权页面数
	 * @param maxAccessProject 最大授权项目数
	 * @param maxAccessStructure 最大授权架构数
	 * @param maxAccessDataSource 最大授权数据源数
	 * @throws
	 */
	public AppLicenseModel(String appId, String appCode, String appName, Integer maxAccessUser, Integer maxAccessMenu, Integer maxAccessRole, Integer maxAccessModule, Integer maxAccessModel, Integer maxAccessPage, Integer maxAccessProject, Integer maxAccessStructure, Integer maxAccessDataSource) {
		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setMaxAccessUser(maxAccessUser);
		this.setMaxAccessMenu(maxAccessMenu);
		this.setMaxAccessRole(maxAccessRole);
		this.setMaxAccessModule(maxAccessModule);
		this.setMaxAccessModel(maxAccessModel);
		this.setMaxAccessPage(maxAccessPage);
		this.setMaxAccessProject(maxAccessProject);
		this.setMaxAccessStructure(maxAccessStructure);
		this.setMaxAccessDataSource(maxAccessDataSource);
	}

	/**
	 * @title LicenseApp
	 * @author zhongjyuan
	 * @description License授权应用模型对象
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param maxAccessUser 最大授权用户数
	 * @param maxAccessMenu 最大授权菜单数
	 * @param maxAccessRole 最大授权角色数
	 * @param maxAccessModule 最大授权模块数
	 * @param maxAccessModel 最大授权模型数
	 * @param maxAccessPage 最大授权页面数
	 * @param maxAccessProject 最大授权项目数
	 * @param maxAccessStructure 最大授权架构数
	 * @param maxAccessDataSource 最大授权数据源数
	 * @param maxConcurrencyUser 最大并发用户数
	 * @throws
	 */
	public AppLicenseModel(String appId, String appCode, String appName, Integer maxAccessUser, Integer maxAccessMenu, Integer maxAccessRole, Integer maxAccessModule, Integer maxAccessModel, Integer maxAccessPage, Integer maxAccessProject, Integer maxAccessStructure, Integer maxAccessDataSource, Integer maxConcurrencyUser) {
		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setMaxAccessUser(maxAccessUser);
		this.setMaxAccessMenu(maxAccessMenu);
		this.setMaxAccessRole(maxAccessRole);
		this.setMaxAccessModule(maxAccessModule);
		this.setMaxAccessModel(maxAccessModel);
		this.setMaxAccessPage(maxAccessPage);
		this.setMaxAccessProject(maxAccessProject);
		this.setMaxAccessStructure(maxAccessStructure);
		this.setMaxAccessDataSource(maxAccessDataSource);

		this.setMaxConcurrencyUser(maxConcurrencyUser);
	}

	/**
	 * @title LicenseApp
	 * @author zhongjyuan
	 * @description License授权应用模型对象
	 * @param appId 应用主键
	 * @param appCode 应用编码
	 * @param appName 应用名称
	 * @param maxAccessUser 最大授权用户数
	 * @param maxAccessMenu 最大授权菜单数
	 * @param maxAccessRole 最大授权角色数
	 * @param maxAccessModule 最大授权模块数
	 * @param maxAccessModel 最大授权模型数
	 * @param maxAccessPage 最大授权页面数
	 * @param maxAccessProject 最大授权项目数
	 * @param maxAccessStructure 最大授权架构数
	 * @param maxAccessDataSource 最大授权数据源数
	 * @param maxConcurrencyUser 最大并发用户数
	 * @param remindDate 提醒开始时间
	 * @param expiredDate 失效开始时间
	 * @param shutdownDate 停止开始时间
	 * @throws
	 */
	public AppLicenseModel(String appId, String appCode, String appName, Integer maxAccessUser, Integer maxAccessMenu, Integer maxAccessRole, Integer maxAccessModule, Integer maxAccessModel, Integer maxAccessPage, Integer maxAccessProject, Integer maxAccessStructure, Integer maxAccessDataSource, Integer maxConcurrencyUser, LocalDateTime remindDate, LocalDateTime expiredDate, LocalDateTime shutdownDate) {
		this.setAppId(appId);
		this.setAppCode(appCode);
		this.setAppName(appName);

		this.setMaxAccessUser(maxAccessUser);
		this.setMaxAccessMenu(maxAccessMenu);
		this.setMaxAccessRole(maxAccessRole);
		this.setMaxAccessModule(maxAccessModule);
		this.setMaxAccessModel(maxAccessModel);
		this.setMaxAccessPage(maxAccessPage);
		this.setMaxAccessProject(maxAccessProject);
		this.setMaxAccessStructure(maxAccessStructure);
		this.setMaxAccessDataSource(maxAccessDataSource);

		this.setMaxConcurrencyUser(maxConcurrencyUser);

		this.setRemindDate(remindDate);
		this.setExpiredDate(expiredDate);
		this.setShutdownDate(shutdownDate);
	}

	/**
	 * @title getAppId
	 * @description 获取应用主键
	 * @return 应用主键
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @title setAppId
	 * @description 设置应用主键
	 * @param appId 应用主键
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @title getAppCode
	 * @description 获取应用编码
	 * @return 应用编码
	 */
	public String getAppCode() {
		return appCode;
	}

	/**
	 * @title setAppCode
	 * @description 设置应用编码
	 * @param appCode 应用编码
	 */
	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	/**
	 * @title getAppName
	 * @description 获取应用名称
	 * @return 应用名称
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @title setAppName
	 * @description 设置应用名称
	 * @param appName 应用名称
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * @title getMaxAccessUser
	 * @description 获取最大授权用户数
	 * @return 最大授权用户数
	 */
	public Integer getMaxAccessUser() {
		return maxAccessUser;
	}

	/**
	 * @title setMaxAccessUser
	 * @description 设置最大授权用户数
	 * @param maxAccessUser 最大授权用户数
	 */
	public void setMaxAccessUser(Integer maxAccessUser) {
		this.maxAccessUser = maxAccessUser;
	}

	/**
	 * @title getMaxAccessMenu
	 * @description 获取最大授权菜单数
	 * @return 最大授权菜单数
	 */
	public Integer getMaxAccessMenu() {
		return maxAccessMenu;
	}

	/**
	 * @title setMaxAccessMenu
	 * @description 设置最大授权菜单数
	 * @param maxAccessMenu 最大授权菜单数
	 */
	public void setMaxAccessMenu(Integer maxAccessMenu) {
		this.maxAccessMenu = maxAccessMenu;
	}

	/**
	 * @title getMaxAccessRole
	 * @description 获取最大授权角色数
	 * @return 最大授权角色数
	 */
	public Integer getMaxAccessRole() {
		return maxAccessRole;
	}

	/**
	 * @title setMaxAccessRole
	 * @description 设置最大授权角色数
	 * @param maxAccessRole 最大授权角色数
	 */
	public void setMaxAccessRole(Integer maxAccessRole) {
		this.maxAccessRole = maxAccessRole;
	}

	/**
	 * @title getMaxAccessModule
	 * @description 获取最大授权模块数
	 * @return 最大授权模块数
	 */
	public Integer getMaxAccessModule() {
		return maxAccessModule;
	}

	/**
	 * @title setMaxAccessModule
	 * @description 设置最大授权模块数
	 * @param maxAccessModule 最大授权模块数
	 */
	public void setMaxAccessModule(Integer maxAccessModule) {
		this.maxAccessModule = maxAccessModule;
	}

	/**
	 * @title getMaxAccessModel
	 * @description 获取最大授权模型数
	 * @return 最大授权模型数
	 */
	public Integer getMaxAccessModel() {
		return maxAccessModel;
	}

	/**
	 * @title setMaxAccessModel
	 * @description 设置最大授权模型数
	 * @param maxAccessModel 最大授权模型数
	 */
	public void setMaxAccessModel(Integer maxAccessModel) {
		this.maxAccessModel = maxAccessModel;
	}

	/**
	 * @title getMaxAccessPage
	 * @description 获取最大授权页面数
	 * @return 最大授权页面数
	 */
	public Integer getMaxAccessPage() {
		return maxAccessPage;
	}

	/**
	 * @title setMaxAccessPage
	 * @description 设置最大授权页面数
	 * @param maxAccessPage 最大授权页面数
	 */
	public void setMaxAccessPage(Integer maxAccessPage) {
		this.maxAccessPage = maxAccessPage;
	}

	/**
	 * @title getMaxAccessProject
	 * @description 获取最大授权项目数
	 * @return 最大授权项目数
	 */
	public Integer getMaxAccessProject() {
		return maxAccessProject;
	}

	/**
	 * @title setMaxAccessProject
	 * @description 设置最大授权项目数
	 * @param maxAccessProject 最大授权项目数
	 */
	public void setMaxAccessProject(Integer maxAccessProject) {
		this.maxAccessProject = maxAccessProject;
	}

	/**
	 * @title getMaxAccessStructure
	 * @description 获取最大授权架构数
	 * @return 最大授权架构数
	 */
	public Integer getMaxAccessStructure() {
		return maxAccessStructure;
	}

	/**
	 * @title setMaxAccessStructure
	 * @description 设置最大授权架构数
	 * @param maxAccessStructure 最大授权架构数
	 */
	public void setMaxAccessStructure(Integer maxAccessStructure) {
		this.maxAccessStructure = maxAccessStructure;
	}

	/**
	 * @title getMaxAccessDataSource
	 * @description 获取最大授权数据源数
	 * @return 最大授权数据源数
	 */
	public Integer getMaxAccessDataSource() {
		return maxAccessDataSource;
	}

	/**
	 * @title setMaxAccessDataSource
	 * @description 设置最大授权数据源数
	 * @param maxAccessDataSource 最大授权数据源数
	 */
	public void setMaxAccessDataSource(Integer maxAccessDataSource) {
		this.maxAccessDataSource = maxAccessDataSource;
	}

	/**
	 * @title getMaxConcurrencyUser
	 * @description 获取最大并发用户数
	 * @return 最大并发用户数
	 */
	public Integer getMaxConcurrencyUser() {
		return maxConcurrencyUser;
	}

	/**
	 * @title setMaxConcurrencyUser
	 * @description 设置最大并发用户数
	 * @param maxConcurrencyUser 最大并发用户数
	 */
	public void setMaxConcurrencyUser(Integer maxConcurrencyUser) {
		this.maxConcurrencyUser = maxConcurrencyUser;
	}

	/**
	 * @title getRemindDate
	 * @description 获取提醒开始时间
	 * @return 提醒开始时间
	 */
	public LocalDateTime getRemindDate() {
		return remindDate;
	}

	/**
	 * @title setRemindDate
	 * @description 设置提醒开始时间
	 * @param remindDate 提醒开始时间
	 */
	public void setRemindDate(LocalDateTime remindDate) {
		this.remindDate = remindDate;
	}

	/**
	 * @title getExpiredDate
	 * @description 获取失效开始时间
	 * @return 失效开始时间
	 */
	public LocalDateTime getExpiredDate() {
		return expiredDate;
	}

	/**
	 * @title setExpiredDate
	 * @description 设置失效开始时间
	 * @param expiredDate 失效开始时间
	 */
	public void setExpiredDate(LocalDateTime expiredDate) {
		this.expiredDate = expiredDate;
	}

	/**
	 * @title getShutdownDate
	 * @description 获取停止开始时间
	 * @return 停止开始时间
	 */
	public LocalDateTime getShutdownDate() {
		return shutdownDate;
	}

	/**
	 * @title setShutdownDate
	 * @description 设置停止开始时间
	 * @param shutdownDate 停止开始时间
	 */
	public void setShutdownDate(LocalDateTime shutdownDate) {
		this.shutdownDate = shutdownDate;
	}
}
