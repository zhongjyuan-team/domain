package zhongjyuan.domain;

/**
 * @className: IEnum
 * @description: 枚举对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:55:46
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IEnum extends zhongjyuan {

}
