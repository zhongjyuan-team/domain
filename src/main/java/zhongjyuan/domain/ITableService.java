package zhongjyuan.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import zhongjyuan.domain.query.PageQuery;
import zhongjyuan.domain.query.SortQuery;
import zhongjyuan.domain.response.PageResult;

/**
 * @className: ITableService
 * @description: 定义了对数据库表进行操作的方法。
 * @author: zhongjyuan
 * @date: 2023年11月22日 上午10:02:29
 * @param <E> 表示实体对象类型
 * @param <PV> 表示分页视图模型类型
 * @param <Q> 表示排序查询对象类型
 * @param <PQ> 表示分页查询对象类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface ITableService<E extends ITableEntity, PV extends AbstractModel, Q extends SortQuery, PQ extends PageQuery> {

	/**
	 * 创建一个实体对象并返回创建后的实体对象。
	 * 
	 * @param entity 要创建的实体对象
	 * @return 创建后的实体对象
	 */
	E create(E entity);

	/**
	 * 更新一个实体对象并返回更新后的实体对象。
	 * 
	 * @param entity 要更新的实体对象
	 * @return 更新后的实体对象
	 */
	E update(E entity);

	/**
	 * 根据指定的id删除一个实体对象，并返回删除的实体对象。
	 * 
	 * @param id 要删除的实体对象的id
	 * @return 删除的实体对象
	 */
	E delete(Long id);

	/**
	 * 根据指定的id集合批量删除实体对象，并返回删除的实体对象列表。
	 * 
	 * @param ids 要删除的实体对象的id集合
	 * @return 删除的实体对象列表
	 */
	List<E> delete(Collection<Serializable> ids);

	/**
	 * 根据指定的id查询一个实体对象，并返回查询到的实体对象。
	 * 
	 * @param id 要查询的实体对象的id
	 * @return 查询到的实体对象
	 */
	E select(Long id);

	/**
	 * 根据指定的id集合批量查询实体对象，并返回查询到的实体对象列表。
	 * 
	 * @param ids 要查询的实体对象的id集合
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(Collection<Serializable> ids);

	/**
	 * 根据指定的查询条件查询实体对象列表，并返回查询到的实体对象列表。
	 * 
	 * @param query 查询条件
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(Q query);

	/**
	 * 根据指定的分页查询条件查询实体对象列表，并返回查询到的分页结果。
	 * 
	 * @param query 分页查询条件
	 * @return 查询到的分页结果
	 */
	PageResult<PV> selectPage(PQ query);
}
