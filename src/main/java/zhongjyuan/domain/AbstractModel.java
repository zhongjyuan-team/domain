package zhongjyuan.domain;

/**
 * @className: AbstractModel
 * @description: 抽象模型类，实现了 {@link IModel} 接口。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:58:21
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public abstract class AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

}
