package zhongjyuan.domain;

/**
 * @className: AuthConstant
 * @description: 认证常量对象. 继承{@link Constant}; 实现{@link IConstant}
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午3:07:40
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class AuthConstant extends Constant implements IConstant {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields PARAM_TICKET: 票据参数名[ticket]
	 */
	public static final String PARAM_TICKET = "ticket";

	/**
	 * @fields HEADER_TICKET: 票据Header名[Ticket]
	 */
	public static final String HEADER_TICKET = "Ticket";

	/**
	 * @fields PARAM_AUTHORIZATION: 令牌参数名[access_token]
	 */
	public static final String PARAM_AUTHORIZATION = "access_token";

	/**
	 * @fields HEADER_AUTHORIZATION: 令牌Header名[Authorization]
	 */
	public static final String HEADER_AUTHORIZATION = "Authorization";

	/**
	 * @fields PARAM_PREFIX_L: 参数前缀[current]
	 */
	public static final String PARAM_PREFIX_L = "current";

	/**
	 * @fields PARAM_PREFIX_U_A: 参数前缀[Current]
	 */
	public static final String PARAM_PREFIX_U_A = "Current";

	/**
	 * @fields IDENTITY_PREFIX_L: 身份前缀[user]
	 */
	public static final String IDENTITY_PREFIX_L = "user";

	/**
	 * @fields IDENTITY_PREFIX_U_A: 身份前缀[User]
	 */
	public static final String IDENTITY_PREFIX_U_A = "User";

	/**
	 * @fields IDENTITY_ID: 身份主键[userId]
	 */
	public static final String IDENTITY_ID = IDENTITY_PREFIX_L.concat(ID_U_A);

	/**
	 * @fields IDENTITY_PARAM_ID: 身份主键参数名[currentUserId]
	 */
	public static final String IDENTITY_PARAM_ID = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(ID_U_A);

	/**
	 * @fields IDENTITY_NAME: 身份名称[userName]
	 */
	public static final String IDENTITY_NAME = IDENTITY_PREFIX_L.concat(NAME_U_A);

	/**
	 * @fields IDENTITY_PARAM_NAME: 身份名称参数名[currentUserName]
	 */
	public static final String IDENTITY_PARAM_NAME = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(NAME_U_A);

	/**
	 * @fields IDENTITY_ACCOUNT: 身份账号[userAccount]
	 */
	public static final String IDENTITY_ACCOUNT = IDENTITY_PREFIX_L.concat(ACCOUNT_U_A);

	/**
	 * @fields IDENTITY_PARAM_ACCOUNT: 身份账号参数名[currentUserAccount]
	 */
	public static final String IDENTITY_PARAM_ACCOUNT = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(ACCOUNT_U_A);

	/**
	 * @fields IDENTITY_SEX: 身份性别[userSex]
	 */
	public static final String IDENTITY_SEX = IDENTITY_PREFIX_L.concat(SEX_U_A);

	/**
	 * @fields IDENTITY_PARAM_SEX: 身份性别参数名[currentUserSex]
	 */
	public static final String IDENTITY_PARAM_SEX = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(SEX_U_A);

	/**
	 * @fields IDENTITY_ICON: 身份头像[userIcon]
	 */
	public static final String IDENTITY_ICON = IDENTITY_PREFIX_L.concat(ICON_U_A);

	/**
	 * @fields IDENTITY_PARAM_ICON: 身份头像参数名[currentUserIcon]
	 */
	public static final String IDENTITY_PARAM_ICON = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(ICON_U_A);

	/**
	 * @fields IDENTITY_EMAIL: 身份邮件[userEmail]
	 */
	public static final String IDENTITY_EMAIL = IDENTITY_PREFIX_L.concat(EMAIL_U_A);

	/**
	 * @fields IDENTITY_PARAM_EMAIL: 身份邮件参数名[currentUserEmail]
	 */
	public static final String IDENTITY_PARAM_EMAIL = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(EMAIL_U_A);

	/**
	 * @fields IDENTITY_MOBILE: 身份手机号码[userMobile]
	 */
	public static final String IDENTITY_MOBILE = IDENTITY_PREFIX_L.concat(MOBBILE_U_A);

	/**
	 * @fields IDENTITY_PARAM_MOBILE: 身份手机号码参数名[currentUserMobile]
	 */
	public static final String IDENTITY_PARAM_MOBILE = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(MOBBILE_U_A);

	/**
	 * @fields IDENTITY_TELEPHONE: 身份电话号码[userTelephone]
	 */
	public static final String IDENTITY_TELEPHONE = IDENTITY_PREFIX_L.concat(TELEPHONE_U_A);

	/**
	 * @fields IDENTITY_PARAM_TELEPHONE: 身份电话号码参数名[currentUserTelephone]
	 */
	public static final String IDENTITY_PARAM_TELEPHONE = PARAM_PREFIX_L.concat(IDENTITY_PREFIX_U_A).concat(TELEPHONE_U_A);

	/**
	 * @fields TENANT_ID: 租户主键[tenantId]
	 */
	public static final String TENANT_ID = TENANT_ID_L_A;

	/**
	 * @fields TENANT_PARAM_ID: 租户主键参数名[currentTenantId]
	 */
	public static final String TENANT_PARAM_ID = PARAM_PREFIX_L.concat(TENANT_ID_U_A);

	/**
	 * @fields TENANT_CODE: 租户编码[tenantCode]
	 */
	public static final String TENANT_CODE = TENANT_CODE_L_A;

	/**
	 * @fields TENANT_PARAM_CODE: 租户编码参数名[currentTenantCode]
	 */
	public static final String TENANT_PARAM_CODE = PARAM_PREFIX_L.concat(TENANT_CODE_U_A);

	/**
	 * @fields TENANT_NAME: 租户名称[tenantName]
	 */
	public static final String TENANT_NAME = TENANT_NAME_L_A;

	/**
	 * @fields TENANT_PARAM_NAME: 租户名称参数名[currentTenantName]
	 */
	public static final String TENANT_PARAM_NAME = PARAM_PREFIX_L.concat(TENANT_NAME_U_A);

	/**
	 * @fields SYSTEM_ID: 系统主键[systemId]
	 */
	public static final String SYSTEM_ID = SYSTEM_ID_L_A;

	/**
	 * @fields SYSTEM_PARAM_ID: 系统主键参数名[currentSystemId]
	 */
	public static final String SYSTEM_PARAM_ID = PARAM_PREFIX_L.concat(SYSTEM_ID_U_A);

	/**
	 * @fields SYSTEM_CODE: 系统编码[systemCode]
	 */
	public static final String SYSTEM_CODE = SYSTEM_CODE_L_A;

	/**
	 * @fields SYSTEM_PARAM_CODE: 系统编码参数名[currentSystemCode]
	 */
	public static final String SYSTEM_PARAM_CODE = PARAM_PREFIX_L.concat(SYSTEM_CODE_U_A);

	/**
	 * @fields SYSTEM_NAME: 系统名称[systemName]
	 */
	public static final String SYSTEM_NAME = SYSTEM_NAME_L_A;

	/**
	 * @fields SYSTEM_PARAM_NAME: 系统名称参数名[currentSystemName]
	 */
	public static final String SYSTEM_PARAM_NAME = PARAM_PREFIX_L.concat(SYSTEM_NAME_U_A);

	/**
	 * @fields MODULE_ID: 模块主键[moduleId]
	 */
	public static final String MODULE_ID = MODULE_ID_L_A;

	/**
	 * @fields MODULE_PARAM_ID: 模块主键参数名[currentModuleId]
	 */
	public static final String MODULE_PARAM_ID = PARAM_PREFIX_L.concat(MODULE_ID_U_A);

	/**
	 * @fields MODULE_CODE: 模块编码[moduleCode]
	 */
	public static final String MODULE_CODE = MODULE_CODE_L_A;

	/**
	 * @fields MODULE_PARAM_CODE: 模块编码参数名[currentModuleCode]
	 */
	public static final String MODULE_PARAM_CODE = PARAM_PREFIX_L.concat(MODULE_CODE_U_A);

	/**
	 * @fields MODULE_NAME: 模块名称[moduleName]
	 */
	public static final String MODULE_NAME = MODULE_NAME_L_A;

	/**
	 * @fields MODULE_PARAM_NAME: 模块名称参数名[currentModuleName]
	 */
	public static final String MODULE_PARAM_NAME = PARAM_PREFIX_L.concat(MODULE_NAME_U_A);

	/**
	 * @fields MODEL_ID: 模型主键[modelId]
	 */
	public static final String MODEL_ID = MODEL_ID_L_A;

	/**
	 * @fields MODEL_PARAM_ID: 模块主键参数名[currentModelId]
	 */
	public static final String MODEL_PARAM_ID = PARAM_PREFIX_L.concat(MODEL_ID_U_A);

	/**
	 * @fields MODEL_CODE: 模型编码[modelCode]
	 */
	public static final String MODEL_CODE = MODEL_CODE_L_A;

	/**
	 * @fields MODEL_PARAM_CODE: 模型编码参数名[currentModelCode]
	 */
	public static final String MODEL_PARAM_CODE = PARAM_PREFIX_L.concat(MODEL_CODE_U_A);

	/**
	 * @fields MODEL_NAME: 模型名称[modelName]
	 */
	public static final String MODEL_NAME = MODEL_NAME_L_A;

	/**
	 * @fields MODEL_PARAM_NAME: 模型名称参数名[currentModelName]
	 */
	public static final String MODEL_PARAM_NAME = PARAM_PREFIX_L.concat(MODEL_NAME_U_A);

	/**
	 * @fields page_ID: 应用主键[appId]
	 */
	public static final String APP_ID = APP_ID_L_A;

	/**
	 * @fields APP_PARAM_ID: 应用主键参数名[currentAppId]
	 */
	public static final String APP_PARAM_ID = PARAM_PREFIX_L.concat(APP_ID_U_A);

	/**
	 * @fields APP_CODE: 应用编码[appCode]
	 */
	public static final String APP_CODE = APP_CODE_L_A;

	/**
	 * @fields APP_PARAM_CODE: 应用编码[currentAppCode]
	 */
	public static final String APP_PARAM_CODE = PARAM_PREFIX_L.concat(APP_CODE_U_A);

	/**
	 * @fields APP_NAME: 应用名称[appName]
	 */
	public static final String APP_NAME = APP_NAME_L_A;

	/**
	 * @fields APP_PARAM_NAME: 应用名称参数名[currentAppName]
	 */
	public static final String APP_PARAM_NAME = PARAM_PREFIX_L.concat(APP_NAME_U_A);

	/**
	 * @fields PAGE_ID: 页面主键[pageId]
	 */
	public static final String PAGE_ID = PAGE_ID_L_A;

	/**
	 * @fields PAGE_PARAM_ID: 页面主键参数名[currentPageId]
	 */
	public static final String PAGE_PARAM_ID = PARAM_PREFIX_L.concat(PAGE_ID_U_A);

	/**
	 * @fields PAGE_CODE: 页面编码[pageCode]
	 */
	public static final String PAGE_CODE = PAGE_CODE_L_A;

	/**
	 * @fields PAGE_PARAM_CODE: 页面编码[currentPageCode]
	 */
	public static final String PAGE_PARAM_CODE = PARAM_PREFIX_L.concat(PAGE_CODE_U_A);

	/**
	 * @fields PAGE_NAME: 页面名称[pageName]
	 */
	public static final String PAGE_NAME = PAGE_NAME_L_A;

	/**
	 * @fields PAGE_PARAM_NAME: 页面名称参数名[currentPageName]
	 */
	public static final String PAGE_PARAM_NAME = PARAM_PREFIX_L.concat(PAGE_NAME_U_A);
}
