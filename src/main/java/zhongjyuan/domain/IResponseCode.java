package zhongjyuan.domain;

/**
 * @className: IResponseCode
 * @description: 响应码对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月9日 下午2:46:59
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IResponseCode extends zhongjyuan {

	/**
	 * 获取响应状态码。
	 *
	 * @return 响应状态码
	 */
	Long getCode();

	/**
	 * 获取响应消息。
	 *
	 * @return 响应消息
	 */
	String getMessage();
}
