package zhongjyuan.domain;

/**
 * @className: IConstant
 * @description: 常量对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:55:24
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IConstant extends zhongjyuan {

	public static final String FRAMEWORK_NAME = "zhongjyuan";

	public static final String FRAMEWORK_AUTHOR = "zhongjyuan";

	public static final String FRAMEWORK_VERSION = "V1.0.0";

	public static final String FRAMEWORK_START_TIME = "2022年10月24日";

	public static final String LOGBACK_DEFAULT_XML = "logback-default.xml";

	public static final String LOGBACK_SPRING_XML = "logback-spring.xml";
}
