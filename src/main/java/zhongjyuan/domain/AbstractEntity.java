package zhongjyuan.domain;

/**
 * @className: AbstractEntity
 * @description: 抽象实体类，实现了 {@link IEntity} 接口。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:57:07
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public abstract class AbstractEntity implements IEntity {

	private static final long serialVersionUID = 1L;

}
