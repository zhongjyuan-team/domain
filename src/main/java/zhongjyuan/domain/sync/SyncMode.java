package zhongjyuan.domain.sync;

import zhongjyuan.domain.IEnum;

/**
 * @className: SyncMode
 * @description: 同步模式
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:57:14
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum SyncMode implements IEnum {

	/**
	 * @fields INITIALIZE: 初始化
	 */
	INITIALIZE("INITIALIZE", "初始化"),

	/**
	 * @fields WHOLE: 全量
	 */
	WHOLE("WHOLE", "全量"),

	/**
	 * @fields INCREASE: 增量
	 */
	INCREASE("INCREASE", "增量");

	/**
	 * @fields code: 编码
	 */
	private String code;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @title: SyncMode
	 * @author: zhongjyuan
	 * @description: 同步模式
	 * @param code 编码
	 * @param name 名称
	 * @throws
	 */
	private SyncMode(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}
}
