package zhongjyuan.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import zhongjyuan.domain.query.PageQuery;
import zhongjyuan.domain.query.SearchQuery;
import zhongjyuan.domain.query.SortQuery;

/**
 * @className: ITableDao
 * @description: 定义了对数据库表进行数据访问的方法。
 * @author: zhongjyuan
 * @date: 2023年11月22日 上午10:05:09
 * @param <E> 表示实体对象类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface ITableDao<E extends ITableEntity> {

	/**
	 * 获取表中最大的行索引。
	 * 
	 * @return 最大的行索引
	 */
	int maxRowIndex();

	/**
	 * 获取表的最后更新时间。
	 * 
	 * @return 最后更新时间
	 */
	LocalDateTime lastUpdateTime();

	/**
	 * 计算满足指定条件的实体对象数量。
	 * 
	 * @param entity 实体对象
	 * @return 实体对象数量
	 */
	int count(E entity);

	/**
	 * 根据指定的实体对象和查询条件，计算满足条件的实体对象数量。
	 * 
	 * @param entity 实体对象
	 * @param query 查询条件
	 * @return 实体对象数量
	 */
	int count(E entity, SearchQuery query);

	/**
	 * 判断是否存在满足指定条件的实体对象。
	 * 
	 * @param entity 实体对象
	 * @return 如果存在则返回true，否则返回false
	 */
	boolean exists(E entity);

	/**
	 * 根据指定的实体对象和查询条件，判断是否存在满足条件的实体对象。
	 * 
	 * @param entity 实体对象
	 * @param query 查询条件
	 * @return 如果存在则返回true，否则返回false
	 */
	boolean exists(E entity, SearchQuery query);

	/**
	 * 插入一个实体对象。
	 * 
	 * @param entity 要插入的实体对象
	 * @return 插入成功返回true，否则返回false
	 */
	boolean insert(E entity);

	/**
	 * 批量插入实体对象。
	 * 
	 * @param entities 要插入的实体对象列表
	 * @return 插入成功返回true数组，对应位置的元素为true表示插入成功，否则为false
	 */
	boolean[] insert(List<E> entities);

	/**
	 * 更新一个实体对象。
	 * 
	 * @param entity 要更新的实体对象
	 * @return 更新成功返回true，否则返回false
	 */
	boolean update(E entity);

	/**
	 * 批量更新实体对象。
	 * 
	 * @param entities 要更新的实体对象列表
	 * @return 更新成功返回true数组，对应位置的元素为true表示更新成功，否则为false
	 */
	boolean[] update(List<E> entities);

	/**
	 * 删除一个实体对象。
	 * 
	 * @param entity 要删除的实体对象
	 * @return 删除成功返回true，否则返回false
	 */
	boolean delete(E entity);

	/**
	 * 根据指定的id删除一个实体对象。
	 * 
	 * @param id 要删除的实体对象的id
	 * @return 删除成功返回true，否则返回false
	 */
	boolean delete(Serializable id);

	/**
	 * 根据指定的id集合批量删除实体对象。
	 * 
	 * @param ids 要删除的实体对象的id集合
	 * @return 删除成功返回true数组，对应位置的元素为true表示删除成功，否则为false
	 */
	boolean[] delete(Collection<Serializable> ids);

	/**
	 * 根据指定的id查询一个实体对象。
	 * 
	 * @param id 要查询的实体对象的id
	 * @return 查询到的实体对象
	 */
	E selectOne(Serializable id);

	/**
	 * 根据指定的实体对象查询一个实体对象。
	 * 
	 * @param entity 实体对象
	 * @return 查询到的实体对象
	 */
	E selectOne(E entity);

	/**
	 * 根据指定的实体对象和排序条件查询一个实体对象。
	 * 
	 * @param entity 实体对象
	 * @param query 排序条件
	 * @return 查询到的实体对象
	 */
	E selectOne(E entity, SortQuery query);

	/**
	 * 根据指定的实体对象查询实体对象列表。
	 * 
	 * @param entity 实体对象
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(E entity);

	/**
	 * 根据指定的实体对象和排序条件查询实体对象列表。
	 * 
	 * @param entity 实体对象
	 * @param query 排序条件
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(E entity, SortQuery query);

	/**
	 * 根据指定的id集合查询实体对象列表。
	 * 
	 * @param ids 要查询的实体对象的id集合
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(Collection<Serializable> ids);

	/**
	 * 根据指定的id集合和排序条件查询实体对象列表。
	 * 
	 * @param ids 要查询的实体对象的id集合
	 * @param query 排序条件
	 * @return 查询到的实体对象列表
	 */
	List<E> selectList(Collection<Serializable> ids, SortQuery query);

	/**
	 * 根据指定的实体对象和分页查询条件查询分页结果。
	 * 
	 * @param entity 实体对象
	 * @param query 分页查询条件
	 * @return 查询到的分页结果
	 */
	List<E> selectPage(E entity, PageQuery query);
}
