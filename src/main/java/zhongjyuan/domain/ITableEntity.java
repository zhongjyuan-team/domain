package zhongjyuan.domain;

import java.time.LocalDateTime;

/**
 * @className: ITableEntity
 * @description: 表格实体对象的基础接口，定义了通用的属性和行为。继承{@link IEntity}
 * @author: zhongjyuan
 * @date: 2023年11月9日 下午2:49:53
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface ITableEntity extends IEntity {

	/**
	 * 获取实体对象的唯一标识符
	 *
	 * @return 实体对象的唯一标识符
	 */
	Long getId();

	/**
	 * 设置实体对象的唯一标识符
	 *
	 * @param id 实体对象的唯一标识符
	 */
	void setId(Long id);

	/**
	 * 获取实体对象的行索引
	 *
	 * @return 实体对象的行索引
	 */
	Long getRowIndex();

	/**
	 * 设置实体对象的行索引
	 *
	 * @param rowIndex 实体对象的行索引
	 */
	void setRowIndex(Long rowIndex);

	/**
	 * 获取实体对象的启用状态
	 *
	 * @return 实体对象的启用状态
	 */
	Boolean getIsEnabled();

	/**
	 * 设置实体对象的启用状态
	 *
	 * @param enable 实体对象的启用状态
	 */
	void setIsEnabled(Boolean enable);

	/**
	 * 获取实体对象的删除状态
	 *
	 * @return 实体对象的删除状态
	 */
	Boolean getIsDeleted();

	/**
	 * 设置实体对象的删除状态
	 *
	 * @param delete 实体对象的删除状态
	 */
	void setIsDeleted(Boolean delete);

	/**
	 * 获取实体对象的描述信息
	 *
	 * @return 实体对象的描述信息
	 */
	String getDescription();

	/**
	 * 设置实体对象的描述信息
	 *
	 * @param description 实体对象的描述信息
	 */
	void setDescription(String description);

	/**
	 * 获取创建者的唯一标识符
	 *
	 * @return 创建者的唯一标识符
	 */
	Long getCreatorId();

	/**
	 * 设置创建者的唯一标识符
	 *
	 * @param creatorId 创建者的唯一标识符
	 */
	void setCreatorId(Long creatorId);

	/**
	 * 获取创建者的名称
	 *
	 * @return 创建者的名称
	 */
	String getCreatorName();

	/**
	 * 设置创建者的名称
	 *
	 * @param creatorName 创建者的名称
	 */
	void setCreatorName(String creatorName);

	/**
	 * 获取实体对象的创建时间
	 *
	 * @return 实体对象的创建时间
	 */
	LocalDateTime getCreateTime();

	/**
	 * 设置实体对象的创建时间
	 *
	 * @param createTime 实体对象的创建时间
	 */
	void setCreateTime(LocalDateTime createTime);

	/**
	 * 获取更新者的唯一标识符
	 *
	 * @return 更新者的唯一标识符
	 */
	Long getUpdatorId();

	/**
	 * 设置更新者的唯一标识符
	 *
	 * @param updatorId 更新者的唯一标识符
	 */
	void setUpdatorId(Long updatorId);

	/**
	 * 获取更新者的名称
	 *
	 * @return 更新者的名称
	 */
	String getUpdatorName();

	/**
	 * 设置更新者的名称
	 *
	 * @param updatorName 更新者的名称
	 */
	void setUpdatorName(String updatorName);

	/**
	 * 获取实体对象的更新时间
	 *
	 * @return 实体对象的更新时间
	 */
	LocalDateTime getUpdateTime();

	/**
	 * 设置实体对象的更新时间
	 *
	 * @param updateTime 实体对象的更新时间
	 */
	void setUpdateTime(LocalDateTime updateTime);
}
