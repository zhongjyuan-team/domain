package zhongjyuan.domain.file;

import zhongjyuan.domain.IEnum;

/**
 * @className: FileType
 * @description: 文件类型
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:36:50
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum FileType implements IEnum {

	/**
	 * @fields word: word
	 */
	word,

	/**
	 * @fields cell: cell
	 */
	cell,

	/**
	 * @fields slide: slide
	 */
	slide
}
