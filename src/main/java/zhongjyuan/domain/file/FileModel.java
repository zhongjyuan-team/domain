package zhongjyuan.domain.file;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: FileModel
 * @description: 文件模型对象
 * @author: zhongjyuan
 * @date: 2023年12月1日 上午10:46:40
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields key: 唯一标识
	 */
	private String key;

	/**
	 * @fields size: 大小
	 */
	private Long size;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @fields type: 类型
	 */
	private String type;

	/**
	 * @fields path: 路径
	 */
	private String path;

	/**
	 * @fields relativePath: 相对路径
	 */
	private String relativePath;

	/**
	 * @fields folder: 文件夹
	 */
	private Boolean folder;

	/**
	 * @fields extension: 后缀
	 */
	private String extension;

	/**
	 * @fields parentName: 父级名称
	 */
	private String parentName;

	/**
	 * @fields parentPath: 父级路劲
	 */
	private String parentPath;

	/**
	 * @fields parentSize: 父级大小
	 */
	private Long parentSize;

	/**
	 * @fields lastModified: 最后修改时间
	 */
	private Long lastModified;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public Boolean getFolder() {
		return folder;
	}

	public void setFolder(Boolean folder) {
		this.folder = folder;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentPath() {
		return parentPath;
	}

	public void setParentPath(String parentPath) {
		this.parentPath = parentPath;
	}

	public Long getParentSize() {
		return parentSize;
	}

	public void setParentSize(Long parentSize) {
		this.parentSize = parentSize;
	}

	public Long getLastModified() {
		return lastModified;
	}

	public void setLastModified(Long lastModified) {
		this.lastModified = lastModified;
	}
}
