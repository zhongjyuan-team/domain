package zhongjyuan.domain.file;

import zhongjyuan.domain.IModel;

/**
 * @className: FileContentModel
 * @description: 文件内容模型对象
 * @author: zhongjyuan
 * @date: 2023年12月1日 下午12:18:02
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileContentModel extends FileModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields content: 正文
	 */
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
