package zhongjyuan.domain.file;

import zhongjyuan.domain.IModel;

/**
 * @className: FileShardModel
 * @description: 文件切片模型对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:34:04
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileShardModel extends FileModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	private String id;

	/**
	 * @fields convertId: 转换主键
	 */
	private String convertId;

	/**
	 * @fields shardIndex: 切片索引
	 */
	private Integer shardIndex;

	/**
	 * @fields shardSize: 切片大小
	 */
	private Long shardSize;

	/**
	 * @fields shardTotal: 切片数量
	 */
	private Integer shardTotal;

	/**
	 * @title:  getId
	 * @description: 获取主键
	 * @return: 主键
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title:  setId
	 * @description: 设置主键
	 * @param id 主键
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title:  getConvertId
	 * @description: 获取装换主键
	 * @return: 装换主键
	 */
	public String getConvertId() {
		return convertId;
	}

	/**
	 * @title:  setConvertId
	 * @description: 设置装换主键
	 * @param convertId 装换主键
	 */
	public void setConvertId(String convertId) {
		this.convertId = convertId;
	}

	/**
	 * @title:  getShardIndex
	 * @description: 获取切片索引
	 * @return: 切片索引
	 */
	public Integer getShardIndex() {
		return shardIndex;
	}

	/**
	 * @title:  setShardIndex
	 * @description: 设置切片索引
	 * @param shardIndex 切片索引
	 */
	public void setShardIndex(Integer shardIndex) {
		this.shardIndex = shardIndex;
	}

	/**
	 * @title:  getShardSize
	 * @description: 获取切片大小
	 * @return: 切片大小
	 */
	public Long getShardSize() {
		return shardSize;
	}

	/**
	 * @title:  setShardSize
	 * @description: 设置切片大小
	 * @param shardSize 切片大小
	 */
	public void setShardSize(Long shardSize) {
		this.shardSize = shardSize;
	}

	/**
	 * @title:  getShardTotal
	 * @description: 获取切片数量
	 * @return: 切片数量
	 */
	public Integer getShardTotal() {
		return shardTotal;
	}

	/**
	 * @title:  setShardTotal
	 * @description: 设置切片数量
	 * @param shardTotal 切片数量
	 */
	public void setShardTotal(Integer shardTotal) {
		this.shardTotal = shardTotal;
	}
}
