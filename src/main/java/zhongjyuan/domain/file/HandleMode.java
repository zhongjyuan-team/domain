package zhongjyuan.domain.file;

import zhongjyuan.domain.IEnum;

/**
 * @className: HandleMode
 * @description: 处理模式
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:32:03
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum HandleMode implements IEnum {
	/**
	 * @fields edit: 编辑
	 */
	edit,

	/**
	 * @fields view_sd: 标清预览
	 */
	view_sd,

	/**
	 * @fields view_hd: 高清预览
	 */
	view_hd,
}
