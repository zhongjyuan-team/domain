package zhongjyuan.domain.file;

import java.time.LocalDateTime;
import java.util.List;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: FileUploadModel
 * @description: 文件上传响应对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 下午4:32:38
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileUploadModel<T> extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields uploader: 上传者主键
	 */
	private String uploader;

	/**
	 * @fields uploaderName: 上传者名称
	 */
	private String uploaderName;

	/**
	 * @fields uploadTime: 上传时间
	 */
	private LocalDateTime uploadTime;

	/**
	 * @fields files: 文件集合
	 */
	private List<T> files;

	/**
	 * @title:  getUploader
	 * @description: 获取上传者主键
	 * @return: 上传者主键
	 */
	public String getUploader() {
		return uploader;
	}

	/**
	 * @title:  setUploader
	 * @description: 设置上传者主键
	 * @param uploader 上传者主键
	 */
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	/**
	 * @title:  getUploaderName
	 * @description: 获取上传者名称
	 * @return: 上传者名称
	 */
	public String getUploaderName() {
		return uploaderName;
	}

	/**
	 * @title:  setUploaderName
	 * @description: 设置上传者名称
	 * @param uploaderName 上传者名称
	 */
	public void setUploaderName(String uploaderName) {
		this.uploaderName = uploaderName;
	}

	/**
	 * @title:  getUploadTime
	 * @description: 获取上传时间
	 * @return: 上传时间
	 */
	public LocalDateTime getUploadTime() {
		return uploadTime;
	}

	/**
	 * @title:  setUploadTime
	 * @description: 设置上传时间
	 * @param uploadTime 上传时间
	 */
	public void setUploadTime(LocalDateTime uploadTime) {
		this.uploadTime = uploadTime;
	}

	/**
	 * @title:  getFiles
	 * @description: 获取文件集合
	 * @return: 文件集合
	 */
	public List<T> getFiles() {
		return files;
	}

	/**
	 * @title: getFile
	 * @author: zhongjyuan
	 * @description: 获取文件
	 * @param index 索引
	 * @return 文件对象
	 * @throws
	 */
	public T getFile(int index) {
		return files.get(index);
	}

	/**
	 * @title:  setFiles
	 * @description: 设置文件集合
	 * @param files 文件集合
	 */
	public void setFiles(List<T> files) {
		this.files = files;
	}

	/**
	 * @title: setFile
	 * @author: zhongjyuan
	 * @description: 设置文件
	 * @param file 文件
	 * @throws
	 */
	public void setFile(T file) {
		this.files.add(file);
	}
}
