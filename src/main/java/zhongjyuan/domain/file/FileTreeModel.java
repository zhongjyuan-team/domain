package zhongjyuan.domain.file;

import java.util.ArrayList;
import java.util.List;

import zhongjyuan.domain.IModel;

/**
 * @className: FileTreeModel
 * @description: 文件树模型对象
 * @author: zhongjyuan
 * @date: 2023年12月1日 下午12:17:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileTreeModel extends FileModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields childrens: 子级集合
	 */
	private List<FileTreeModel> childrens;

	public FileTreeModel() {
		this.childrens = new ArrayList<FileTreeModel>();
	}

	public List<FileTreeModel> getChildrens() {
		return childrens;
	}

	public void setChildren(FileTreeModel children) {
		this.childrens.add(children);
	}

	public void setChildrens(List<FileTreeModel> childrens) {
		this.childrens = childrens;
	}
}
