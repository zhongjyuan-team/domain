package zhongjyuan.domain.system;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: App
 * @description: 应用对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 下午5:05:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class App extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields url: 地址
	 */
	protected String url;

	/**
	 * @fields icon: 图标
	 */
	protected String icon;

	/**
	 * @fields device: 设备类型
	 */
	protected Integer device;

	/**
	 * @fields systemId: 系统主键
	 */
	protected String systemId;

	/**
	 * @fields systemCode: 系统编码
	 */
	protected String systemCode;

	/**
	 * @fields systemName: 系统名称
	 */
	protected String systemName;

	/**
	 * @fields privateKey: 私钥
	 */
	protected String privateKey;

	/**
	 * @fields secretKey: 授权码
	 */
	protected String secretKey;

	/**
	 * @fields refresh: 刷新周期
	 */
	protected Integer refresh;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getUrl
	 * @author: zhongjyuan
	 * @description: 获取路径
	 * @return 路径
	 * @throws
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @title: setUrl
	 * @author: zhongjyuan
	 * @description: 设置路径
	 * @param url 路径
	 * @throws
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @title: getIcon
	 * @author: zhongjyuan
	 * @description: 获取图标
	 * @return 图标
	 * @throws
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @title: setIcon
	 * @author: zhongjyuan
	 * @description: 设置图标
	 * @param icon 图标
	 * @throws
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @title: getDevice
	 * @author: zhongjyuan
	 * @description: 获取设备类型
	 * @return 设备类型
	 * @throws
	 */
	public Integer getDevice() {
		return device;
	}

	/**
	 * @title: setDevice
	 * @author: zhongjyuan
	 * @description: 设置设备类型
	 * @param device 设备类型
	 * @throws
	 */
	public void setDevice(Integer device) {
		this.device = device;
	}

	/**
	 * @title: getSystemId
	 * @author: zhongjyuan
	 * @description: 获取系统唯一标识
	 * @return 系统唯一标识
	 * @throws
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @title: setSystemId
	 * @author: zhongjyuan
	 * @description: 设置系统唯一标识 
	 * @param systemId 系统唯一标识
	 * @throws
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @title: getSystemCode
	 * @author: zhongjyuan
	 * @description: 获取系统编码
	 * @return 系统编码
	 * @throws
	 */
	public String getSystemCode() {
		return systemCode;
	}

	/**
	 * @title: setSystemCode
	 * @author: zhongjyuan
	 * @description: 设置系统编码
	 * @param systemCode 系统编码
	 * @throws
	 */
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	/**
	 * @title: getSystemName
	 * @author: zhongjyuan
	 * @description: 获取系统名称
	 * @return 系统名称
	 * @throws
	 */
	public String getSystemName() {
		return systemName;
	}

	/**
	 * @title: setSystemName
	 * @author: zhongjyuan
	 * @description: 设置系统名称
	 * @param systemName 系统名称
	 * @throws
	 */
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	/**
	 * @title: getPrivateKey
	 * @author: zhongjyuan
	 * @description: 获取私钥
	 * @return 私钥
	 * @throws
	 */
	public String getPrivateKey() {
		return privateKey;
	}

	/**
	 * @title: setPrivateKey
	 * @author: zhongjyuan
	 * @description: 设置私钥
	 * @param privateKey 私钥
	 * @throws
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	/**
	 * @title: getSecretKey
	 * @author: zhongjyuan
	 * @description: 获取授权码
	 * @return 授权码
	 * @throws
	 */
	public String getSecretKey() {
		return secretKey;
	}

	/**
	 * @title: setSecretKey
	 * @author: zhongjyuan
	 * @description: 设置授权码
	 * @param secretKey 授权码
	 * @throws
	 */
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	/**
	 * @title: getRefresh
	 * @author: zhongjyuan
	 * @description: 获取刷新周期
	 * @return 刷新周期
	 * @throws
	 */
	public Integer getRefresh() {
		return refresh;
	}

	/**
	 * @title: setRefresh
	 * @author: zhongjyuan
	 * @description: 设置刷新周期
	 * @param refresh 刷新周期
	 * @throws
	 */
	public void setRefresh(Integer refresh) {
		this.refresh = refresh;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
