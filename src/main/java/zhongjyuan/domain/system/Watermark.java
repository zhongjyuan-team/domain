package zhongjyuan.domain.system;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: Watermark
 * @description: 水印对象
 * @author: zhongjyuan
 * @date: 2021年12月1日 上午11:39:28
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Watermark extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields prefix: 前缀
	 */
	protected String prefix;

	/**
	 * @fields content: 正文
	 */
	protected String content;

	/**
	 * @fields xAxis: X轴定位
	 */
	protected String xAxis;

	/**
	 * @fields xSpace: X轴间距
	 */
	protected String xSpace;

	/**
	 * @fields yAxis: Y轴定位
	 */
	protected String yAxis;

	/**
	 * @fields ySpace: Y轴间距
	 */
	protected String ySpace;

	/**
	 * @fields font: 字体
	 */
	protected String font;

	/**
	 * @fields fontSize: 字号
	 */
	protected String fontSize;

	/**
	 * @fields color: 颜色
	 */
	protected String color;

	/**
	 * @fields alpha: 透明度
	 */
	protected String alpha;

	/**
	 * @fields width: 长度
	 */
	protected String width;

	/**
	 * @fields height: 高度
	 */
	protected String height;

	/**
	 * @fields angle: 倾斜度
	 */
	protected String angle;

	/**
	 * @fields rows: 行数
	 */
	protected String rows;

	/**
	 * @fields cols: 列数
	 */
	protected String cols;

	/**
	 * @fields lineHeight: 行高
	 */
	protected String lineHeight;

	/**
	 * @title:  getPrefix
	 * @description: 获取前缀
	 * @return: 前缀
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @title:  setPrefix
	 * @description: 设置前缀
	 * @param prefix 前缀
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @title:  getContent
	 * @description: 获取正文
	 * @return: 正文
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @title:  setContent
	 * @description: 设置正文
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @title:  getxAxis
	 * @description: 获取X轴定位
	 * @return: X轴定位
	 */
	public String getxAxis() {
		return xAxis;
	}

	/**
	 * @title:  setxAxis
	 * @description: 设置X轴定位
	 * @param xAxis X轴定位
	 */
	public void setxAxis(String xAxis) {
		this.xAxis = xAxis;
	}

	/**
	 * @title:  getxSpace
	 * @description: 获取X轴间距
	 * @return: X轴间距
	 */
	public String getxSpace() {
		return xSpace;
	}

	/**
	 * @title:  setxSpace
	 * @description: 设置X轴间距
	 * @param xSpace X轴间距
	 */
	public void setxSpace(String xSpace) {
		this.xSpace = xSpace;
	}

	/**
	 * @title:  getyAxis
	 * @description: 获取Y轴定位
	 * @return: Y轴定位
	 */
	public String getyAxis() {
		return yAxis;
	}

	/**
	 * @title:  setyAxis
	 * @description: 设置Y轴定位
	 * @param yAxis Y轴定位
	 */
	public void setyAxis(String yAxis) {
		this.yAxis = yAxis;
	}

	/**
	 * @title:  getySpace
	 * @description: 获取Y轴间距
	 * @return: Y轴间距
	 */
	public String getySpace() {
		return ySpace;
	}

	/**
	 * @title:  setySpace
	 * @description: 设置Y轴间距
	 * @param ySpace Y轴间距
	 */
	public void setySpace(String ySpace) {
		this.ySpace = ySpace;
	}

	/**
	 * @title:  getFont
	 * @description: 获取字体
	 * @return: 字体
	 */
	public String getFont() {
		return font;
	}

	/**
	 * @title:  setFont
	 * @description: 设置字体
	 * @param font 字体
	 */
	public void setFont(String font) {
		this.font = font;
	}

	/**
	 * @title:  getFontSize
	 * @description: 获取字体大小
	 * @return: 字体大小
	 */
	public String getFontSize() {
		return fontSize;
	}

	/**
	 * @title:  setFontSize
	 * @description: 设置字体大小
	 * @param fontSize 字体大小
	 */
	public void setFontSize(String fontSize) {
		this.fontSize = fontSize;
	}

	/**
	 * @title:  getColor
	 * @description: 获取颜色
	 * @return: 颜色
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @title:  setColor
	 * @description: 设置颜色
	 * @param color 颜色
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @title:  getAlpha
	 * @description: 获取透明度
	 * @return: 透明度
	 */
	public String getAlpha() {
		return alpha;
	}

	/**
	 * @title:  setAlpha
	 * @description: 设置透明度
	 * @param alpha 透明度
	 */
	public void setAlpha(String alpha) {
		this.alpha = alpha;
	}

	/**
	 * @title:  getWidth
	 * @description: 获取宽度
	 * @return: 宽度
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @title:  setWidth
	 * @description: 设置宽度
	 * @param width 宽度
	 */
	public void setWidth(String width) {
		this.width = width;
	}

	/**
	 * @title:  getHeight
	 * @description: 获取高度
	 * @return: 高度
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @title:  setHeight
	 * @description: 设置高度
	 * @param height 高度
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @title:  getAngle
	 * @description: 获取倾斜度
	 * @return: 倾斜度
	 */
	public String getAngle() {
		return angle;
	}

	/**
	 * @title:  setAngle
	 * @description: 设置倾斜度
	 * @param angle 倾斜度
	 */
	public void setAngle(String angle) {
		this.angle = angle;
	}

	/**
	 * @title:  getRows
	 * @description: 获取行数
	 * @return: 行数
	 */
	public String getRows() {
		return rows;
	}

	/**
	 * @title:  setRows
	 * @description: 设置行数
	 * @param rows 行数
	 */
	public void setRows(String rows) {
		this.rows = rows;
	}

	/**
	 * @title:  getCols
	 * @description: 获取列数
	 * @return: 列数
	 */
	public String getCols() {
		return cols;
	}

	/**
	 * @title:  setCols
	 * @description: 设置列数
	 * @param cols 列数
	 */
	public void setCols(String cols) {
		this.cols = cols;
	}

	/**
	 * @title:  getLineHeight
	 * @description: 获取行高
	 * @return: 行高
	 */
	public String getLineHeight() {
		return lineHeight;
	}

	/**
	 * @title:  setLineHeight
	 * @description: 设置行高
	 * @param lineHeight 行高
	 */
	public void setLineHeight(String lineHeight) {
		this.lineHeight = lineHeight;
	}
}
