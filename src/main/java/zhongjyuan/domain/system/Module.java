package zhongjyuan.domain.system;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: Module
 * @description: 模块对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 下午5:06:18
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class Module extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	protected String id;

	/**
	 * @fields code: 编码
	 */
	protected String code;

	/**
	 * @fields name: 名称
	 */
	protected String name;

	/**
	 * @fields icon: 图标
	 */
	protected String icon;

	/**
	 * @fields rowIndex: 行号
	 */
	protected Integer rowIndex;

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getIcon
	 * @author: zhongjyuan
	 * @description: 获取图标
	 * @return 图标
	 * @throws
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @title: setIcon
	 * @author: zhongjyuan
	 * @description: 设置图标
	 * @param icon 图标
	 * @throws
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
