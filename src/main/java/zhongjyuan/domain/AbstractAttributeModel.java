package zhongjyuan.domain;

import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

/**
 * @className: AbstractAttributeModel
 * @description: 抽象属性模型对象.继承{@link AbstractModel}; 实现{@link IAttributeModel}
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午12:28:16
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class AbstractAttributeModel extends AbstractModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 属性集合
	 */
	protected Properties properties = new Properties();

	@Override
	public void clear() {
		properties.clear();
	}

	@Override
	public Object get(Object key) {
		return properties.get(key);
	}

	@Override
	public Object remove(Object key) {
		Object value = properties.get(key);
		properties.remove(key);
		return value;
	}

	@Override
	public Object put(Object key, Object value) {
		properties.put(key, value);
		return value;
	}

	@Override
	public void putAll(Map<Object, Object> values) {
		for (Entry<? extends Object, ? extends Object> value : values.entrySet()) {
			properties.put(value.getKey(), value.getValue());
		}
	}

}
