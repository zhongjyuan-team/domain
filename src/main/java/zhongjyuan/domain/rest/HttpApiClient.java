package zhongjyuan.domain.rest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import zhongjyuan.domain.utils.format.JsonJackUtil;

/**
 * @className: HttpApiClient
 * @description: HTTP API 客户端
 * @author: zhongjyuan
 * @date: 2023年12月4日 下午5:34:02
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class HttpApiClient {

	static Logger LOGGER = LoggerFactory.getLogger(HttpApiClient.class);

	static final String GET = "GET";

	static final String PUT = "PUT";

	static final String POST = "POST";

	static final String DELETE = "DELETE";

	static int CONNECT_TIMEOUT = 5000; // 连接超时时间

	static int RESPONSE_TIMEOUT = 10000; // 响应超时时间

	static HttpClient httpClient = new HttpClient();

	static HttpMethod httpMethod = null;

	public static HttpClient getHttpClient() {
		return httpClient;
	}

	public static HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public static void setConnectTimeout(int connectTimeout) {
		CONNECT_TIMEOUT = connectTimeout;
	}

	public static void setResponseTimeout(int ResponseTimeout) {
		RESPONSE_TIMEOUT = ResponseTimeout;
	}

	// region GET

	/**
	 * @title: get
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:18:37
	 * @description: 获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应反向对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T get(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {
		return baseExchange(path, GET, requestBody, headers, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:30:43
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, Map<String, Object> requestParams, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, null, null, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:06:32
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, null, null, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:27:07
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, Map<String, Object> requestParams, K requestBody, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, requestBody, null, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:05:18
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, K requestBody, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, requestBody, null, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:46:42
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, Map<String, Object> requestParams, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, requestBody, headers, responseType);
	}

	/**
	 * @title: getByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:02:16
	 * @description: 通过路由参数Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapParams(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, GET, requestBody, headers, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:49:33
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, null, null, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:00:29
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, null, null, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:52:15
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, K requestBody, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, requestBody, null, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:59:07
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, K requestBody, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, requestBody, null, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:55:21
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, requestBody, headers, responseType);
	}

	/**
	 * @title: getByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:57:38
	 * @description: 通过路由占位符Map获取
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T getByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, GET, requestBody, headers, responseType);
	}

	// endregion

	// region POST

	/**
	 * @title: post
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:18:37
	 * @description: 提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应反向对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T post(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {
		return baseExchange(path, POST, requestBody, headers, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:30:43
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, Map<String, Object> requestParams, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, null, null, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:06:32
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, null, null, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:27:07
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, Map<String, Object> requestParams, K requestBody, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, requestBody, null, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:05:18
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, K requestBody, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, requestBody, null, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:46:42
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, Map<String, Object> requestParams, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, requestBody, headers, responseType);
	}

	/**
	 * @title: postByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:02:16
	 * @description: 通过路由参数Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapParams(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, POST, requestBody, headers, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:49:33
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, null, null, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:00:29
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, null, null, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:52:15
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, K requestBody, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, requestBody, null, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:59:07
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, K requestBody, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, requestBody, null, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:55:21
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, requestBody, headers, responseType);
	}

	/**
	 * @title: postByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:57:38
	 * @description: 通过路由占位符Map提交
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T postByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, POST, requestBody, headers, responseType);
	}

	// endregion

	// region PUT

	/**
	 * @title: put
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:18:37
	 * @description: 修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应反向对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T put(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {
		return baseExchange(path, PUT, requestBody, headers, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:30:43
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, Map<String, Object> requestParams, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, null, null, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:06:32
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, null, null, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:27:07
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, Map<String, Object> requestParams, K requestBody, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, requestBody, null, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:05:18
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, K requestBody, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, requestBody, null, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:46:42
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, Map<String, Object> requestParams, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, requestBody, headers, responseType);
	}

	/**
	 * @title: putByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:02:16
	 * @description: 通过路由参数Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapParams(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, PUT, requestBody, headers, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:49:33
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, null, null, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:00:29
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, null, null, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:52:15
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, K requestBody, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, requestBody, null, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:59:07
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, K requestBody, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, requestBody, null, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:55:21
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, requestBody, headers, responseType);
	}

	/**
	 * @title: putByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:57:38
	 * @description: 通过路由占位符Map修改
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T putByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, PUT, requestBody, headers, responseType);
	}

	// endregion

	// region DELETE

	/**
	 * @title: delete
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:18:37
	 * @description: 删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应反向对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T delete(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {
		return baseExchange(path, DELETE, requestBody, headers, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:30:43
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, Map<String, Object> requestParams, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, null, null, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:06:32
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, null, null, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:27:07
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, Map<String, Object> requestParams, K requestBody, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, requestBody, null, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:05:18
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, K requestBody, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, requestBody, null, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:46:42
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, Map<String, Object> requestParams, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, requestBody, headers, responseType);
	}

	/**
	 * @title: deleteByMapParams
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:02:16
	 * @description: 通过路由参数Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestParams 请求路由参数Map
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应结果
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapParams(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... requestParams) {

		path = appendParams(path, requestParams);

		return baseExchange(path, DELETE, requestBody, headers, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:49:33
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, null, null, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午5:00:29
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, null, null, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:52:15
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, K requestBody, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, requestBody, null, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:59:07
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, K requestBody, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, requestBody, null, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:55:21
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param pathVariables 请求路径占位符
	 * @param responseType 响应类型
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Map<String, Object> pathVariables, Class<T> responseType) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, requestBody, headers, responseType);
	}

	/**
	 * @title: deleteByMapVariables
	 * @author zhongjyuan
	 * @date 2020年12月14日下午4:57:38
	 * @description: 通过路由占位符Map删除
	 * @param <K> 请求泛型对象
	 * @param <T> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对路径
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @param pathVariables 请求路径占位符
	 * @return T 响应结果 适用于返回自定义响应对象
	 * @throws
	 */
	public static <K, T> T deleteByMapVariables(String path, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType, Object... pathVariables) {

		path = replaceUrlVariables(path, pathVariables);

		return baseExchange(path, DELETE, requestBody, headers, responseType);
	}

	// endregion

	/**
	 * @title: baseExchange
	 * @author zhongjyuan
	 * @date 2020年12月14日下午3:41:10
	 * @description: 基础数据交换
	 * @param <T> 请求Body泛型对象
	 * @param <K> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对地址
	 * @param method 请求方式
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @return T 返回类型
	 * @throws
	 */
	@SuppressWarnings("unchecked")
	public static <T, K> T baseExchange(String url, String method, K requestBody, MultiValuedMap<String, String> headers, Class<T> responseType) {
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(CONNECT_TIMEOUT);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(RESPONSE_TIMEOUT);

		httpMethod = null;
		try {
			switch (method) {
				case GET:
					httpMethod = createGetMethod(url, requestBody, headers);
					break;
				case PUT:
					httpMethod = createPutMethod(url, requestBody, headers);
					break;
				case POST:
					httpMethod = createPostMethod(url, requestBody, headers);
					break;
				case DELETE:
					httpMethod = createDeleteMethod(url, requestBody, headers);
					break;
				default:
					break;
			}

			if (httpMethod == null) {
				LOGGER.error("Request failed to method empty.");
				return null;
			}

			// 执行请求
			LOGGER.info("HttpApiClient=>url:{};method:{}", url.toString(), method);
			int statusCode = httpClient.executeMethod(httpMethod);
			if (statusCode == HttpStatus.SC_OK) {
				String response = httpMethod.getResponseBodyAsString();
				LOGGER.debug("HttpApiClient=>url:{};method:{};response:{}", url.toString(), method, response);
				if (responseType == String.class) {
					return (T) response;
				} else {
					return JsonJackUtil.objectMapper().readValue(response, responseType);
				}
			} else {
				LOGGER.error("Failed to make POST request: {}", httpMethod.getStatusLine());
			}
		} catch (Exception e) {
			LOGGER.error("An error occurred while making the POST request", e);
		} finally {
			if (httpMethod != null) {
				httpMethod.releaseConnection();
			}
		}

		return null;
	}

	/**
	 * @title: baseExchange
	 * @author zhongjyuan
	 * @date 2020年12月14日下午3:39:22
	 * @description: 基础数据交换 @param <T> 请求Body泛型对象
	 * @param <K> 响应泛型对象
	 * @param serverName 服务名称
	 * @param path 请求相对地址
	 * @param method 请求方式
	 * @param requestBody 请求Body
	 * @param headers 请求Header
	 * @param responseType 响应类型
	 * @return T 返回类型
	 * @throws
	 */
	public static <T, K> T baseExchange(String url, String method, K requestBody, MultiValuedMap<String, String> headers, TypeReference<T> responseType) {
		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(CONNECT_TIMEOUT);
		httpClient.getHttpConnectionManager().getParams().setSoTimeout(RESPONSE_TIMEOUT);

		httpMethod = null;
		try {
			switch (method) {
				case GET:
					httpMethod = createGetMethod(url, requestBody, headers);
					break;
				case PUT:
					httpMethod = createPutMethod(url, requestBody, headers);
					break;
				case POST:
					httpMethod = createPostMethod(url, requestBody, headers);
					break;
				case DELETE:
					httpMethod = createDeleteMethod(url, requestBody, headers);
					break;
				default:
					break;
			}

			if (httpMethod == null) {
				LOGGER.error("Request failed to method empty.");
				return null;
			}

			// 执行请求
			LOGGER.info("HttpApiClient=>url:{};method:{}", url.toString(), method);
			int statusCode = httpClient.executeMethod(httpMethod);
			if (statusCode == HttpStatus.SC_OK) {
				String response = httpMethod.getResponseBodyAsString();
				LOGGER.debug("HttpApiClient=>url:{};method:{};response:{}", url.toString(), method, response);
				return JsonJackUtil.objectMapper().readValue(response, responseType);
			} else {
				LOGGER.error("Failed to make POST request: {}", httpMethod.getStatusLine());
			}
		} catch (Exception e) {
			LOGGER.error("An error occurred while making the POST request", e);
		} finally {
			if (httpMethod != null) {
				httpMethod.releaseConnection();
			}
		}

		return null;
	}

	/**
	 * @title: createGetMethod
	 * @author: zhongjyuan
	 * @description: 创建 GET 请求方法
	 * @param <K> 请求体的类型（在 GET 请求中没有请求体，该参数可忽略）
	 * @param url 请求的URL
	 * @param requestBody 请求体
	 * @param headers 请求头
	 * @return GET 请求方法
	 * @throws IOException 如果创建请求方法时出现错误，抛出 IOException 异常
	 */
	private static <K> GetMethod createGetMethod(String url, K requestBody, MultiValuedMap<String, String> headers) throws IOException {
		GetMethod method = new GetMethod(url);

		if (headers != null) {
			for (String key : headers.keySet()) {
				for (String value : headers.get(key)) {
					method.addRequestHeader(key, value);
				}
			}
		}

		return method;
	}

	/**
	 * @title: createPutMethod
	 * @author: zhongjyuan
	 * @description: 创建 PUT 请求方法
	 * @param <K> 请求体的类型
	 * @param url 请求的URL
	 * @param requestBody 请求体
	 * @param headers 请求头
	 * @return PUT 请求方法
	 * @throws IOException 如果创建请求方法时出现错误，抛出 IOException 异常
	 */
	private static <K> PutMethod createPutMethod(String url, K requestBody, MultiValuedMap<String, String> headers) throws IOException {
		PutMethod method = new PutMethod(url);

		if (headers != null) {
			for (String key : headers.keySet()) {
				for (String value : headers.get(key)) {
					method.addRequestHeader(key, value);
				}
			}
		}

		if (requestBody != null) {
			String jsonBody = JsonJackUtil.objectMapper().writeValueAsString(requestBody);
			RequestEntity requestEntity = new StringRequestEntity(jsonBody, "application/json", "UTF-8");
			method.setRequestEntity(requestEntity);
		}

		return method;
	}

	/**
	 * @title: createPostMethod
	 * @author: zhongjyuan
	 * @description: 创建 POST 请求方法
	 * @param <K> 请求体的类型
	 * @param url 请求的URL
	 * @param requestBody 请求体
	 * @param headers 请求头
	 * @return POST 请求方法
	 * @throws IOException 如果创建请求方法时出现错误，抛出 IOException 异常
	 */
	private static <K> PostMethod createPostMethod(String url, K requestBody, MultiValuedMap<String, String> headers) throws IOException {
		PostMethod method = new PostMethod(url);

		if (headers != null) {
			for (String key : headers.keySet()) {
				for (String value : headers.get(key)) {
					method.addRequestHeader(key, value);
				}
			}
		}

		if (requestBody != null) {
			String jsonBody = JsonJackUtil.objectMapper().writeValueAsString(requestBody);
			RequestEntity requestEntity = new StringRequestEntity(jsonBody, "application/json", "UTF-8");
			method.setRequestEntity(requestEntity);
		}

		return method;
	}

	/**
	 * @title: createDeleteMethod
	 * @author: zhongjyuan
	 * @description: 创建 DELETE 请求方法
	 * @param <K> 请求体的类型
	 * @param url 请求的URL
	 * @param requestBody 请求体
	 * @param headers 请求头
	 * @return DELETE 请求方法
	 * @throws IOException 如果创建请求方法时出现错误，抛出 IOException 异常
	 */
	private static <K> DeleteMethod createDeleteMethod(String url, K requestBody, MultiValuedMap<String, String> headers) throws IOException {
		DeleteMethod method = new DeleteMethod(url);

		if (headers != null) {
			for (String key : headers.keySet()) {
				for (String value : headers.get(key)) {
					method.addRequestHeader(key, value);
				}
			}
		}

		return method;
	}

	/**
	 * @title: replaceUrlVariables
	 * @author: zhongjyuan
	 * @description: 替换URL中的变量
	 * @param url 原始URL
	 * @param variables 变量数组，按照 key-value 键值对的形式传入
	 * @return 替换后的URL
	 */
	public static String replaceUrlVariables(String url, Object... variables) {
		try {
			if (variables != null && variables.length > 0) {
				for (int i = 0; i < variables.length; i += 2) {
					if (variables[i] != null && variables[i + 1] != null) {
						String encodedKey = URLEncoder.encode(variables[i].toString(), StandardCharsets.UTF_8.toString());
						String encodedValue = URLEncoder.encode(variables[i + 1].toString(), StandardCharsets.UTF_8.toString());
						url = url.replace("{" + encodedKey + "}", encodedValue);
					}
				}
			}

			return url;
		} catch (UnsupportedEncodingException e) {
			return url;
		}
	}

	/**
	 * @title: replaceUrlVariables
	 * @author: zhongjyuan
	 * @description: 替换URL中的变量
	 * @param url 原始URL
	 * @param variables 变量数组，按照 key-value 键值对的形式传入
	 * @return 替换后的URL
	 */
	public static String replaceUrlVariables(String url, Map<String, Object> variables) {
		try {
			for (Map.Entry<String, Object> entry : variables.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();

				if (key != null && value != null) {
					String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());
					String encodedValue = URLEncoder.encode(value.toString(), StandardCharsets.UTF_8.toString());
					url = url.replace("{" + encodedKey + "}", encodedValue);
				}
			}

			return url;
		} catch (UnsupportedEncodingException e) {
			return url;
		}
	}

	/**
	 * @title: appendParams
	 * @author: zhongjyuan
	 * @description: 将参数拼接到URL中
	 * @param url 原始URL
	 * @param params 参数数组，按照 key-value 键值对的形式传入
	 * @return 拼接后的URL
	 */
	public static String appendParams(String url, Object... params) {
		try {
			URI uri = new URI(url);
			StringBuilder sb = new StringBuilder(uri.toString());

			if (params != null && params.length > 0) {
				boolean hasQuery = uri.getQuery() != null;
				sb.append(hasQuery ? "&" : "?");

				for (int i = 0; i < params.length; i += 2) {
					if (params[i] != null && params[i + 1] != null) {
						String key = URLEncoder.encode(params[i].toString(), StandardCharsets.UTF_8.toString());
						String value = URLEncoder.encode(params[i + 1].toString(), StandardCharsets.UTF_8.toString());
						sb.append(key).append("=").append(value).append("&");
					}
				}

				sb.deleteCharAt(sb.length() - 1);
			}

			return sb.toString();
		} catch (URISyntaxException e) {
			return url;
		} catch (UnsupportedEncodingException e) {
			return url;
		}
	}

	/**
	 * @title: appendParams
	 * @author: zhongjyuan
	 * @description: 将参数拼接到URL中
	 * @param url 原始URL
	 * @param params 参数集合
	 * @return 拼接后的URL
	 */
	public static String appendParams(String url, Map<String, Object> params) {
		try {
			URI uri = new URI(url);
			String queryString = uri.getQuery();

			if (params != null) {
				StringBuilder sb = new StringBuilder();

				for (Map.Entry<String, Object> entry : params.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();

					if (key != null && value != null) {
						String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());
						String encodedValue = URLEncoder.encode(value.toString(), StandardCharsets.UTF_8.toString());
						sb.append("&").append(encodedKey).append("=").append(encodedValue);
					}
				}

				if (sb.length() > 0) {
					queryString = (queryString == null || queryString.isEmpty()) ? "" : queryString + "";
					queryString += sb.toString();
				}
			}

			URI updatedUri = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), queryString, uri.getFragment());
			return updatedUri.toString();
		} catch (URISyntaxException e) {
			return url;
		} catch (UnsupportedEncodingException e) {
			return url;
		}
	}
}
