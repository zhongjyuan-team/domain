package zhongjyuan.domain.utils;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ITool;
import zhongjyuan.domain.exception.AssertException;

/**
 * @className: AssertUtil
 * @description: 断言工具
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午11:36:11
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class AssertUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @title isTrue
	 * @author zhongjyuan
	 * @description isTrue
	 * @param expression 条件[False时异常]
	 * @param message 异常信息
	 * @return void
	 * @throws AssertException
	 */
	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isTrue
	 * @author zhongjyuan
	 * @description isTrue
	 * @param expression 条件[False时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static void isTrue(boolean expression, IResponseCode code) {
		if (!expression) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isFalse
	 * @author zhongjyuan
	 * @description isFalse
	 * @param expression 条件[True时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static void isFalse(boolean expression, String message) {
		if (expression) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isFalse
	 * @author zhongjyuan
	 * @description isFalse
	 * @param expression 条件[True时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static void isFalse(boolean expression, IResponseCode code) {
		if (expression) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isNull
	 * @author zhongjyuan
	 * @description isNull
	 * @param object 条件[NotNull时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static void isNull(Object object, String message) {
		if (ObjectUtils.isNotEmpty(object)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isNull
	 * @author zhongjyuan
	 * @description isNull
	 * @param object 条件[NotNull时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static void isNull(Object object, IResponseCode code) {
		if (ObjectUtils.isNotEmpty(object)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title notNull
	 * @author zhongjyuan
	 * @description notNull
	 * @param object 条件[Null时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static void notNull(Object object, String message) {
		if (ObjectUtils.isEmpty(object)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title notNull
	 * @author zhongjyuan
	 * @description notNull
	 * @param object 条件[Null时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static void notNull(Object object, IResponseCode code) {
		if (ObjectUtils.isEmpty(object)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param text 条件[NotEmpty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void isEmpty(T text, String message) {
		if (StringUtils.isNotEmpty(text)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param text 条件[NotEmpty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void isEmpty(T text, IResponseCode code) {
		if (StringUtils.isNotEmpty(text)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param text 条件[Empty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void notEmpty(T text, String message) {
		if (StringUtils.isEmpty(text)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param text 条件[Empty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void notEmpty(T text, IResponseCode code) {
		if (StringUtils.isEmpty(text)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isBlank
	 * @author zhongjyuan
	 * @description isBlank
	 * @param <T> 泛型对象
	 * @param text 条件[NotBlank时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void isBlank(T text, String message) {
		if (StringUtils.isNotBlank(text)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isBlank
	 * @author zhongjyuan
	 * @description isBlank
	 * @param <T> 泛型对象
	 * @param text 条件[NotBlank时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void isBlank(T text, IResponseCode code) {
		if (StringUtils.isNotBlank(text)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title notBlank
	 * @author zhongjyuan
	 * @description notBlank
	 * @param <T> 泛型对象
	 * @param text 条件[Blank时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void notBlank(T text, String message) {
		if (StringUtils.isBlank(text)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title notBlank
	 * @author zhongjyuan
	 * @description notBlank
	 * @param <T> 泛型对象
	 * @param text 条件[Blank时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T extends CharSequence> void notBlank(T text, IResponseCode code) {
		if (StringUtils.isBlank(text)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param array 条件[NotEmpty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T> void isEmpty(T[] array, String message) {
		if (ArrayUtils.isNotEmpty(array)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param array 条件[NotEmpty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T> void isEmpty(T[] array, IResponseCode code) {
		if (ArrayUtils.isNotEmpty(array)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param array 条件[Empty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T> void notEmpty(T[] array, String message) {
		if (ArrayUtils.isEmpty(array)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param array 条件[Empty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T> void notEmpty(T[] array, IResponseCode code) {
		if (ArrayUtils.isEmpty(array)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param collection 条件[NotEmpty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T> void isEmpty(Collection<T> collection, String message) {
		if (CollectionUtils.isNotEmpty(collection)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title isEmpty
	 * @author zhongjyuan
	 * @description isEmpty
	 * @param <T> 泛型对象
	 * @param collection 条件[NotEmpty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T> void isEmpty(Collection<T> collection, IResponseCode code) {
		if (CollectionUtils.isNotEmpty(collection)) {
			throw new AssertException(code);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param collection 条件[Empty时异常]
	 * @param message 异常信息
	 * @throws AssertException
	 */
	public static <T> void notEmpty(Collection<T> collection, String message) {
		if (CollectionUtils.isEmpty(collection)) {
			throw new AssertException(message);
		}
	}

	/**
	 * @title notEmpty
	 * @author zhongjyuan
	 * @description notEmpty
	 * @param <T> 泛型对象
	 * @param collection 条件[Empty时异常]
	 * @param code 异常消息
	 * @throws AssertException
	 */
	public static <T> void notEmpty(Collection<T> collection, IResponseCode code) {
		if (CollectionUtils.isEmpty(collection)) {
			throw new AssertException(code);
		}
	}
}
