package zhongjyuan.domain.utils.security;

import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import zhongjyuan.domain.ITool;

/**
 * @Project: eap-security
 * @Package: com.idea.eap.security.hmac
 * @ClassName: HMACCrypto
 * @Description: HMAC工具类
 * @author zhongjyuan
 * @date 2021年3月24日 下午4:17:48
 *
 */
public class HMACCrypto implements ITool {

	private static final long serialVersionUID = 1L;

	public static final String KEY_MAC = "HmacMD5";

	/**
	 * @Title generateKey
	 * @Author zhongjyuan
	 * @Description 生成Key
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String generateKey() {

		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_MAC);
			SecretKey secreKey = keyGenerator.generateKey();
			return Base64.getEncoder().encodeToString(secreKey.getEncoded());
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param bytes
	 * @Param @param secretKeyt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] encrypt(byte[] bytes, String secretKeyt) {
		try {
			SecretKey secreKey = new SecretKeySpec(Base64.getDecoder().decode(secretKeyt), KEY_MAC);
			Mac mac = Mac.getInstance(secreKey.getAlgorithm());
			mac.init(secreKey);
			return mac.doFinal(bytes);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext
	 * @Param @param secretKeyt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] encrypt(String plaintext, String secretKeyt) {
		return encrypt(plaintext.getBytes(), secretKeyt);
	}
}
