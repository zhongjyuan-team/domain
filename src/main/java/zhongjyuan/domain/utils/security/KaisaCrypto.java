package zhongjyuan.domain.utils.security;

import zhongjyuan.domain.ITool;

/**
 * @Project: eap-security
 * @Package: com.idea.eap.security.kaisa
 * @ClassName: KaisaCrypto
 * @Description: Kaisa工具类
 * @author zhongjyuan
 * @date 2021年3月24日 下午4:32:51
 *
 */
public class KaisaCrypto implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密 
	 * @Param @param plaintext 明文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encrypt(String plaintext, int secretKeyt) {

		char[] chars = plaintext.toCharArray();
		StringBuffer stringBuffer = new StringBuffer();

		for (char aChar : chars) {
			int asciiCode = aChar;
			asciiCode += secretKeyt;
			char result = (char) asciiCode;
			stringBuffer.append(result);
		}
		return stringBuffer.toString();
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密 
	 * @Param @param ciphertext 密文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decrypt(String ciphertext, int secretKeyt) {

		char[] chars = ciphertext.toCharArray();
		StringBuilder stringBuilder = new StringBuilder();

		for (char aChar : chars) {
			int asciiCode = aChar;
			asciiCode -= secretKeyt;
			char result = (char) asciiCode;
			stringBuilder.append(result);
		}

		return stringBuilder.toString();
	}
}
