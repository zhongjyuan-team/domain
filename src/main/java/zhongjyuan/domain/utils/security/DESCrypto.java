package zhongjyuan.domain.utils.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import zhongjyuan.domain.ITool;

/**
 * @Project: eap-security
 * @Package: com.idea.eap.security.des
 * @ClassName: DESCrypto
 * @Description: DES工具类
 * @author zhongjyuan
 * @date 2021年3月24日 下午3:51:11
 *
 */
public class DESCrypto implements ITool {

	private static final long serialVersionUID = 1L;
	/**
	 * @FieldType String
	 * @Fields CHARSET:(默认编码)
	 */
	private static final String CHARSET = "utf-8";
	/**
	 * @FieldType String
	 * @Fields IV_PARAMETER:(偏移变量，固定占8位字节)
	 */
	private final static String IV_PARAMETER = "12345678";
	/**
	 * @FieldType String
	 * @Fields ALGORITHM:(密钥算法)
	 */
	private static final String ALGORITHM = "DES";
	/**
	 * @FieldType String
	 * @Fields CIPHER_ALGORITHM:(加密/解密算法-工作模式-填充模式)
	 */
	private static final String CIPHER_ALGORITHM = "DES/CBC/PKCS5Padding";

	/**
	 * @Title generateKey
	 * @Author zhongjyuan
	 * @Description 生成key
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Param @throws Exception
	 * @Return Key
	 * @Throws
	 */
	private static Key generateKey(String secretKeyt) throws Exception {
		DESKeySpec desKeySpec = new DESKeySpec(secretKeyt.getBytes(CHARSET));
		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM);
		return secretKeyFactory.generateSecret(desKeySpec);
	}

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext 明文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encrypt(String plaintext, String secretKeyt) {

		if (secretKeyt == null || secretKeyt.length() < 8)
			throw new RuntimeException("加密失败，key不能小于8位");
		if (plaintext == null)
			return null;

		try {
			Key secretKey = generateKey(secretKeyt);
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
			byte[] bytes = cipher.doFinal(plaintext.getBytes(CHARSET));

			return new String(Base64.getEncoder().encode(bytes));
		} catch (Exception e) {
			e.printStackTrace();
			return plaintext;
		}
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext 密文
	 * @Param @param secretKeyt 密钥
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decrypt(String ciphertext, String secretKeyt) {

		if (secretKeyt == null || secretKeyt.length() < 8)
			throw new RuntimeException("加密失败，key不能小于8位");
		if (ciphertext == null)
			return null;

		try {
			Key secretKey = generateKey(secretKeyt);
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
			cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);

			return new String(cipher.doFinal(Base64.getDecoder().decode(ciphertext.getBytes(CHARSET))), CHARSET);
		} catch (Exception e) {
			e.printStackTrace();
			return ciphertext;
		}
	}

	/**
	 * @Title encryptFile
	 * @Author zhongjyuan
	 * @Description 加密文件
	 * @Param @param secretKeyt 密钥
	 * @Param @param srcFile 待加密文件
	 * @Param @param destFile 已加密文件
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encryptFile(String secretKeyt, String srcFile, String destFile) {

		if (secretKeyt == null || secretKeyt.length() < 8)
			throw new RuntimeException("加密失败，key不能小于8位");

		try {
			IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, generateKey(secretKeyt), iv);
			InputStream is = new FileInputStream(srcFile);
			OutputStream out = new FileOutputStream(destFile);
			CipherInputStream cis = new CipherInputStream(is, cipher);
			byte[] buffer = new byte[1024];

			int r;
			while ((r = cis.read(buffer)) > 0) {
				out.write(buffer, 0, r);
			}

			cis.close();
			is.close();
			out.close();
			return destFile;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * @Title decryptFile
	 * @Author zhongjyuan
	 * @Description 解密文件
	 * @Param @param secretKeyt 密钥
	 * @Param @param srcFile 已加密文件
	 * @Param @param destFile 解密文件
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decryptFile(String secretKeyt, String srcFile, String destFile) {

		if (secretKeyt == null || secretKeyt.length() < 8)
			throw new RuntimeException("加密失败，key不能小于8位");

		try {
			File file = new File(destFile);
			if (!file.exists()) {
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			IvParameterSpec iv = new IvParameterSpec(IV_PARAMETER.getBytes(CHARSET));
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, generateKey(secretKeyt), iv);
			InputStream is = new FileInputStream(srcFile);
			OutputStream out = new FileOutputStream(destFile);
			CipherOutputStream cos = new CipherOutputStream(out, cipher);
			byte[] buffer = new byte[1024];

			int r;
			while ((r = is.read(buffer)) >= 0) {
				cos.write(buffer, 0, r);
			}

			cos.close();
			is.close();
			out.close();
			return destFile;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
