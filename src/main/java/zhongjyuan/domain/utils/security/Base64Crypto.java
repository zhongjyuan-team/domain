package zhongjyuan.domain.utils.security;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;

import zhongjyuan.domain.ITool;

/**
 * @ClassName: Base64Crypto
 * @author zhongjyuan
 * @date 2021年2月19日 下午6:28:53
 * @Description: Base64处理工具
 * 
 */
public class Base64Crypto implements ITool {

	private static final long serialVersionUID = 1L;

	private static final char[] legalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

	/**
	 * @Title encode
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param bytes
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	/**
	 * @Title encode
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param plaintext
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encode(String plaintext) {
		byte[] bytes = plaintext.getBytes();
		return encode(bytes);
	}

	/**
	 * @Title encodeParameter
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param parameters
	 * @Param @return
	 * @Param @throws UnsupportedEncodingException
	 * @Return String
	 * @Throws
	 */
	@SuppressWarnings("rawtypes")
	public static String encodeParameter(SortedMap<Object, Object> parameters) throws UnsupportedEncodingException {

		StringBuffer stringBuffer = new StringBuffer();
		Set set = parameters.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			String k = (String) entry.getKey();
			Object v = entry.getValue();
			if (null != v && !"".equals(v)) {
				stringBuffer.append(k + "=" + v + "&");
			}
		}
		stringBuffer.deleteCharAt(stringBuffer.lastIndexOf("&"));

		byte[] parameterByte = stringBuffer.toString().getBytes("UTF-8");
		return encodeUrl(parameterByte);
	}

	/**
	 * @Title encodeUrl
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param bytes
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encodeUrl(byte[] bytes) {

		int start = 0;
		int len = bytes.length;
		StringBuffer stringBuffer = new StringBuffer(bytes.length * 3 / 2);

		int end = len - 3;
		int i = start;
		int n = 0;

		while (i <= end) {
			int d = ((((int) bytes[i]) & 0x0ff) << 16) | ((((int) bytes[i + 1]) & 0x0ff) << 8) | (((int) bytes[i + 2]) & 0x0ff);

			stringBuffer.append(legalChars[(d >> 18) & 63]);
			stringBuffer.append(legalChars[(d >> 12) & 63]);
			stringBuffer.append(legalChars[(d >> 6) & 63]);
			stringBuffer.append(legalChars[d & 63]);

			i += 3;

			if (n++ >= 14) {
				n = 0;
				stringBuffer.append(" ");
			}
		}

		if (i == start + len - 2) {
			int d = ((((int) bytes[i]) & 0x0ff) << 16) | ((((int) bytes[i + 1]) & 255) << 8);

			stringBuffer.append(legalChars[(d >> 18) & 63]);
			stringBuffer.append(legalChars[(d >> 12) & 63]);
			stringBuffer.append(legalChars[(d >> 6) & 63]);
			stringBuffer.append("=");
		} else if (i == start + len - 1) {
			int d = (((int) bytes[i]) & 0x0ff) << 16;

			stringBuffer.append(legalChars[(d >> 18) & 63]);
			stringBuffer.append(legalChars[(d >> 12) & 63]);
			stringBuffer.append("==");
		}

		return stringBuffer.toString().replaceAll(" ", "");
	}

	/**
	 * @Title encodeUrl
	 * @Author zhongjyuan
	 * @Description 编码
	 * @Param @param plaintext
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encodeUrl(String plaintext) {
		return encodeUrl(plaintext.getBytes());
	}

	/**
	 * @Title decodToByte
	 * @Author zhongjyuan
	 * @Description 解码
	 * @Param @param ciphertext
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] decodToByte(String ciphertext) {
		return Base64.getDecoder().decode(ciphertext);
	}

	/**
	 * @Title decodToString
	 * @Author zhongjyuan
	 * @Description 解码
	 * @Param @param ciphertext
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decodToString(String ciphertext) {
		byte[] bytes = Base64.getDecoder().decode(ciphertext);
		return new String(bytes);
	}
}
