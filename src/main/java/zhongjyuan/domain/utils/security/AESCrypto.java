package zhongjyuan.domain.utils.security;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import zhongjyuan.domain.ITool;

/**
 * @ClassName: AESCrypto
 * @Description: AES工具类
 * @author: zhongjyuan
 * @date: 2022年3月3日 下午3:12:53
 * @Copyright: @2022 zhongjyuan.com
 */
public class AESCrypto implements ITool {

	private static final long serialVersionUID = 1L;
	/**
	 * @FieldType String
	 * @Fields CHARSET:(默认编码)
	 */
	private static final String CHARSET = "utf-8";
	/**
	 * @FieldType String
	 * @Fields ALGORITHM:(密钥算法)
	 */
	private static final String ALGORITHM = "AES";
	/**
	 * @FieldType String
	 * @Fields KEY:(16位加解密Key前后端一致)
	 */
	private static final String KEY = "cBssbHB3ZA==HKcc";
	/**
	 * @FieldType String
	 * @Fields CIPHER_ALGORITHM:(加密/解密算法-工作模式-填充模式)
	 */
	private static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

	public static String encrypt(String plaintext) {
		if (plaintext == null)
			return null;

		try {
			KeyGenerator kgen = KeyGenerator.getInstance(ALGORITHM);
			kgen.init(128);

			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(KEY.getBytes(), ALGORITHM));
			byte[] bytes = cipher.doFinal(plaintext.getBytes(CHARSET));

			return new String(Base64.getEncoder().encode(bytes));
		} catch (Exception e) {
			//			e.printStackTrace();
			return plaintext;
		}
	}

	public static String decrypt(String ciphertext) {
		if (ciphertext == null)
			return null;

		try {
			KeyGenerator kgen = KeyGenerator.getInstance(ALGORITHM);
			kgen.init(128);

			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(KEY.getBytes(), ALGORITHM));

			return new String(cipher.doFinal(Base64.getDecoder().decode(ciphertext.getBytes(CHARSET))), CHARSET);
		} catch (Exception e) {
			//			e.printStackTrace();
			return ciphertext;
		}
	}
}
