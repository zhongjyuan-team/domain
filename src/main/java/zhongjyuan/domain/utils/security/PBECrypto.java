package zhongjyuan.domain.utils.security;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import zhongjyuan.domain.ITool;

/**
 * @Project: eap-security
 * @Package: com.idea.eap.security.pbe
 * @ClassName: PBECrypto
 * @Description: PBE工具类
 * @author zhongjyuan
 * @date 2021年3月24日 下午5:08:57
 *
 */
public class PBECrypto implements ITool {

	private static final long serialVersionUID = 1L;

	public static final int ITERATION_COUNT = 100;

	public static final String ALGORITHM = "PBEWITHMD5andDES";

	/**
	 * @Title generateSalt
	 * @Author zhongjyuan
	 * @Description 生成盐
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] generateSalt() {
		try {
			SecureRandom random = new SecureRandom();
			return random.generateSeed(8);
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title generateSaltToString
	 * @Author zhongjyuan
	 * @Description 生成盐
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String generateSaltToString() {
		try {
			SecureRandom random = new SecureRandom();
			return new String(random.generateSeed(8));
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title generateKey
	 * @Author zhongjyuan
	 * @Description 生成Key
	 * @Param @param password
	 * @Param @return
	 * @Param @throws Exception
	 * @Return Key
	 * @Throws
	 */
	private static Key generateKey(String password) throws Exception {
		PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM);
		return secretKeyFactory.generateSecret(pbeKeySpec);
	}

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param bytes
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] encrypt(byte[] bytes, String secretKeyt, byte[] salt) {
		try {
			Key key = generateKey(secretKeyt);
			PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, ITERATION_COUNT);
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key, pbeParameterSpec);
			return cipher.doFinal(bytes);
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title encrypt
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] encrypt(String plaintext, String secretKeyt, byte[] salt) {
		try {
			return encrypt(plaintext.getBytes(), secretKeyt, salt);
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title encryptToString
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param bytes
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encryptToString(byte[] bytes, String secretKeyt, byte[] salt) {
		try {
			return new String(encrypt(bytes, secretKeyt, salt));
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title encryptToString
	 * @Author zhongjyuan
	 * @Description 加密
	 * @Param @param plaintext
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String encryptToString(String plaintext, String secretKeyt, byte[] salt) {
		try {
			return new String(encrypt(plaintext.getBytes(), secretKeyt, salt));
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param bytes
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] decrypt(byte[] bytes, String secretKeyt, byte[] salt) {
		try {
			Key key = generateKey(secretKeyt);
			PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, ITERATION_COUNT);
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key, pbeParameterSpec);
			return cipher.doFinal(bytes);
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title decrypt
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return byte[]
	 * @Throws
	 */
	public static byte[] decrypt(String ciphertext, String secretKeyt, byte[] salt) {
		try {
			return decrypt(ciphertext.getBytes(), secretKeyt, salt);
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title decryptToString
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param bytes
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decryptToString(byte[] bytes, String secretKeyt, byte[] salt) {
		try {
			return new String(decrypt(bytes, secretKeyt, salt));
		} catch (Exception x) {
			return null;
		}
	}

	/**
	 * @Title decryptToString
	 * @Author zhongjyuan
	 * @Description 解密
	 * @Param @param ciphertext
	 * @Param @param secretKeyt
	 * @Param @param salt
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String decryptToString(String ciphertext, String secretKeyt, byte[] salt) {
		try {
			return new String(decrypt(ciphertext.getBytes(), secretKeyt, salt));
		} catch (Exception x) {
			return null;
		}
	}
}
