package zhongjyuan.domain.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import zhongjyuan.domain.ITool;

/**
 * @className: I18nUtil
 * @description: 多语言工具
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午11:41:38
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class I18nUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @className: Language
	 * @description: 语言包接口
	 * @author: zhongjyuan
	 * @date: 2022年7月14日 上午11:41:27
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public interface Language {

		/**
		 * @title: message
		 * @author: zhongjyuan
		 * @description: 获取对应语言的文本
		 * @param key 字符串key
		 * @param args 格式化参数
		 * @return 语言文本内容
		 */
		String message(String key, Object... args);
	}

	/**
	 * @title: message
	 * @author: zhongjyuan
	 * @description: 获取对应语言的文本，当资源文件或key不存在时，直接返回
	 * @param locale 语言
	 * @param bundleName 资源文件名
	 * @param key 字符串key
	 * @param args 格式化参数
	 * @return 语言文本内容
	 */
	public static String message(Locale locale, String bundleName, String key, Object... args) {
		ResourceBundle bundle;
		try {
			bundle = ResourceBundle.getBundle(bundleName, locale);
		} catch (Exception e) {
			bundle = null;
		}
		try {
			return MessageFormat.format(bundle.getString(key), args);
		} catch (MissingResourceException e) {
			return MessageFormat.format(key, args);
		}
	}

	/**
	 * @title: message
	 * @author: zhongjyuan
	 * @description: 获取对应语言的文本，当资源文件或key不存在时，直接返回
	 * @param bundleName 资源文件名
	 * @param key 字符串key
	 * @param args 格式化参数
	 * @return 语言文本内容
	 */
	public static String message(String bundleName, String key, Object... args) {
		return message(Locale.getDefault(), bundleName, key, args);
	}

	/**
	 * @title: language
	 * @author: zhongjyuan
	 * @description: 获取语言包
	 * @param locale 语言
	 * @param bundleName 语言包名称
	 * @return {@link Language}
	 */
	public static Language language(Locale locale, String bundleName) {
		return (key, args) -> message(locale, bundleName, key, args);
	}

	/**
	 * @title: language
	 * @author: zhongjyuan
	 * @description: 获取语言包
	 * @param bundleName 语言包名称
	 * @return {@link Language}
	 */
	public static Language language(String bundleName) {
		return language(Locale.getDefault(), bundleName);
	}
}
