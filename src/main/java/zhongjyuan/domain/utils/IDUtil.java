package zhongjyuan.domain.utils;

import org.apache.commons.lang3.StringUtils;

import zhongjyuan.domain.ITool;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @className: IDUtil
 * @description: 高效GUID产生算法(sequence),基于Snowflake实现64位自增ID算法。
 * @author: zhongjyuan
 * @date: 2022年7月27日 上午11:45:08
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class IDUtil extends UUIDUtil implements ITool {

	private static final long serialVersionUID = 1L;

	private static Sequence WORKER = new Sequence();

	public static final DateTimeFormatter MILLISECOND = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

	public static long getId() {
		return WORKER.nextId();
	}

	public static String getIdStr() {
		return String.valueOf(WORKER.nextId());
	}

	public static String getMillisecond() {
		return LocalDateTime.now().format(MILLISECOND);
	}

	public static String getTimeId() {
		return getMillisecond() + getId();
	}

	public static void initSequence(long workerId, long datacenterId) {
		WORKER = new Sequence(workerId, datacenterId);
	}

	public static String get32UUID() {
		ThreadLocalRandom random = ThreadLocalRandom.current();
		return (new UUID(random.nextLong(), random.nextLong())).toString().replace("-", "");
	}
}

class Sequence {

	private final long workerId;

	private final long datacenterId;

	private long sequence = 0L;

	private long lastTimestamp = -1L;

	public Sequence() {
		this.datacenterId = getDatacenterId(31L);
		this.workerId = getMaxWorkerId(this.datacenterId, 31L);
	}

	public Sequence(long workerId, long datacenterId) {
		this.workerId = workerId;
		this.datacenterId = datacenterId;
	}

	protected static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
		StringBuilder mpid = new StringBuilder();
		mpid.append(datacenterId);
		String name = ManagementFactory.getRuntimeMXBean().getName();
		if (StringUtils.isNotEmpty(name)) {
			mpid.append(name.split("@")[0]);
		}

		return (long) (mpid.toString().hashCode() & '\uffff') % (maxWorkerId + 1L);
	}

	protected static long getDatacenterId(long maxDatacenterId) {
		long id = 0L;
		try {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			if (network == null) {
				id = 1L;
			} else {
				byte[] mac = network.getHardwareAddress();
				if (null != mac) {
					id = (255L & (long) mac[mac.length - 1] | 65280L & (long) mac[mac.length - 2] << 8) >> 6;
					id %= maxDatacenterId + 1L;
				}
			}
		} catch (Exception var7) {
			var7.printStackTrace();
		}
		return id;
	}

	public synchronized long nextId() {
		long timestamp = this.timeGen();
		if (timestamp < this.lastTimestamp) {
			long offset = this.lastTimestamp - timestamp;
			if (offset > 5L) {
				throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
			}
			try {
				this.wait(offset << 1);
				timestamp = this.timeGen();
				if (timestamp < this.lastTimestamp) {
					throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", offset));
				}
			} catch (Exception var6) {
				throw new RuntimeException(var6);
			}
		}

		if (this.lastTimestamp == timestamp) {
			this.sequence = this.sequence + 1L & 4095L;
			if (this.sequence == 0L) {
				timestamp = this.tilNextMillis(this.lastTimestamp);
			}
		} else {
			this.sequence = ThreadLocalRandom.current().nextLong(1L, 3L);
		}

		this.lastTimestamp = timestamp;
		return timestamp - 1288834974657L << 22 | this.datacenterId << 17 | this.workerId << 12 | this.sequence;
	}

	protected long tilNextMillis(long lastTimestamp) {
		long timestamp = timeGen();
		while (timestamp <= lastTimestamp) {
			timestamp = timeGen();
		}
		return timestamp;
	}

	protected long timeGen() {
		return System.currentTimeMillis();
	}
}
