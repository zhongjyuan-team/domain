package zhongjyuan.domain.utils.image;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;

import javax.imageio.ImageIO;

import zhongjyuan.domain.ITool;

/**
 * @ClassName: ImageUtil
 * @Description: (图片处理工具类)
 * @author: zhongjyuan
 * @date: 2022年6月14日 上午11:39:01
 * @Copyright: zhongjyuan.com
 */
public class ImageUtil implements ITool {

	private static final long serialVersionUID = 1L;

	private static final String FILE_FORMAT = "PNG";

	// 1.图片切圆角

	/**
	 * <p>
	 * 功能描述: [图片生成圆角,默认全部生成]
	 * </p>
	 * 
	 * @Title makeRoundedCorner
	 * @param image
	 * @return
	 */
	public static BufferedImage makeRoundedCorner(BufferedImage image) {
		return makeRoundedCorner(image, null);
	}

	/**
	 * <p>
	 * 功能描述: [图片切圆角]
	 * </p>
	 * 
	 * @Title makeRoundedCorner
	 * @param image        需要切圆角的图片
	 * @param cornerRadius 圆角的弧度大小,0为直角,越大越偏向圆形
	 * @return
	 */
	public static BufferedImage makeRoundedCorner(BufferedImage image, Integer cornerRadius) {
		return makeRoundedCorner(image, cornerRadius, false, false, false, false);
	}

	/**
	 * <p>
	 * 功能描述: [图片生成圆角,支持自定义哪些角生成圆角,默认全生成]
	 * </p>
	 * 
	 * @Title makeRoundedCorner
	 * @param image        图片
	 * @param cornerRadius 圆弧角度
	 * @param leftUp       左上,为true不生成
	 * @param leftDown     左下,为true不生成
	 * @param rightUp      右上,为true不生成
	 * @param rightDown    右下,为true不生成
	 * @return
	 */
	public static BufferedImage makeRoundedCorner(BufferedImage image, Integer cornerRadius, Boolean leftUp, Boolean leftDown, Boolean rightUp, Boolean rightDown) {
		Integer cornerRadiusTemp = cornerRadius == null ? (image.getWidth() + image.getHeight()) / 8 : cornerRadius;
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = output.createGraphics();
		g2.setComposite(AlphaComposite.Src);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadiusTemp, cornerRadiusTemp));
		if (leftUp) {
			g2.fillRect(0, 0, cornerRadiusTemp, cornerRadiusTemp);// 恢复左上直角
		}
		if (leftDown) {
			g2.fillRect(0, 0 + h - cornerRadiusTemp, cornerRadiusTemp, cornerRadiusTemp); // 恢复左下直角
		}
		if (rightUp) {
			g2.fillRect(0 + w - cornerRadiusTemp, 0, cornerRadiusTemp, cornerRadiusTemp);// 恢复右上直角
		}
		if (rightDown) {
			g2.fillRect(w - cornerRadiusTemp, h - cornerRadiusTemp, cornerRadiusTemp, cornerRadiusTemp);// 恢复右下直角
		}
		g2.setComposite(AlphaComposite.SrcAtop);
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
		return output;
	}

	// 2.图片生成边框

	/**
	 * <p>
	 * 功能描述: [图片生成边框,默认圆角]
	 * </p>
	 * 
	 * @Title makeRoundBorder
	 * @param image 图片
	 * @param color 边框颜色
	 * @return
	 */
	public static BufferedImage makeRoundBorder(BufferedImage image, Color color) {
		return makeRoundBorder(image, color, null, null);
	}

	/**
	 * <p>
	 * 功能描述: [图片生成边框]
	 * </p>
	 * 
	 * @Title makeRoundBorder
	 * @param image        图片
	 * @param color        边框颜色
	 * @param size         边框边距
	 * @param cornerRadius 圆弧角度
	 * @return
	 */
	public static BufferedImage makeRoundBorder(BufferedImage image, Color color, Integer size, Integer cornerRadius) {
		return makeRoundBorder(image, color, size, cornerRadius, false, false, false, false);
	}

	/**
	 * <p>
	 * 功能描述: [图片生成边框]
	 * </p>
	 * 
	 * @Title makeRoundBorder
	 * @param image        图片
	 * @param color        边框颜色
	 * @param size         边框边距
	 * @param cornerRadius 圆弧角度
	 * @return
	 */
	public static BufferedImage makeRoundBorder(BufferedImage image, Color color, Integer size, Integer cornerRadius, Boolean leftUp, Boolean leftDown, Boolean rightUp, Boolean rightDown) {
		Integer sizeTemp = size == null ? (image.getWidth() + image.getHeight()) / 40 : size;
		int w = image.getWidth() + sizeTemp;
		int h = image.getHeight() + sizeTemp;
		Integer cornerRadiusTemp = cornerRadius == null ? (w + h) / 8 : cornerRadius;
		BufferedImage imageTemp = makeRoundedCorner(image, Math.abs(cornerRadiusTemp - sizeTemp), leftUp, leftDown, rightUp, rightDown);
		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = output.createGraphics();
		g2.setComposite(AlphaComposite.Src);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(color == null ? Color.WHITE : color);
		g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadiusTemp, cornerRadiusTemp));
		if (leftUp) {
			g2.fillRect(0, 0, cornerRadiusTemp, cornerRadiusTemp);// 恢复左上直角

		}
		if (leftDown) {
			g2.fillRect(0, 0 + h - cornerRadiusTemp, cornerRadiusTemp, cornerRadiusTemp); // 恢复左下直角
		}
		if (rightUp) {
			g2.fillRect(0 + w - cornerRadiusTemp, 0, cornerRadiusTemp, cornerRadiusTemp);// 恢复右上直角
		}
		if (rightDown) {
			g2.fillRect(w - cornerRadiusTemp, h - cornerRadiusTemp, cornerRadiusTemp, cornerRadiusTemp);// 恢复右下直角
		}
		g2.setComposite(AlphaComposite.SrcAtop);
		g2.drawImage(imageTemp, sizeTemp >> 1, sizeTemp >> 1, null);
		g2.dispose();
		return output;
	}

	// 3.添加背景图片

	/**
	 * <p>
	 * 功能描述: [给图片添加背景图片]
	 * </p>
	 * 
	 * @Title addBackImage
	 * @param source  原图片
	 * @param imgPath 背景图片地址
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public static BufferedImage addBackImage(BufferedImage source, String imgPath) throws MalformedURLException, IOException {
		BufferedImage image = getImageByPath(imgPath);
		return addBackImage(source, image);
	}

	public static BufferedImage addBackImage(BufferedImage source, BufferedImage backgroundImage) {
		return addBackImage(source, backgroundImage, null, null, 1.0f);
	}

	/**
	 * <p>
	 * 功能描述: [添加背景图片]
	 * </p>
	 * 
	 * @Title addBackImage
	 * @param source  原图片
	 * @param imgPath 背景图片路径
	 * @param disx    原图片到背景图片x轴方向距离
	 * @param disy    原图片到背景图片y轴方向距离
	 * @param disy    原图片到背景图片y轴方向距离
	 * @return aplha 原图片透明度 取值范围0~1.0f
	 * @throws Exception
	 */
	public static BufferedImage addBackImage(BufferedImage source, BufferedImage backgroundImage, Integer disx, Integer disy, Float aplha) {
		if (backgroundImage == null) {
			return source;
		}
		int bgWidth = backgroundImage.getWidth() >= source.getWidth() ? backgroundImage.getWidth() : source.getWidth();
		int bgheight = backgroundImage.getHeight() >= source.getHeight() ? backgroundImage.getHeight() : source.getHeight();
		int qrWidth = source.getWidth();
		// 距离背景图片x边的距离，默认居中显示
		//		disx = disx == null ? (bgWidth - qrWidth) / 2 : disx;
		//		// 距离背景图片y边的距离，默认居中显示
		//		disy = disy == null ? (bgheight - qrWidth) / 2 : disy;
		//		aplha = aplha == null ? 1.0f : aplha;
		Graphics2D rng = backgroundImage.createGraphics();
		rng.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, aplha == null ? 1.0f : aplha));// 透明度
		rng.drawImage(source, disx == null ? (bgWidth - qrWidth) / 2 : disx, disy == null ? (bgheight - qrWidth) / 2 : disy, qrWidth, qrWidth, null);
		rng.drawImage(backgroundImage, 0, 0, null);
		return backgroundImage;
	}

	// 4.获取图片

	/**
	 * <p>
	 * 功能描述: [根据路径图片图片]
	 * </p>
	 * 
	 * @Title getImageByPath
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public static BufferedImage getImageByPath(String path) throws MalformedURLException, IOException {
		if (path.startsWith("http")) { // 从网络获取logo
			return ImageIO.read(new URL(path));
		} else { // 从资源目录下获取图片
			File file = new File(path);
			if (!file.exists()) {
				return null;
			}
			return ImageIO.read(file);
		}
	}

	// 5.图片添加文本说明

	/**
	 * <p>
	 * 功能描述: [ ]
	 * </p>
	 * 
	 * @Title addText
	 * @param text
	 * @param img
	 * @return
	 */
	public static BufferedImage addText(String text, BufferedImage img) {
		return addText(text, img, null, null, null, null, null, null, 0.3f);
	}

	/**
	 * <p>
	 * 功能描述: [添加文本]
	 * </p>
	 * 
	 * @Title addText
	 * @param text      文字内容
	 * @param img       图片
	 * @param fontName  字体
	 * @param fontStyle 样式
	 * @param color     颜色(可设置透明度)
	 * @param fontSize  大小
	 * @param x         x坐标 以左上角为0点
	 * @param y         y坐标 以左上角为0点
	 * @return
	 */
	public static BufferedImage addText(String text, BufferedImage img, String fontName, Integer fontStyle, Color color, Integer fontSize, Integer x, Integer y, Float alpha) {
		// 目标文件
		int width = img.getWidth(null);
		int height = img.getHeight(null);
		Graphics2D g = img.createGraphics();
		g.drawImage(img, 0, 0, width, height, null);
		g.setColor(color == null ? Color.BLACK : color);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha == null ? 0.3f : alpha));
		g.setFont(new Font(fontName == null ? "微软雅黑,Arial" : fontName, fontStyle == null ? Font.BOLD : fontStyle, fontSize == null ? 32 : fontSize));
		g.drawString(text, (x == null ? g.getFontMetrics().stringWidth(text) + width / 2 : x), (y == null ? height / 2 : y) + (fontSize == null ? 32 : fontSize));
		g.dispose();
		return img;
	}

	// 6.图片添加logo

	/**
	 * <p>
	 * 功能描述: [添加logo]
	 * </p>
	 * 
	 * @Title insertLogoImage
	 * @param source  原图片
	 * @param imgPath logo图片路径
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static BufferedImage insertLogoImage(BufferedImage source, String imgPath) throws IOException {
		BufferedImage backgroundImage = getImageByPath(imgPath);
		if (backgroundImage == null) {
			return source;
		}
		BufferedImage src = ImageIO.read(new File(imgPath));
		return insertLogoImage(source, src);
	}

	/**
	 * <p>
	 * 功能描述: [添加logo]
	 * </p>
	 * 
	 * @Title insertLogoImage
	 * @param source  原图片
	 * @param imgPath logo图片 base64字符串
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static BufferedImage insertLogoImageByBase64(BufferedImage source, String imgPath) throws IOException {
		BufferedImage backgroundImage = base64ToImage(imgPath);
		if (backgroundImage == null) {
			return source;
		}
		BufferedImage src = ImageIO.read(new File(imgPath));
		return insertLogoImage(source, src);
	}

	public static BufferedImage insertLogoImage(BufferedImage source, BufferedImage src) {
		return insertLogoImage(source, src, null, null, 6);
	}

	/**
	 * <p>
	 * 功能描述: [添加图片logo]
	 * </p>
	 * 
	 * @Title insertLogoImage
	 * @param source
	 * @param imgPath
	 * @param disx    logo图片到原图片x方向距离
	 * @param disy    logo图片到原图片y方向距离
	 * @param radio   logo图片缩放倍数(相对于目标图片),比如6就是目标图片的1/6
	 * @throws Exception
	 */
	public static BufferedImage insertLogoImage(BufferedImage source, BufferedImage src, Integer disx, Integer disy, Integer radio) {
		//radio = radio == null ? 6 : radio;
		// 压缩logo
		int width = source.getWidth() / (radio == null ? 6 : radio);
		int height = source.getHeight() / (radio == null ? 6 : radio);

		// 距离背景图片x边的距离，默认居中显示
		//disx = disx == null ? (source.getWidth() - width) / 2 : disx;
		// 距离背景图片y边的距离，默认居中显示
		//disy = disy == null ? (source.getHeight() - height) / 2 : disy;

		// 插入LOGO
		Graphics2D graph = source.createGraphics();
		graph.drawImage(src, disx == null ? (source.getWidth() - width) / 2 : disx, disy == null ? (source.getHeight() - height) / 2 : disy, width, height, null);
		graph.drawImage(source, 0, 0, null);
		graph.dispose();
		src.flush();

		return source;
	}

	// 7.图片保存转换

	/**
	 * <p>
	 * 功能描述: [图片生成到指定路径]
	 * </p>
	 * 
	 * @Title toFile
	 * @param image
	 * @param targetFile
	 * @throws IOException
	 * @throws Exception
	 */
	public static void toFile(BufferedImage image, String targetFile) throws IOException {
		mkdirs(targetFile);
		String suffix = targetFile.substring(targetFile.lastIndexOf('.') + 1);
		ImageIO.write(image, suffix, new File(targetFile));
	}

	/**
	 * <p>
	 * 功能描述: [图片转base64字符串]
	 * </p>
	 * 
	 * @Title toBase64String
	 * @param output
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static String toBase64String(BufferedImage image) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(image, FILE_FORMAT, output);
		String base64 = Base64.getEncoder().encodeToString(output.toByteArray());
		return base64;
	}

	/**
	 * <p>
	 * 功能描述: [base64字符串转图片]
	 * </p>
	 * 
	 * @Title toBase64String
	 * @param base64
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static BufferedImage base64ToImage(String base64) throws IOException {
		byte[] b = Base64.getDecoder().decode(base64);
		ByteArrayInputStream input = new ByteArrayInputStream(b);
		BufferedImage image = ImageIO.read(input);
		return image;
	}

	/**
	 * <p>
	 * 功能描述: [ 图片以byte[]格式展示]
	 * </p>
	 * 
	 * @Title toByte
	 * @param image
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static byte[] toByte(BufferedImage image) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(image, FILE_FORMAT, output);
		return output.toByteArray();
	}

	/**
	 * <p>
	 * 功能描述: [输出图片到流]
	 * </p>
	 * 
	 * @Title toStream
	 * @param image
	 * @param output
	 * @throws IOException
	 * @throws Exception
	 */
	public static void toStream(BufferedImage image, OutputStream output) throws IOException {
		ImageIO.write(image, FILE_FORMAT, output);
	}

	// 8.图片缩放
	/**
	 * <p>
	 * 功能描述: [图片缩放]
	 * </p>
	 * 
	 * @Title imageScale
	 * @param imagePth
	 * @param radio
	 * @param scale
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 * @throws Exception
	 */
	public static BufferedImage imageScale(String imagePth, Integer radio, Boolean scale) throws MalformedURLException, IOException {
		BufferedImage image = getImageByPath(imagePth);
		return imageScale(image, radio, scale);
	}

	/**
	 * <p>
	 * 功能描述: [图片缩放]
	 * </p>
	 * 
	 * @Title imageScale
	 * @param image 原图片
	 * @param radio 放大缩小倍数
	 * @param scale 放大还是缩小 默认false缩小 true放大
	 * @return
	 */
	public static BufferedImage imageScale(BufferedImage image, Integer radio, Boolean scale) {
		int width;
		int height;
		if (scale) {
			width = image.getWidth() * radio;
			height = image.getHeight() * radio;
		} else {
			width = image.getWidth() / radio;
			height = image.getHeight() / radio;
		}
		return imageScale(image, width, height);
	}

	/**
	 * <p>
	 * 功能描述: [图片缩放]
	 * </p>
	 * 
	 * @Title imageScale
	 * @param image
	 * @param width
	 * @param height
	 * @return
	 */
	public static BufferedImage imageScale(BufferedImage image, Integer width, Integer height) {
		Image src = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage tag = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g = tag.getGraphics();
		g.drawImage(src, 0, 0, null); // 绘制缩小后的图
		g.dispose();
		return tag;
	}

	private static void mkdirs(String destPath) {
		File file = new File(destPath);
		// 当文件夹不存在时，mkdirs会自动创建多层目录，区别于mkdir．(mkdir如果父目录不存在则会抛出异常)
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
	}
}
