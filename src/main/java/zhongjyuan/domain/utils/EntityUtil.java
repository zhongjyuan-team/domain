package zhongjyuan.domain.utils;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import zhongjyuan.domain.Constant;
import zhongjyuan.domain.ITool;

/**
 * @className: EntityUtil
 * @description: 实体工具类
 * @author: zhongjyuan
 * @date: 2020年12月5日 下午5:05:48
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class EntityUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @Title setCreation
	 * @Author zhongjyuan
	 * @Description 赋值创建相关字段值
	 * @Param @param <T> 实体泛型对象
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	public static <T> void setCreation(T entity) {
		setCreation(entity, null);
	}

	/**
	 * @Title setCreation
	 * @Author zhongjyuan
	 * @Description 赋值创建相关字段值
	 * @Param @param <T> 实体对象 - 泛型对象
	 * @Param @param entity 实体对象
	 * @Param @param params 自定义字段值Map
	 * @Return void
	 * @Throws
	 */
	public static <T> void setCreation(T entity, Map<String, Object> params) {

		LocalDateTime currentTime = LocalDateTime.now();

		String userId = Constant.ANONYMOUS_L;
		String userName = Constant.ANONYMOUS_L;

		String tenantCode = Constant.ANONYMOUS_L;
		String tenantName = Constant.ANONYMOUS_L;

		try {

			Class<?> objClass = entity.getClass();

			// 获取当前类和其父类的所有字段（包括受保护字段）
			List<Field> allFields = new ArrayList<Field>();

			Class<?> currentClass = objClass;
			while (currentClass != null) {
				Field[] declaredFields = currentClass.getDeclaredFields();
				allFields.addAll(Arrays.asList(declaredFields));
				currentClass = currentClass.getSuperclass();
			}

			String fieldName;
			Class<?> fieldType;

			for (Field field : allFields) {
				field.setAccessible(true);

				fieldName = field.getName();
				fieldType = field.getType();

				switch (fieldName) {
					case Constant.ID_L:
					case Constant.ID_U:
					case Constant.ID_U_A:
						if (fieldType == Long.class)
							field.set(entity, IDUtil.getId());
						if (fieldType == String.class)
							field.set(entity, UUIDUtil.randomUUID(false, false));
						break;
					case Constant.IS_LEAF_L:
					case Constant.IS_LEAF_L_A:
					case Constant.IS_LEAF_HUMP_L_A:
					case Constant.IS_LEAF_U:
					case Constant.IS_LEAF_U_A:
					case Constant.IS_LEAF_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 1);
						if (fieldType == Boolean.class)
							field.set(entity, true);
						break;
					case Constant.LEVEL_L:
					case Constant.LEVEL_U:
					case Constant.LEVEL_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.OUTLINE_L:
					case Constant.OUTLINE_U:
					case Constant.OUTLINE_U_A:
						if (fieldType == String.class)
							field.set(entity, "");
						break;
					case Constant.IS_SYSTEM_L:
					case Constant.IS_SYSTEM_L_A:
					case Constant.IS_SYSTEM_HUMP_L_A:
					case Constant.IS_SYSTEM_U:
					case Constant.IS_SYSTEM_U_A:
					case Constant.IS_SYSTEM_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.IS_ENABLED_L:
					case Constant.IS_ENABLED_L_A:
					case Constant.IS_ENABLED_HUMP_L_A:
					case Constant.IS_ENABLED_U:
					case Constant.IS_ENABLED_U_A:
					case Constant.IS_ENABLED_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 1);
						if (fieldType == Boolean.class)
							field.set(entity, true);
						break;
					case Constant.IS_DELETED_L:
					case Constant.IS_DELETED_L_A:
					case Constant.IS_DELETED_HUMP_L_A:
					case Constant.IS_DELETED_U:
					case Constant.IS_DELETED_U_A:
					case Constant.IS_DELETED_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.IS_EXTERNAL_L:
					case Constant.IS_EXTERNAL_L_A:
					case Constant.IS_EXTERNAL_HUMP_L_A:
					case Constant.IS_EXTERNAL_U:
					case Constant.IS_EXTERNAL_U_A:
					case Constant.IS_EXTERNAL_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.IS_INTERNAL_L:
					case Constant.IS_INTERNAL_L_A:
					case Constant.IS_INTERNAL_HUMP_L_A:
					case Constant.IS_INTERNAL_U:
					case Constant.IS_INTERNAL_U_A:
					case Constant.IS_INTERNAL_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 1);
						if (fieldType == Boolean.class)
							field.set(entity, true);
						break;
					case Constant.IS_BUSINESS_L:
					case Constant.IS_BUSINESS_L_A:
					case Constant.IS_BUSINESS_HUMP_L_A:
					case Constant.IS_BUSINESS_U:
					case Constant.IS_BUSINESS_U_A:
					case Constant.IS_BUSINESS_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 1);
						if (fieldType == Boolean.class)
							field.set(entity, true);
						break;
					case Constant.IS_LOCK_L:
					case Constant.IS_LOCK_L_A:
					case Constant.IS_LOCK_HUMP_L_A:
					case Constant.IS_LOCK_U:
					case Constant.IS_LOCK_U_A:
					case Constant.IS_LOCK_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.ROW_INDEX_L:
					case Constant.ROW_INDEX_L_A:
					case Constant.ROW_INDEX_HUMP_L_A:
						if (fieldType == Integer.class)
							field.set(entity, 10);
						if (fieldType == String.class)
							field.set(entity, "10");
						break;
					case Constant.TENANT_ID_L:
					case Constant.TENANT_ID_L_A:
					case Constant.TENANT_ID_HUMP_L_A:
					case Constant.TENANT_ID_U:
					case Constant.TENANT_ID_U_A:
					case Constant.TENANT_ID_HUMP_U_A:
					case Constant.TENANT_CODE_L:
					case Constant.TENANT_CODE_L_A:
					case Constant.TENANT_CODE_HUMP_L_A:
					case Constant.TENANT_CODE_U:
					case Constant.TENANT_CODE_U_A:
					case Constant.TENANT_CODE_HUMP_U_A:
						if (fieldType == String.class)
							field.set(entity, tenantCode);
						break;
					case Constant.TENANT_NAME_L:
					case Constant.TENANT_NAME_L_A:
					case Constant.TENANT_NAME_HUMP_L_A:
					case Constant.TENANT_NAME_U:
					case Constant.TENANT_NAME_U_A:
					case Constant.TENANT_NAME_HUMP_U_A:
						if (fieldType == String.class)
							field.set(entity, tenantName);
						break;
					case Constant.CREATOR_ID_L:
					case Constant.CREATOR_ID_L_A:
					case Constant.CREATOR_ID_HUMP_L_A:
					case Constant.CREATOR_ID_U:
					case Constant.CREATOR_ID_U_A:
					case Constant.CREATOR_ID_HUMP_U_A:
					case Constant.UPDATOR_ID_L:
					case Constant.UPDATOR_ID_L_A:
					case Constant.UPDATOR_ID_HUMP_L_A:
					case Constant.UPDATOR_ID_U:
					case Constant.UPDATOR_ID_U_A:
					case Constant.UPDATOR_ID_HUMP_U_A:
						if (fieldType == Long.class)
							field.set(entity, Long.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == Integer.class)
							field.set(entity, Integer.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == String.class)
							field.set(entity, userId);
						break;
					case Constant.CREATOR_NAME_L:
					case Constant.CREATOR_NAME_L_A:
					case Constant.CREATOR_NAME_HUMP_L_A:
					case Constant.CREATOR_NAME_U:
					case Constant.CREATOR_NAME_U_A:
					case Constant.CREATOR_NAME_HUMP_U_A:
					case Constant.UPDATOR_NAME_L:
					case Constant.UPDATOR_NAME_L_A:
					case Constant.UPDATOR_NAME_HUMP_L_A:
					case Constant.UPDATOR_NAME_U:
					case Constant.UPDATOR_NAME_U_A:
					case Constant.UPDATOR_NAME_HUMP_U_A:
						if (fieldType == String.class)
							field.set(entity, userName);
						break;
					case Constant.CREATE_TIME_L:
					case Constant.CREATE_TIME_L_A:
					case Constant.CREATE_TIME_HUMP_L_A:
					case Constant.CREATE_TIME_U:
					case Constant.CREATE_TIME_U_A:
					case Constant.CREATE_TIME_HUMP_U_A:
					case Constant.UPDATE_TIME_L:
					case Constant.UPDATE_TIME_L_A:
					case Constant.UPDATE_TIME_HUMP_L_A:
					case Constant.UPDATE_TIME_U:
					case Constant.UPDATE_TIME_U_A:
					case Constant.UPDATE_TIME_HUMP_U_A:
						if (fieldType == LocalDateTime.class)
							field.set(entity, currentTime);
						break;
					default:
						if (params != null && params.containsKey(fieldName)) {
							field.set(entity, params.get(fieldName));
						}
						break;
				}
			}
		} catch (IllegalAccessException e) {

		}
	}

	/**
	 * @Title setEdition
	 * @Author zhongjyuan
	 * @Description 赋值更新相关字段值
	 * @Param @param <T> 实体对象 - 泛型对象
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	public static <T> void setEdition(T entity) {
		setEdition(entity, null);
	}

	/**
	 * @Title setEdition
	 * @Author zhongjyuan
	 * @Description 赋值更新相关字段值
	 * @Param @param <T> 实体对象 - 泛型对象
	 * @Param @param entity 实体对象
	 * @Param @param params 自定义字段值Map
	 * @Return void
	 * @Throws
	 */
	public static <T> void setEdition(T entity, Map<String, Object> params) {

		LocalDateTime currentTime = LocalDateTime.now();

		String userId = Constant.ANONYMOUS_L;
		String userName = Constant.ANONYMOUS_L;

		try {
			Class<?> objClass = entity.getClass();

			// 获取当前类和其父类的所有字段（包括受保护字段）
			List<Field> allFields = new ArrayList<Field>();

			Class<?> currentClass = objClass;
			while (currentClass != null) {
				Field[] declaredFields = currentClass.getDeclaredFields();
				allFields.addAll(Arrays.asList(declaredFields));
				currentClass = currentClass.getSuperclass();
			}

			String fieldName;
			Class<?> fieldType;

			for (Field field : allFields) {
				field.setAccessible(true);

				fieldName = field.getName();
				fieldType = field.getType();

				switch (fieldName) {
					case Constant.UPDATOR_ID_L:
					case Constant.UPDATOR_ID_L_A:
					case Constant.UPDATOR_ID_HUMP_L_A:
					case Constant.UPDATOR_ID_U:
					case Constant.UPDATOR_ID_U_A:
					case Constant.UPDATOR_ID_HUMP_U_A:
						if (fieldType == Long.class)
							field.set(entity, Long.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == Integer.class)
							field.set(entity, Integer.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == String.class)
							field.set(entity, userId);
						break;
					case Constant.UPDATOR_NAME_L:
					case Constant.UPDATOR_NAME_L_A:
					case Constant.UPDATOR_NAME_HUMP_L_A:
					case Constant.UPDATOR_NAME_U:
					case Constant.UPDATOR_NAME_U_A:
					case Constant.UPDATOR_NAME_HUMP_U_A:
						if (fieldType == String.class)
							field.set(entity, userName);
						break;
					case Constant.UPDATE_TIME_L:
					case Constant.UPDATE_TIME_L_A:
					case Constant.UPDATE_TIME_HUMP_L_A:
					case Constant.UPDATE_TIME_U:
					case Constant.UPDATE_TIME_U_A:
					case Constant.UPDATE_TIME_HUMP_U_A:
						if (fieldType == LocalDateTime.class)
							field.set(entity, currentTime);
						break;
					default:
						if (params != null && params.containsKey(fieldName)) {
							field.set(entity, params.get(fieldName));
						}
						break;
				}
			}
		} catch (IllegalAccessException e) {

		}
	}

	/**
	 * @Title setDeletion
	 * @Author zhongjyuan
	 * @Description 赋值移除相关字段值
	 * @Param @param <T> 实体对象 - 泛型对象
	 * @Param @param entity 实体对象
	 * @Return void
	 * @Throws
	 */
	public static <T> void setDeletion(T entity) {
		setDeletion(entity, null);
	}

	/**
	 * @Title setDeletion
	 * @Author zhongjyuan
	 * @Description 赋值移除相关字段值
	 * @Param @param <T> 实体对象 - 泛型对象
	 * @Param @param entity 实体对象
	 * @Param @param params 自定义字段值Map
	 * @Return void
	 * @Throws
	 */
	public static <T> void setDeletion(T entity, Map<String, Object> params) {

		LocalDateTime currentTime = LocalDateTime.now();

		String userId = Constant.ANONYMOUS_L;
		String userName = Constant.ANONYMOUS_L;

		try {
			Class<?> objClass = entity.getClass();

			// 获取当前类和其父类的所有字段（包括受保护字段）
			List<Field> allFields = new ArrayList<Field>();

			Class<?> currentClass = objClass;
			while (currentClass != null) {
				Field[] declaredFields = currentClass.getDeclaredFields();
				allFields.addAll(Arrays.asList(declaredFields));
				currentClass = currentClass.getSuperclass();
			}

			String fieldName;
			Class<?> fieldType;

			for (Field field : allFields) {
				field.setAccessible(true);

				fieldName = field.getName();
				fieldType = field.getType();

				switch (fieldName) {
					case Constant.IS_ENABLED_L:
					case Constant.IS_ENABLED_L_A:
					case Constant.IS_ENABLED_HUMP_L_A:
					case Constant.IS_ENABLED_U:
					case Constant.IS_ENABLED_U_A:
					case Constant.IS_ENABLED_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 0);
						if (fieldType == Boolean.class)
							field.set(entity, false);
						break;
					case Constant.IS_DELETED_L:
					case Constant.IS_DELETED_L_A:
					case Constant.IS_DELETED_HUMP_L_A:
					case Constant.IS_DELETED_U:
					case Constant.IS_DELETED_U_A:
					case Constant.IS_DELETED_HUMP_U_A:
						if (fieldType == Integer.class)
							field.set(entity, 1);
						if (fieldType == Boolean.class)
							field.set(entity, true);
						break;
					case Constant.UPDATOR_ID_L:
					case Constant.UPDATOR_ID_L_A:
					case Constant.UPDATOR_ID_HUMP_L_A:
					case Constant.UPDATOR_ID_U:
					case Constant.UPDATOR_ID_U_A:
					case Constant.UPDATOR_ID_HUMP_U_A:
						if (fieldType == Long.class)
							field.set(entity, Long.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == Integer.class)
							field.set(entity, Integer.valueOf(Constant.ANONYMOUS_L.equals(userId) ? "0" : userId));
						if (fieldType == String.class)
							field.set(entity, userId);
						break;
					case Constant.UPDATOR_NAME_L:
					case Constant.UPDATOR_NAME_L_A:
					case Constant.UPDATOR_NAME_HUMP_L_A:
					case Constant.UPDATOR_NAME_U:
					case Constant.UPDATOR_NAME_U_A:
					case Constant.UPDATOR_NAME_HUMP_U_A:
						if (fieldType == String.class)
							field.set(entity, userName);
						break;
					case Constant.UPDATE_TIME_L:
					case Constant.UPDATE_TIME_L_A:
					case Constant.UPDATE_TIME_HUMP_L_A:
					case Constant.UPDATE_TIME_U:
					case Constant.UPDATE_TIME_U_A:
					case Constant.UPDATE_TIME_HUMP_U_A:
						if (fieldType == LocalDateTime.class)
							field.set(entity, currentTime);
						break;
					default:
						if (params != null && params.containsKey(fieldName)) {
							field.set(entity, params.get(fieldName));
						}
						break;
				}
			}
		} catch (IllegalAccessException e) {

		}
	}
}
