package zhongjyuan.domain.utils;

import java.util.UUID;

import zhongjyuan.domain.ITool;

/**
 * @className: UUIDUtil
 * @description: 高效GUID产生算法(sequence),基于Snowflake实现64位自增ID算法。
 * @author: zhongjyuan
 * @date: 2022年7月27日 上午11:44:00
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class UUIDUtil implements ITool {

	private static final long serialVersionUID = 1L;

	public static String randomUUID(boolean rmLine, boolean upperCase) {
		String uuid = generateUUID();
		uuid = rmLine ? uuid.replaceAll("-", "") : uuid;
		uuid = upperCase ? uuid.toUpperCase() : uuid;
		return uuid;
	}

	public static String randomUUID() {
		return randomUUID(true, false);
	}

	private static String generateUUID() {
		return UUID.randomUUID().toString();
	}
}
