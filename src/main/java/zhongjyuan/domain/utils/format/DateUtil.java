package zhongjyuan.domain.utils.format;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

import zhongjyuan.domain.ITool;

/**
 * @ClassName DateUtil
 * @Description 日期处理
 * @Author zhongjyuan
 * @Date 2022年7月27日 上午10:50:11
 * @Copyright zhongjyuan.com
 */
public class DateUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields SECOND_MILLIS: 秒转换成毫秒
	 */
	public static final long SECOND_MILLIS = 1000;

	/**
	 * @Fields TIME_ZONE_DEFAULT: 默认时区
	 */
	public static final String TIME_ZONE_DEFAULT = "GMT+8";

	/**
	 * @Fields DATE_PATTERN: 时间格式(yyyy-MM-dd)
	 */
	public final static String DATE_PATTERN = "yyyy-MM-dd";

	/**
	 * @Fields DATE_TIME_PATTERN: 时间格式(yyyy-MM-dd HH:mm:ss)
	 */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/**
	 * @Title format
	 * @Author zhongjyuan
	 * @Description 格式化
	 * @Param @param date 时间对象
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String format(Date date) {
		return format(date, DATE_PATTERN);
	}

	/**
	 * @Title format
	 * @Author zhongjyuan
	 * @Description 格式化
	 * @Param @param date 时间对象
	 * @Param @param pattern 时间格式
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String format(Date date, String pattern) {
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			return dateFormat.format(date);
		}
		return null;
	}

	/**
	 * @Title max
	 * @Author zhongjyuan
	 * @Description 最大时间
	 * @Param @param date1
	 * @Param @param date2
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date max(Date date1, Date date2) {
		if (date1 == null)
			return date2;

		if (date2 == null)
			return date1;

		return date1.compareTo(date2) > 0 ? date1 : date2;
	}

	/**
	 * @Title beforeNow
	 * @Author zhongjyuan
	 * @Description 是否当前之前
	 * @Param @param date
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean beforeNow(Date date) {
		return date.getTime() < System.currentTimeMillis();
	}

	/**
	 * @Title afterNow
	 * @Author zhongjyuan
	 * @Description 是否当前之后
	 * @Param @param date
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean afterNow(Date date) {
		return date.getTime() >= System.currentTimeMillis();
	}

	/**
	 * @Title isExpired
	 * @Author zhongjyuan
	 * @Description 是否失效
	 * @Param @param date
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean isExpired(Date date) {
		return System.currentTimeMillis() > date.getTime();
	}

	/**
	 * @Title diff
	 * @Author zhongjyuan
	 * @Description 时间差值
	 * @Param @param endTime 结束时间
	 * @Param @param startTime 开始时间
	 * @Param @return
	 * @Return long
	 * @Throws
	 */
	public static long diff(Date endTime, Date startTime) {
		return endTime.getTime() - startTime.getTime();
	}

	/**
	 * @Title addTime
	 * @Author zhongjyuan
	 * @Description 增加时间
	 * @Param @param duration
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date addTime(Duration duration) {
		return new Date(System.currentTimeMillis() + duration.toMillis());
	}

	/**
	 * @Title computeDate
	 * @Author zhongjyuan
	 * @Description 计算当期时间相差的日期
	 * @Param @param field
	 *        日历字段[Calendar.MONTH,Calendar.DAY_OF_MONTH,Calendar.HOUR_OF_DAY]
	 * @Param @param amount 相差的数值
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date computeDate(int field, int amount) {
		return computeDate(null, field, amount);
	}

	/**
	 * @Title computeDate
	 * @Author zhongjyuan
	 * @Description 计算当期时间相差的日期
	 * @Param @param date 设置时间
	 * @Param @param field
	 *        日历字段[Calendar.MONTH,Calendar.DAY_OF_MONTH,Calendar.HOUR_OF_DAY]
	 * @Param @param amount 相差的数值
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date computeDate(Date date, int field, int amount) {
		if (amount == 0)
			return date;

		Calendar c = Calendar.getInstance();
		if (date != null)
			c.setTime(date);

		c.add(field, amount);
		return c.getTime();
	}

	/**
	 * @Title buildTime
	 * @Author zhongjyuan
	 * @Description 创建指定时间
	 * @Param @param year 年
	 * @Param @param mouth 月
	 * @Param @param day 日
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date buildTime(int year, int mouth, int day) {
		return buildTime(year, mouth, day, 0, 0, 0);
	}

	/**
	 * @Title buildTime
	 * @Author zhongjyuan
	 * @Description 创建指定时间
	 * @Param @param year 年
	 * @Param @param mouth 月
	 * @Param @param day 日
	 * @Param @param hour 小时
	 * @Param @param minute 分钟
	 * @Param @param second 秒
	 * @Param @return
	 * @Return Date
	 * @Throws
	 */
	public static Date buildTime(int year, int mouth, int day, int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, mouth - 1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		calendar.set(Calendar.MILLISECOND, 0); // 一般情况下，都是 0 毫秒
		return calendar.getTime();
	}
}
