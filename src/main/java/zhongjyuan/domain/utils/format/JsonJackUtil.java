package zhongjyuan.domain.utils.format;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.type.TypeReference;

/**
 * @className: JsonJackUtil
 * @description: JSON序列化与反序列化工具类
 * @author: zhongjyuan
 * @date: 2023年11月24日 下午3:08:25
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class JsonJackUtil {

	// 默认日期格式
	protected static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

	// 默认时间格式
	protected static final String DEFAULT_TIME_PATTERN = "HH:mm:ss[.SSS]";

	// 默认日期时间格式
	protected static final String DEFAULT_DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss[.SSS]";

	// 默认日期格式化器
	protected static final DateTimeFormatter DEFAULT_DATE_FORMATTER = DateTimeFormatter.ofPattern(DEFAULT_DATE_PATTERN);

	// 默认时间格式化器
	protected static final DateTimeFormatter DEFAULT_TIME_FORMATTER = DateTimeFormatter.ofPattern(DEFAULT_TIME_PATTERN);

	// 默认日期时间格式化器
	protected static final DateTimeFormatter DEFAULT_DATETIME_FORMATTER = DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_PATTERN);

	/**
	 * @className: SQLDateSerializer
	 * @description: java.sql.Date - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:45:08
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class SQLDateSerializer extends JsonSerializer<java.sql.Date> {

		@Override
		public void serialize(java.sql.Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(new SimpleDateFormat(DEFAULT_DATE_PATTERN).format(value));
		}
	}

	/**
	 * @className: SQLDateDeserializer
	 * @description: java.sql.Date - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:45:14
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class SQLDateDeserializer extends JsonDeserializer<java.sql.Date> {

		@Override
		public java.sql.Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			try {
				return java.sql.Date.valueOf(value);
			} catch (Exception e) {
				return null;
			}
		}
	}

	/**
	 * @className: SQLTimestampSerializer
	 * @description: java.sql.Timestamp - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 上午11:58:58
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class SQLTimestampSerializer extends JsonSerializer<java.sql.Timestamp> {

		@Override
		public void serialize(java.sql.Timestamp value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN).format(value));
		}
	}

	/**
	 * @className: SQLTimestampDeserializer
	 * @description: java.sql.Timestamp - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:40:22
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class SQLTimestampDeserializer extends JsonDeserializer<java.sql.Timestamp> {

		@Override
		public java.sql.Timestamp deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (value != null && !value.isEmpty()) {
				return java.sql.Timestamp.valueOf(value);
			} else {
				return null;
			}
		}
	}

	/**
	 * @className: DateSerializer
	 * @description: java.util.Date - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:42:35
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class DateSerializer extends JsonSerializer<java.util.Date> {

		@Override
		public void serialize(java.util.Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN).format(value));
		}
	}

	/**
	 * @className: DateDeserializer
	 * @description: java.util.Date - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:43:13
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class DateDeserializer extends JsonDeserializer<java.util.Date> {

		@Override
		public java.util.Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			try {
				return new SimpleDateFormat(DEFAULT_DATE_TIME_PATTERN).parse(value);
			} catch (ParseException e) {
				return null;
			}
		}
	}

	/**
	 * @className: LocalDateSerializer
	 * @description: java.time.LocalDate - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:51:18
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalDateSerializer extends JsonSerializer<java.time.LocalDate> {

		@Override
		public void serialize(java.time.LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(DEFAULT_DATE_FORMATTER));
		}
	}

	/**
	 * @className: LocalDateDeserializer
	 * @description: java.time.LocalDate - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:51:25
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalDateDeserializer extends JsonDeserializer<java.time.LocalDate> {

		@Override
		public java.time.LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			try {
				return java.time.LocalDate.parse(value, DEFAULT_DATE_FORMATTER);
			} catch (Exception e) {
				return null;
			}
		}
	}

	/**
	 * @className: LocalTimeSerializer
	 * @description: java.time.LocalTime - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:52:46
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalTimeSerializer extends JsonSerializer<java.time.LocalTime> {

		@Override
		public void serialize(java.time.LocalTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(DEFAULT_TIME_FORMATTER));
		}
	}

	/**
	 * @className: LocalTimeDeserializer
	 * @description: java.time.LocalTime - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:52:53
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalTimeDeserializer extends JsonDeserializer<java.time.LocalTime> {

		@Override
		public java.time.LocalTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			try {
				return java.time.LocalTime.parse(value, DEFAULT_TIME_FORMATTER);
			} catch (Exception e) {
				return null;
			}
		}
	}

	/**
	 * @className: LocalDateTimeSerializer
	 * @description: java.time.LocalDateTime - 序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:48:01
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalDateTimeSerializer extends JsonSerializer<java.time.LocalDateTime> {

		@Override
		public void serialize(java.time.LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
			gen.writeString(value.format(DEFAULT_DATETIME_FORMATTER));
		}
	}

	/**
	 * @className: LocalDateTimeDeserializer
	 * @description: java.time.LocalDateTime - 反序列化
	 * @author: zhongjyuan
	 * @date: 2023年11月24日 下午2:48:07
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class LocalDateTimeDeserializer extends JsonDeserializer<java.time.LocalDateTime> {

		@Override
		public java.time.LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
			String value = p.getText();
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			try {
				return java.time.LocalDateTime.parse(value, DEFAULT_DATETIME_FORMATTER);
			} catch (Exception e) {
				return null;
			}
		}
	}

	/**
	 * @title: objectMapper
	 * @author: zhongjyuan
	 * @description: 创建并返回一个ObjectMapper实例
	 * @return 返回一个配置了自定义模块的ObjectMapper实例
	 */
	public static ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();

		// 创建自定义模块
		SimpleModule module = new SimpleModule("zhongjyuan", new Version(1, 0, 0, null));

		// 注册自定义的序列化器和反序列化器
		module.addSerializer(java.sql.Date.class, new SQLDateSerializer());
		module.addDeserializer(java.sql.Date.class, new SQLDateDeserializer());

		module.addSerializer(java.sql.Timestamp.class, new SQLTimestampSerializer());
		module.addDeserializer(java.sql.Timestamp.class, new SQLTimestampDeserializer());

		module.addSerializer(java.util.Date.class, new DateSerializer());
		module.addDeserializer(java.util.Date.class, new DateDeserializer());

		module.addSerializer(java.time.LocalDate.class, new LocalDateSerializer());
		module.addDeserializer(java.time.LocalDate.class, new LocalDateDeserializer());

		module.addSerializer(java.time.LocalDateTime.class, new LocalDateTimeSerializer());
		module.addDeserializer(java.time.LocalDateTime.class, new LocalDateTimeDeserializer());

		// 将自定义模块注册到 ObjectMapper
		objectMapper.registerModule(module);

		return objectMapper;
	}

	public static <T> T readValue(String content, Class<T> valueType) {
		if (StringUtils.isEmpty(content) || valueType == null) {
			return null;
		}
		try {
			return objectMapper().readValue(content, valueType);
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T readValue(String content, TypeReference<T> valueType) {
		if (StringUtils.isEmpty(content) || valueType == null) {
			return null;
		}
		try {
			return objectMapper().readValue(content, valueType);
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T toJavaObject(String content, Class<T> valueType) {
		return readValue(content, valueType);
	}

	public static <T> T toJavaObject(String content, TypeReference<T> valueTypeRef) {
		return readValue(content, valueTypeRef);
	}

	public static String toISONString(Object object) {
		try {
			return objectMapper().writeValueAsString(object);
		} catch (Exception e) {
			return null;
		}
	}
}
