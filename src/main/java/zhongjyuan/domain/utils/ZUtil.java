package zhongjyuan.domain.utils;

import java.io.InputStreamReader;
import java.io.LineNumberReader;

import org.apache.commons.lang3.StringUtils;

import zhongjyuan.domain.ITool;

/**
 * @ClassName ZUtil
 * @Description 工具类
 * @Author zhongjyuan
 * @Date 2022年10月29日 下午12:48:59
 * @Copyright zhongjyuan.com
 */
public class ZUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @Title isWindowsOs
	 * @Author zhongjyuan
	 * @Description 是否Windows系统
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean isWindowsOs() {
		String osName = System.getProperty("os.name");
		if (StringUtils.isBlank(osName)) {
			return false;
		}
		return osName.startsWith("Windows");
	}

	/**
	 * @Title isDockerOs
	 * @Author zhongjyuan
	 * @Description 是否Docker系统
	 * @Param @return
	 * @Return boolean
	 * @Throws
	 */
	public static boolean isDockerOs() {

		try {
			Process process = Runtime.getRuntime().exec(new String[] { "sh", "-c", "printenv |grep 'wish_os_docker'" }, null, null);
			InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
			LineNumberReader lineNumberReader = new LineNumberReader(inputStreamReader);
			process.waitFor();

			String line;
			while ((line = lineNumberReader.readLine()) != null) {
				String[] arr = line.split("=");
				// System.out.println(arr[0].trim() + ":" + arr[1].trim());
				if (StringUtils.equals("docker", arr[1].trim()) || StringUtils.equals("k8s", arr[1].trim())) {
					return true;
				}
			}
		} catch (Exception x) {
			return false;
		}

		return false;
	}

	/**
	 * @Title dockerLicenseMark
	 * @Author zhongjyuan
	 * @Description Docker容器License变量
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String dockerLicenseMark() {
		String result = "WISH|WISH|WISH";

		try {

			Process process = Runtime.getRuntime().exec(new String[] { "sh", "-c", "printenv |grep 'wish_license_mark'" }, null, null);
			InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
			LineNumberReader lineNumberReader = new LineNumberReader(inputStreamReader);
			process.waitFor();

			String line;
			while ((line = lineNumberReader.readLine()) != null) {
				String arr[] = line.split("=");
				// System.out.println(arr[0].trim() + ":" + arr[1].trim());
				return arr[1];
			}
		} catch (Exception x) {
			return result;
		}

		return result;
	}
}
