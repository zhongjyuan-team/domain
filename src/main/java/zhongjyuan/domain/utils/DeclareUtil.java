package zhongjyuan.domain.utils;

import java.lang.reflect.Method;

import zhongjyuan.domain.ITool;

/**
 * @className: DeclareUtil
 * @description: 代理工具
 * @author: zhongjyuan
 * @date: 2021年12月6日 上午11:23:40
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class DeclareUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @title: getDeclaredMethodWithParent
	 * @author: zhongjyuan
	 * @description: getDeclaredMethodWithParent
	 * @param targetClass
	 * @param methodName
	 * @param parameteTypes
	 * @return {@link Method}
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Method getDeclaredMethodWithParent(Class targetClass, String methodName, Class<?>... parameteTypes) throws NoSuchMethodException {
		Boolean doneFlag = false;

		Method declaredMethod = null;
		Class curClass = targetClass;
		while (!doneFlag && curClass != Object.class) {

			try {
				declaredMethod = curClass.getDeclaredMethod(methodName, parameteTypes);
			} catch (NoSuchMethodException e) {
				doneFlag = false;
				curClass = curClass.getSuperclass();
				continue;
			}

			doneFlag = true;
		}

		if (declaredMethod == null) {
			throw new NoSuchMethodException(methodName);
		}

		return declaredMethod;
	}
}
