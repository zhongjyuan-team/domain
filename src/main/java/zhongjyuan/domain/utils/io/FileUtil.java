package zhongjyuan.domain.utils.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import zhongjyuan.domain.ITool;
import zhongjyuan.domain.file.FileTreeModel;

/**
 * @ClassName FileUtil
 * @Description 文件帮助
 * @Author zhongjyuan
 * @Date 2022年12月29日 上午10:26:30
 * @Copyright zhongjyuan.com
 */
public class FileUtil implements ITool {

	private static final long serialVersionUID = 1L;

	/**
	 * @Title getFileName
	 * @Author zhongjyuan
	 * @Description 获取文件名称
	 * @Param @param url 文件地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getFileName(String url) {
		if (StringUtils.isBlank(url))
			return "";

		String fileName = url.substring(url.lastIndexOf('/') + 1);

		return fileName.split("\\?")[0];
	}

	/**
	 * @Title getFileExtension
	 * @Author zhongjyuan
	 * @Description 获取文件拓展
	 * @Param @param url 文件地址
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getFileExtension(String url) {
		if (StringUtils.isBlank(url))
			return "";

		String fileName = handleAllFileName(url);
		if (StringUtils.isBlank(fileName))
			return "";

		return fileName.substring(fileName.lastIndexOf("."));
	}

	/**
	 * @Title getFileNameExtension
	 * @Author zhongjyuan
	 * @Description 获取文件名拓展
	 * @Param @param fileName 文件名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String getFileNameExtension(String fileName) {
		fileName = getFileName(fileName);
		if (fileName == null)
			return null;

		String fileExt = fileName.substring(fileName.lastIndexOf("."));

		return fileExt.toLowerCase();
	}

	/**
	 * @Title handleFileName
	 * @Author zhongjyuan
	 * @Description 处理文件名
	 * @Param @param fileName 文件名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String handleFileName(String fileName) {
		int unixSep = fileName.lastIndexOf('/');

		// Check for Windows-style path
		int winSep = fileName.lastIndexOf('\\');

		// Cut off at latest possible point
		int pos = (winSep > unixSep ? winSep : unixSep);
		if (pos != -1) {
			// Any sort of path separator found...
			fileName = fileName.substring(pos + 1);
		}

		return fileName.substring(0, fileName.lastIndexOf("."));
	}

	/**
	 * @Title handleAllFileName
	 * @Author zhongjyuan
	 * @Description 处理文件名
	 * @Param @param fileName 文件名
	 * @Param @return
	 * @Return String
	 * @Throws
	 */
	public static String handleAllFileName(String fileName) {
		int unixSep = fileName.lastIndexOf('/');

		// Check for Windows-style path
		int winSep = fileName.lastIndexOf('\\');

		// Cut off at latest possible point
		int pos = (winSep > unixSep ? winSep : unixSep);
		if (pos != -1) {
			// Any sort of path separator found...
			fileName = fileName.substring(pos + 1);
		}

		return fileName;
	}

	/**
	 * @Title readLines
	 * @Author zhongjyuan
	 * @Description 读取行
	 * @Param @param path 地址
	 * @Param @param ignoreComments 忽略说明(开头#)
	 * @Param @return
	 * @Param @throws IOException
	 * @Return List<String>
	 * @Throws
	 */
	public static List<String> readLines(Path path, boolean ignoreComments) throws IOException {
		File file = path.toFile();
		if (!file.isFile()) {
			return new ArrayList<String>();
		}

		List<String> lines = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = reader.readLine()) != null) {
				if (ignoreComments && !line.startsWith("#") && !lines.contains(line)) {
					lines.add(line);
				}
			}
		}

		return lines;
	}

	/**
	 * @Title writeLines
	 * @Author zhongjyuan
	 * @Description 写入行
	 * @Param @param path 地址
	 * @Param @param lines 行集合
	 * @Param @throws IOException
	 * @Return void
	 * @Throws
	 */
	public static void writeLines(Path path, Collection<String> lines) throws IOException {
		Files.write(path, lines, StandardCharsets.UTF_8);
	}

	public static long getFolderSize(File folder) {
		long size = 0;
		for (File file : folder.listFiles()) {
			if (file.isFile()) {
				size += file.length();
			} else {
				size += getFolderSize(file);
			}
		}
		return size;
	}

	public static boolean isZipFile(Path path) {
		return Files.isRegularFile(path) && path.toString().toLowerCase().endsWith(".zip");
	}

	public static boolean isJarFile(Path path) {
		return Files.isRegularFile(path) && path.toString().toLowerCase().endsWith(".jar");
	}

	/**
	 * @title: toTree
	 * @author: zhongjyuan
	 * @description: 转树形
	 * @param path 根节点路径
	 * @return
	 */
	public static FileTreeModel toTree(String path) {
		return toTree(new File(path));
	}

	/**
	 * @title: toTree
	 * @author: zhongjyuan
	 * @description: 转树形
	 * @param root 根节点文件对象
	 * @return
	 */
	public static FileTreeModel toTree(File root) {
		FileTreeModel tree = new FileTreeModel();

		if (root == null || !root.exists() || !root.isDirectory()) {
			return tree;
		}

		tree.setKey(String.valueOf(root.hashCode()));
		tree.setSize(getFolderSize(root));
		tree.setName(root.getName());
		tree.setType("folder");
		tree.setPath(root.getAbsolutePath());
		tree.setRelativePath(root.getPath());
		tree.setExtension(null);
		tree.setFolder(root.isDirectory());
		tree.setParentName(null);
		tree.setParentSize(null);
		tree.setParentPath(null);
		tree.setLastModified(root.lastModified());

		recursiveTree(root, tree);

		return tree;
	}

	/**
	 * @title: recursiveTree
	 * @author: zhongjyuan
	 * @description: 递归转树形
	 * @param parent 父级文件对象
	 * @param parentNode 父级节点对象
	 */
	private static void recursiveTree(File parent, FileTreeModel parentNode) {
		File[] childs = parent.listFiles();
		fileSort(childs);

		List<FileTreeModel> childrens = new ArrayList<FileTreeModel>();
		for (File child : childs) {
			FileTreeModel children = new FileTreeModel();

			children.setKey(String.valueOf(child.hashCode()));
			children.setName(child.getName());
			children.setPath(child.getAbsolutePath());
			children.setRelativePath(child.getPath());
			children.setFolder(child.isDirectory());
			children.setLastModified(child.lastModified());

			children.setParentName(parentNode.getName());
			children.setParentSize(parentNode.getSize());
			children.setParentPath(parentNode.getPath());

			if (child.isFile()) {
				children.setSize(child.length());
				children.setType(getFileNameExtension(child.getName()));
				children.setExtension(getFileNameExtension(child.getName()));
			} else {
				children.setSize(getFolderSize(child));
				children.setType("folder");
				children.setExtension(null);

				recursiveTree(child, children);
			}
		}
		parentNode.setChildrens(childrens);
	}

	/**
	 * @title: fileSort
	 * @author: zhongjyuan
	 * @description: 文件排序
	 * @param files 文件对象数组
	 */
	private static void fileSort(File[] files) {
		Comparator<File> comparator = new Comparator<File>() {

			@Override
			public int compare(File f1, File f2) {
				if (f1.isDirectory() && f2.isFile()) {
					return -1; // 目录排在前面
				} else if (f1.isFile() && f2.isDirectory()) {
					return 1; // 文件排在后面
				} else if (f1.getName().length() != f2.getName().length()) {
					return f1.getName().length() > f2.getName().length() ? 1 : -1; // 根据文件名长度排序
				} else {
					return 0 - f1.getName().compareToIgnoreCase(f2.getName()); // 根据文件名的字典序排序
				}
			}
		};
		Arrays.sort(files, comparator);
	}
}
