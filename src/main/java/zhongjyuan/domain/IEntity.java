package zhongjyuan.domain;

/**
 * @className: IEntity
 * @description: 实体对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月20日 上午11:54:33
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IEntity extends zhongjyuan {

}
