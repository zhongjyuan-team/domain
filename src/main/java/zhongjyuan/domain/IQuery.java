package zhongjyuan.domain;

/**
 * @className: IQuery
 * @description: 条件对象的基础接口，定义了通用的属性和行为。
 * @author: zhongjyuan
 * @date: 2023年11月9日 下午2:50:14
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IQuery extends zhongjyuan {

}
