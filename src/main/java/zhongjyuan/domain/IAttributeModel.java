package zhongjyuan.domain;

import java.util.Map;

/**
 * @className: IAttributeModel
 * @description: 属性模型对象的基础接口，定义了通用的属性和行为。继承{@link IModel}
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午12:27:10
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface IAttributeModel extends IModel {

	/**
	 * 清除所有属性值。
	 */
	public void clear();

	/**
	 * 获取指定键对应的属性值。
	 * @param key 属性键
	 * @return 对应的属性值
	 */
	public Object get(Object key);

	/**
	 * 移除指定键及其对应的属性值。
	 * @param key 要移除的属性键
	 * @return 被移除的属性值
	 */
	public Object remove(Object key);

	/**
	 * 设置指定键对应的属性值。
	 * @param key 属性键
	 * @param value 属性值
	 * @return 之前与 key 关联的值，如果之前不存在，则返回 null
	 */
	public Object put(Object key, Object value);

	/**
	 * 将指定 Map 中的所有键值对设置到属性模型中。
	 * @param values 包含属性键值对的 Map
	 */
	public void putAll(Map<Object, Object> values);
}
