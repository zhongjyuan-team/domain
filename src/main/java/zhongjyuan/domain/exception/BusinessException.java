package zhongjyuan.domain.exception;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;

import zhongjyuan.domain.Constant;
import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.response.ResponseResult;

/**
 * @className: BusinessException
 * @description: 业务异常对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午3:13:55
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields responseResult: 响应结果对象
	 */
	protected ResponseResult<?> responseResult;

	/**
	 * @fields exceptionType: 异常类型
	 */
	protected String exceptionType = Constant.FRAMEWORK_NAME;

	/**
	 * @fields dateFormat: 时间格式化
	 */
	protected DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 */
	public BusinessException() {
		this.responseResult = ResponseResult.error();
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 */
	public BusinessException(Throwable throwable) {
		super(throwable);
		this.responseResult = ResponseResult.error();
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param message 异常消息
	 */
	public BusinessException(String message) {
		this.responseResult = ResponseResult.error(message);
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 */
	public BusinessException(Throwable throwable, String message) {
		super(message, throwable);
		this.responseResult = ResponseResult.error(message);
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param message 异常消息
	 * @return {@link BusinessException}
	 */
	public BusinessException build(String message) {
		this.responseResult = ResponseResult.error(message);
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public BusinessException(String message, Object... args) {
		this.responseResult = ResponseResult.error(MessageFormat.format(message, args));
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public BusinessException(Throwable throwable, String message, Object... args) {
		super(MessageFormat.format(message, args), throwable);
		this.responseResult = ResponseResult.error(MessageFormat.format(message, args));
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param message 异常消息
	 * @param args 消息占位值
	 * @return {@link BusinessException}
	 */
	public BusinessException build(String message, Object... args) {
		this.responseResult = ResponseResult.error(MessageFormat.format(message, args));
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param code 异常码
	 * @param message 异常消息
	 */
	public BusinessException(Long code, String message) {
		this.responseResult = ResponseResult.error(code, message);
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param code 异常码
	 * @param message 异常消息
	 */
	public BusinessException(Throwable throwable, Long code, String message) {
		super(message, throwable);
		this.responseResult = ResponseResult.error(code, message);
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param code 异常码
	 * @param message 异常消息
	 * @return {@link BusinessException}
	 */
	public BusinessException build(Long code, String message) {
		this.responseResult = ResponseResult.error(code, message);
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public BusinessException(IResponseCode code) {
		this.responseResult = ResponseResult.error(code.getCode(), code.getMessage());
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public BusinessException(Throwable throwable, IResponseCode code) {
		super(code.getMessage(), throwable);
		this.responseResult = ResponseResult.error(code.getCode(), code.getMessage());
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @return {@link BusinessException}
	 */
	public BusinessException build(IResponseCode code) {
		this.responseResult = ResponseResult.error(code.getCode(), code.getMessage());
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 异常消息
	 */
	public BusinessException(IResponseCode code, String message) {
		this.responseResult = ResponseResult.error(code.getCode(), message);
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 异常消息
	 */
	public BusinessException(Throwable throwable, IResponseCode code, String message) {
		super(message, throwable);
		this.responseResult = ResponseResult.error(code.getCode(), message);
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 异常消息
	 * @return {@link BusinessException}
	 */
	public BusinessException build(IResponseCode code, String message) {
		this.responseResult = ResponseResult.error(code.getCode(), message);
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public BusinessException(IResponseCode code, Object... args) {
		this.responseResult = ResponseResult.error(code.getCode(), MessageFormat.format(code.getMessage(), args));
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public BusinessException(Throwable throwable, IResponseCode code, Object... args) {
		super(MessageFormat.format(code.getMessage(), args), throwable);
		this.responseResult = ResponseResult.error(code.getCode(), MessageFormat.format(code.getMessage(), args));
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 * @return {@link BusinessException}
	 */
	public BusinessException build(IResponseCode code, Object... args) {
		this.responseResult = ResponseResult.error(code.getCode(), MessageFormat.format(code.getMessage(), args));
		return this;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public BusinessException(ResponseResult<?> responseResult) {
		this.responseResult = responseResult;
	}

	/**
	 * @title: BusinessException
	 * @author: zhongjyuan
	 * @description: 业务异常对象
	 * @param throwable 异常抛出对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public BusinessException(Throwable throwable, ResponseResult<?> responseResult) {
		super(responseResult.getMessage(), throwable);
		this.responseResult = responseResult;
	}

	/**
	 * @title: build
	 * @author: zhongjyuan
	 * @description: 构造
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 * @return {@link BusinessException}
	 */
	public BusinessException build(ResponseResult<?> responseResult) {
		this.responseResult = responseResult;
		return this;
	}

	/**
	 * @title: getExceptionType
	 * @author: zhongjyuan
	 * @description: 获取异常类型
	 * @return 异常类型
	 */
	public String getExceptionType() {
		return exceptionType;
	}

	/**
	 * @title: setExceptionType
	 * @author: zhongjyuan
	 * @description: 设置异常类型
	 * @param type 异常类型
	 */
	public void setExceptionType(String type) {
		this.exceptionType = type;
	}

	/**
	 * @title: getResponseResult
	 * @author: zhongjyuan
	 * @description: 获取响应结果对象
	 * @return 响应结果对象
	 */
	public ResponseResult<?> getResponseResult() {
		return responseResult;
	}

	/**
	 * @title: toString
	 * @author: zhongjyuan
	 * @description: toString
	 * @return
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		String result = dateFormat.format(new Date()).concat(" [" + exceptionType + "] ERROR: [").concat(responseResult.toString()).concat("]");
		if (this.getCause() != null) {
			result.concat("\n");
			result.concat("Exception: ").concat(ExceptionUtils.getStackTrace(getCause()));
		}
		return result;
	}
}
