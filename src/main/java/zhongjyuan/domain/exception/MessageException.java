package zhongjyuan.domain.exception;

import java.text.MessageFormat;

import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.response.ResponseResult;

/**
 * @className: MessageException
 * @description: 消息异常对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午10:44:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class MessageException extends BusinessException {

	private static final long serialVersionUID = 1L;

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 */
	public MessageException() {
		super();
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象 
	 * @param throwable 异常抛出对象
	 */
	public MessageException(Throwable throwable) {
		super(throwable);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param message 异常消息
	 */
	public MessageException(String message) {
		super(message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 */
	public MessageException(Throwable throwable, String message) {
		super(throwable, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public MessageException(String message, Object... args) {
		super(message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public MessageException(Throwable throwable, String message, Object... args) {
		super(throwable, message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param code 编码
	 * @param message 消息
	 */
	public MessageException(String code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码
	 * @param message 消息
	 */
	public MessageException(Throwable throwable, String code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @Title MessageException
	 * @Author zhongjyuan
	 * @Description 消息异常对象
	 * @Param @param code 响应接口对象
	 * @Throws
	 */
	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public MessageException(IResponseCode code) {
		super(code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public MessageException(Throwable throwable, IResponseCode code) {
		super(throwable, code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public MessageException(IResponseCode code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public MessageException(Throwable throwable, IResponseCode code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public MessageException(IResponseCode code, Object... args) {
		super(code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public MessageException(Throwable throwable, IResponseCode code, Object... args) {
		super(throwable, code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public MessageException(ResponseResult<?> responseResult) {
		super(responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: MessageException
	 * @author: zhongjyuan
	 * @description: 消息异常对象
	 * @param throwable 异常抛出对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public MessageException(Throwable throwable, ResponseResult<?> responseResult) {
		super(throwable, responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}
}