package zhongjyuan.domain.exception;

import java.text.MessageFormat;

import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.response.ResponseResult;

/**
 * @className: ToolException
 * @description: 工具异常对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午10:44:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ToolException extends BusinessException {

	private static final long serialVersionUID = 1L;

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 */
	public ToolException() {
		super();
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象 
	 * @param throwable 异常抛出对象
	 */
	public ToolException(Throwable throwable) {
		super(throwable);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param message 异常消息
	 */
	public ToolException(String message) {
		super(message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 */
	public ToolException(Throwable throwable, String message) {
		super(throwable, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public ToolException(String message, Object... args) {
		super(message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public ToolException(Throwable throwable, String message, Object... args) {
		super(throwable, message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param code 编码
	 * @param message 消息
	 */
	public ToolException(String code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码
	 * @param message 消息
	 */
	public ToolException(Throwable throwable, String code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @Title ToolException
	 * @Author zhongjyuan
	 * @Description 工具异常对象
	 * @Param @param code 响应接口对象
	 * @Throws
	 */
	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public ToolException(IResponseCode code) {
		super(code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public ToolException(Throwable throwable, IResponseCode code) {
		super(throwable, code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public ToolException(IResponseCode code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public ToolException(Throwable throwable, IResponseCode code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public ToolException(IResponseCode code, Object... args) {
		super(code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public ToolException(Throwable throwable, IResponseCode code, Object... args) {
		super(throwable, code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public ToolException(ResponseResult<?> responseResult) {
		super(responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: ToolException
	 * @author: zhongjyuan
	 * @description: 工具异常对象
	 * @param throwable 异常抛出对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public ToolException(Throwable throwable, ResponseResult<?> responseResult) {
		super(throwable, responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}
}