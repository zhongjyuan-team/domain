package zhongjyuan.domain.exception;

import java.text.MessageFormat;

import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.response.ResponseResult;

/**
 * @className: FileException
 * @description: 文件异常对象
 * @author: zhongjyuan
 * @date: 2022年7月14日 上午10:44:52
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class FileException extends BusinessException {

	private static final long serialVersionUID = 1L;

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 */
	public FileException() {
		super();
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象 
	 * @param throwable 异常抛出对象
	 */
	public FileException(Throwable throwable) {
		super(throwable);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR);
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param message 异常消息
	 */
	public FileException(String message) {
		super(message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 */
	public FileException(Throwable throwable, String message) {
		super(throwable, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), message);
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public FileException(String message, Object... args) {
		super(message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param message 异常消息
	 * @param args 消息占位值
	 */
	public FileException(Throwable throwable, String message, Object... args) {
		super(throwable, message, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
		this.responseResult = ResponseResult.error(ResponseCode.PARAM_ERROR.getCode(), MessageFormat.format(message, args));
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param code 编码
	 * @param message 消息
	 */
	public FileException(String code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码
	 * @param message 消息
	 */
	public FileException(Throwable throwable, String code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @Title FileException
	 * @Author zhongjyuan
	 * @Description 文件异常对象
	 * @Param @param code 响应接口对象
	 * @Throws
	 */
	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public FileException(IResponseCode code) {
		super(code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 */
	public FileException(Throwable throwable, IResponseCode code) {
		super(throwable, code);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public FileException(IResponseCode code, String message) {
		super(code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param message 消息
	 */
	public FileException(Throwable throwable, IResponseCode code, String message) {
		super(throwable, code, message);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public FileException(IResponseCode code, Object... args) {
		super(code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param code 编码接口对象 {@link IResponseCode}
	 * @param args 消息占位值
	 */
	public FileException(Throwable throwable, IResponseCode code, Object... args) {
		super(throwable, code, args);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public FileException(ResponseResult<?> responseResult) {
		super(responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}

	/**
	 * @title: FileException
	 * @author: zhongjyuan
	 * @description: 文件异常对象
	 * @param throwable 异常抛出对象
	 * @param responseResult 响应结果对象 {@link ResponseResult}
	 */
	public FileException(Throwable throwable, ResponseResult<?> responseResult) {
		super(throwable, responseResult);
		this.exceptionType += " - " + this.getClass().getSimpleName();
	}
}