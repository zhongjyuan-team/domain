package zhongjyuan.domain.query;

import java.text.MessageFormat;

import zhongjyuan.domain.AbstractQuery;
import zhongjyuan.domain.IQuery;

/**
 * @className: SearchQuery
 * @description: 查询条件对象. 拓展{@link AbstractQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:29:03
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class SearchQuery extends AbstractQuery implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields searchValue: 查询值
	 */
	protected String searchValue;

	/**
	 * 构造函数
	 */
	public SearchQuery() {

	}

    /**
     * 构造函数
     *
     * @param searchValue 查询值
     */
	public SearchQuery(String searchValue) {
		this.searchValue = searchValue;
	}

	/**
	 * 获取查询值
	 * 
	 * @return 查询值
	 */
	public String getSearchValue() {
		return this.searchValue;
	}

	/**
	 * 设置查询值
	 * @param 查询值
	 */
	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	@Override
	public String toString() {
		return MessageFormat.format("SearchQuery[searchValue: {0}]", this.searchValue);
	}
}
