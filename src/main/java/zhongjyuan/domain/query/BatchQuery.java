package zhongjyuan.domain.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;

import zhongjyuan.domain.AbstractQuery;
import zhongjyuan.domain.IQuery;

/**
 * @className: BatchQuery
 * @description: 批量条件对象. 拓展{@link AbstractQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午5:12:55
 * @param <T> 批量查询的数据类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class BatchQuery<T> extends AbstractQuery implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * 待处理的数据集合
	 */
	protected Collection<T> assemble;

	/**
	 * 构造方法
	 */
	public BatchQuery() {
		this.assemble = new ArrayList<T>();
	}

	/**
	 * 构造方法
	 * 
	 * @param assemble 待处理的数据集合
	 */
	public BatchQuery(Collection<T> assemble) {
		this.assemble = assemble;
	}

	/**
	 * 获取待处理的数据集合
	 * 
	 * @return 数据集合
	 */
	public Collection<T> getAssemble() {
		return this.assemble;
	}

	/**
	 * 设置待处理的数据集合
	 * 
	 * @param assemble 数据集合
	 */
	public void setAssemble(Collection<T> assemble) {
		this.assemble = assemble;
	}

	public void put(T value) {
		this.assemble.add(value);
	}

	@Override
	public String toString() {
		return MessageFormat.format("BatchQuery[assemble: {0}]", this.assemble.toString());
	}
}
