package zhongjyuan.domain.query;

import java.text.MessageFormat;

import zhongjyuan.domain.IQuery;

/**
 * @className: SortQuery
 * @description: 排序查询对象. 拓展{@link SearchQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:31:47
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class SortQuery extends SearchQuery implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields sortName: 排序名称
	 */
	protected String sortName;

	/**
	 * @fields sortType: 排序类型
	 */
	protected String sortType;

	/**
	 * 构造函数
	 */
	public SortQuery() {

	}

    /**
     * 构造函数
     *
     * @param searchQuery 查询条件对象
     */
	public SortQuery(SearchQuery searchQuery) {
		this.searchValue = searchQuery.searchValue;
	}

    /**
     * 构造函数
     *
     * @param sortName 排序名称
     */
	public SortQuery(String sortName) {
		this.sortName = sortName;
	}

    /**
     * 构造函数
     *
     * @param sortName 排序名称
     * @param sortType 排序类型
     */
	public SortQuery(String sortName, String sortType) {
		this.sortName = sortName;
		this.sortType = sortType;
	}

    /**
     * 构造函数
     *
     * @param sortName 排序名称
     * @param sortType 排序类型
     * @param searchValue 查询值
     */
	public SortQuery(String sortName, String sortType, String searchValue) {
		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

    /**
     * 获取排序名称
     *
     * @return 排序名称
     */
    public String getSortName() {
        return this.sortName;
    }

    /**
     * 设置排序名称
     *
     * @param sortName 排序名称
     */
    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    /**
     * 获取排序类型
     *
     * @return 排序类型
     */
    public String getSortType() {
        return this.sortType;
    }

    /**
     * 设置排序类型
     *
     * @param sortType 排序类型
     */
    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

	@Override
	public String toString() {
		return MessageFormat.format("SortQuery[sortName: {0} | sortType: {1} | searchValue: {2}]", this.sortName, this.sortType, this.searchValue);
	}
}
