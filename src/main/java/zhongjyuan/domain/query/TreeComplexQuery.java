package zhongjyuan.domain.query;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import zhongjyuan.domain.IQuery;

/**
 * @className: TreeComplexQuery
 * @description: 复杂树形查询类，拓展{@link TreeLazyQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午5:10:50
 * @param <key>
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TreeComplexQuery<key> extends TreeLazyQuery<key> implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * 是否显示命中节点
	 */
	protected Boolean showHitNodes;

	/**
	 * 指定命中节点主键集合
	 */
	protected List<key> hitNodeIds;

	/**
	 * 指定命中节点主键字符串
	 */
	protected String hitNodeIdString;

	/**
	 * 构造方法 
	 */
	public TreeComplexQuery() {

	}

	/**
	 * 构造方法
	 * 
	 * @param treeLazyQuery TreeLazyQuery对象
	 */
	public TreeComplexQuery(TreeLazyQuery<key> treeLazyQuery) {
		this.rootId = treeLazyQuery.rootId;
		this.showLevel = treeLazyQuery.showLevel;
		this.showSiblings = treeLazyQuery.showSiblings;

		this.sortName = treeLazyQuery.sortName;
		this.sortType = treeLazyQuery.sortType;

		this.searchValue = treeLazyQuery.searchValue;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 */
	public TreeComplexQuery(key rootId) {
		this.rootId = rootId;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 */
	public TreeComplexQuery(key rootId, Long showLevel) {
		this.rootId = rootId;
		this.showLevel = showLevel;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIds 命中节点主键集合
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, List<key> hitNodeIds) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIds = hitNodeIds;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIds 命中节点主键集合
	 * @param sortName 排序字段名
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, List<key> hitNodeIds, String sortName) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIds = hitNodeIds;

		this.sortName = sortName;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIds 命中节点主键集合
	 * @param sortName 排序字段名
	 * @param sortType 排序类型
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, List<key> hitNodeIds, String sortName, String sortType) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIds = hitNodeIds;

		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIds 命中节点主键集合
	 * @param sortName 排序字段名
	 * @param sortType 排序类型
	 * @param searchValue 搜索值
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, List<key> hitNodeIds, String sortName, String sortType, String searchValue) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIds = hitNodeIds;

		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIdString 命中节点主键字符串
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, String hitNodeIdString) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIdString = hitNodeIdString;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIdString 命中节点主键字符串
	 * @param sortName 排序字段名
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, String hitNodeIdString, String sortName) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIdString = hitNodeIdString;

		this.sortName = sortName;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIdString 命中节点主键字符串
	 * @param sortName 排序字段名
	 * @param sortType 排序类型
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, String hitNodeIdString, String sortName, String sortType) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIdString = hitNodeIdString;

		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * 构造方法
	 * 
	 * @param rootId 根节点ID
	 * @param showLevel 显示层级
	 * @param showSiblings 是否显示兄弟节点
	 * @param showHitNodes 是否显示命中节点
	 * @param hitNodeIdString 命中节点主键字符串
	 * @param sortName 排序字段名
	 * @param sortType 排序类型
	 * @param searchValue 搜索值
	 */
	public TreeComplexQuery(key rootId, Long showLevel, Boolean showSiblings, Boolean showHitNodes, String hitNodeIdString, String sortName, String sortType, String searchValue) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
		this.showHitNodes = showHitNodes;
		this.hitNodeIdString = hitNodeIdString;

		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

	/**
	 * 获取是否显示命中节点
	 * 
	 * @return 是否显示命中节点
	 */
	public Boolean getShowHitNodes() {
		return this.showHitNodes;
	}

	/**
	 * 设置是否显示命中节点
	 * 
	 * @param showHitNodes 是否显示命中节点
	 */
	public void setShowHitNodes(Boolean showHitNodes) {
		this.showHitNodes = showHitNodes;
	}

	/**
	 * 获取命中节点主键集合
	 * 
	 * @return 命中节点主键集合
	 */
	public List<key> getHitNodeIds() {
		return this.hitNodeIds;
	}

	/**
	 * 设置命中节点主键集合
	 * 
	 * @param hitNodeIds 命中节点主键集合
	 */
	public void setHitNodeIds(List<key> hitNodeIds) {
		this.hitNodeIds = hitNodeIds;
	}

	/**
	 * 获取命中节点主键字符串
	 * 
	 * @return 命中节点主键字符串
	 */
	public String getHitNodeIdString() {
		return this.hitNodeIdString;
	}

	/**
	 * 设置命中节点主键字符串
	 * 
	 * @param hitNodeIdString 命中节点主键字符串
	 */
	public void setHitNodeIdString(String hitNodeIdString) {
		this.hitNodeIdString = hitNodeIdString;
	}

	/**
	 * 设置命中节点主键字符串
	 * 
	 * @param hitNodeIds 命中节点主键集合
	 */
	public void setHitNodeIdString(List<key> hitNodeIds) {
		this.hitNodeIdString = String.join(",", hitNodeIds.stream().map(hitNodeId -> hitNodeId.toString()).collect(Collectors.toList()));
	}

	@Override
	public String toString() {
		return MessageFormat.format("TreeComplexQuery[rootId: {0} | showLevel: {1} | showSiblings: {2} | sortName: {3} | | sortType: {4} | searchValue: {5}]", this.rootId, this.showLevel, this.showSiblings, this.sortName, this.sortType, this.searchValue);
	}
}
