package zhongjyuan.domain.query;

import java.text.MessageFormat;

import zhongjyuan.domain.IQuery;

/**
 * @className: TreeLazyQuery
 * @description: 懒加载树形查询类，拓展{@link TreeQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:57:03
 * @param <key> 主键类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TreeLazyQuery<key> extends TreeQuery<key> implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * 是否显示兄弟节点
	 */
	protected Boolean showSiblings;

	/**
	 * 构造函数
	 */
	public TreeLazyQuery() {

	}

	/**
	 * 构造函数
	 *
	 * @param treeQuery 树形查询类
	 */
	public TreeLazyQuery(TreeQuery<key> treeQuery) {
		this.rootId = treeQuery.rootId;
		this.showLevel = treeQuery.showLevel;

		this.sortName = treeQuery.sortName;
		this.sortType = treeQuery.sortType;

		this.searchValue = treeQuery.searchValue;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 */
	public TreeLazyQuery(key rootId) {
		this.rootId = rootId;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 */
	public TreeLazyQuery(key rootId, Long showLevel) {
		this.rootId = rootId;
		this.showLevel = showLevel;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param showSiblings 是否显示兄弟节点
	 */
	public TreeLazyQuery(key rootId, Long showLevel, Boolean showSiblings) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param showSiblings 是否显示兄弟节点
	 * @param sortName 排序名称
	 */
	public TreeLazyQuery(key rootId, Long showLevel, Boolean showSiblings, String sortName) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;

		this.sortName = sortName;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param showSiblings 是否显示兄弟节点
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 */
	public TreeLazyQuery(key rootId, Long showLevel, Boolean showSiblings, String sortName, String sortType) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;

		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param showSiblings 是否显示兄弟节点
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 * @param searchValue 查询值
	 */
	public TreeLazyQuery(key rootId, Long showLevel, Boolean showSiblings, String sortName, String sortType, String searchValue) {
		this.rootId = rootId;
		this.showLevel = showLevel;
		this.showSiblings = showSiblings;

		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

	/**
	 * 获取是否显示兄弟节点
	 *
	 * @return 是否显示兄弟节点
	 */
	public Boolean getShowSiblings() {
		return this.showSiblings;
	}

	/**
	 * 设置是否显示兄弟节点
	 *
	 * @param showSiblings 是否显示兄弟节点
	 */
	public void setShowSiblings(Boolean showSiblings) {
		this.showSiblings = showSiblings;
	}

	@Override
	public String toString() {
		return MessageFormat.format("TreeLazyQuery[rootId: {0} | showLevel: {1} | showSiblings: {2} | sortName: {3} | | sortType: {4} | searchValue: {5}]", this.rootId, this.showLevel, this.showSiblings, this.sortName, this.sortType, this.searchValue);
	}
}
