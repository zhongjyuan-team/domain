package zhongjyuan.domain.query;

import java.text.MessageFormat;

import zhongjyuan.domain.IQuery;

/**
 * @className: PageQuery
 * @description: 分页查询对象. 拓展{@link SortQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:46:45
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PageQuery extends SortQuery implements IQuery {

	private static final long serialVersionUID = 1L;

	// 分页索引，默认为1
	protected Long index = 1L;

	// 分页大小，默认为20
	protected Long size = 20L;

	/**
	 * 构造函数
	 */
	public PageQuery() {

	}

	/**
	 * 构造函数
	 *
	 * @param SortQuery 排序条件对象
	 */
	public PageQuery(SortQuery sortQuery) {

		this.sortName = sortQuery.sortName;
		this.sortType = sortQuery.sortType;

		this.searchValue = sortQuery.searchValue;
	}

	/**
	 * 构造函数
	 *
	 * @param index 分页索引
	 */
	public PageQuery(Long index) {
		this.index = index;
	}

	/**
	 * 构造函数
	 *
	 * @param index 分页索引
	 * @param size  分页大小
	 */
	public PageQuery(Long index, Long size) {
		this.index = index;
		this.size = size;
	}

	/**
	 * 构造函数
	 *
	 * @param index 分页索引
	 * @param size  分页大小
	 * @param sortName 排序名称
	 */
	public PageQuery(Long index, Long size, String sortName) {
		this.index = index;
		this.size = size;

		this.sortName = sortName;
	}

	/**
	 * 构造函数
	 *
	 * @param index 分页索引
	 * @param size  分页大小
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 */
	public PageQuery(Long index, Long size, String sortName, String sortType) {
		this.index = index;
		this.size = size;

		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * 构造函数
	 *
	 * @param index 分页索引
	 * @param size  分页大小
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 * @param searchValue 查询值
	 */
	public PageQuery(Long index, Long size, String sortName, String sortType, String searchValue) {
		this.index = index;
		this.size = size;

		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

	/**
	 * 获取分页索引
	 * 
	 * @return 分页索引
	 */
	public Long getPageIndex() {
		return this.index;
	}

	/**
	 * 设置分页索引
	 * 
	 * @param index 分页索引
	 */
	public void setPageIndex(Long index) {
		this.index = index;
	}

	/**
	 * 获取分页大小
	 * 
	 * @return 分页大小
	 */
	public Long getPageSize() {
		return this.size;
	}

	/**
	 * 设置分页大小
	 * 
	 * @param size 分页大小
	 */
	public void setPageSize(Long size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return MessageFormat.format("PageQuery[index: {0} | size: {1} | sortName: {2} | sortType: {3} | searchValue: {4}]", this.index, this.size, this.sortName, this.sortType, this.searchValue);
	}
}
