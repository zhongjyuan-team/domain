package zhongjyuan.domain.query;

import java.text.MessageFormat;

import zhongjyuan.domain.IQuery;

/**
 * @className: TreeQuery
 * @description: 树形查询类，拓展{@link SortQuery} 类, 实现{@link IQuery}接口.
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:50:47
 * @param <key> 主键类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TreeQuery<key> extends SortQuery implements IQuery {

	private static final long serialVersionUID = 1L;

	/**
	 * 指定节点主键
	 */
	protected key rootId;

	/**
	 * 指定展示层级数量
	 */
	protected Long showLevel;

	/**
	 * 构造函数
	 */
	public TreeQuery() {

	}

	/**
	 * 构造函数
	 *
	 * @param SortQuery 排序条件对象
	 */
	public TreeQuery(SortQuery sortQuery) {
		this.sortName = sortQuery.sortName;
		this.sortType = sortQuery.sortType;

		this.searchValue = sortQuery.searchValue;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 */
	public TreeQuery(key rootId) {
		this.rootId = rootId;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 */
	public TreeQuery(key rootId, Long showLevel) {
		this.rootId = rootId;
		this.showLevel = showLevel;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param sortName 排序名称
	 */
	public TreeQuery(key rootId, Long showLevel, String sortName) {
		this.rootId = rootId;
		this.showLevel = showLevel;

		this.sortName = sortName;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 */
	public TreeQuery(key rootId, Long showLevel, String sortName, String sortType) {
		this.rootId = rootId;
		this.showLevel = showLevel;

		this.sortName = sortName;
		this.sortType = sortType;
	}

	/**
	 * 构造函数
	 *
	 * @param rootId 指定节点主键
	 * @param showLevel 指定展示层级数量
	 * @param sortName 排序名称
	 * @param sortType 排序类型
	 * @param searchValue 查询值
	 */
	public TreeQuery(key rootId, Long showLevel, String sortName, String sortType, String searchValue) {
		this.rootId = rootId;
		this.showLevel = showLevel;

		this.sortName = sortName;
		this.sortType = sortType;

		this.searchValue = searchValue;
	}

	/**
	 * 获取根节点主键
	 *
	 * @return 根节点主键
	 */
	public key getRootId() {
		return this.rootId;
	}

	/**
	 * 设置根节点主键
	 *
	 * @param rootId 根节点主键
	 */
	public void setRootId(key rootId) {
		this.rootId = rootId;
	}

	/**
	 * 获取展示层级数量
	 *
	 * @return 展示层级数量
	 */
	public Long getShowLevel() {
		return this.showLevel;
	}

	/**
	 * 设置展示层级数量
	 *
	 * @param showLevel 展示层级数量
	 */
	public void setShowLevel(Long showLevel) {
		// 添加输入验证，确保showLevel大于等于0
		if (showLevel >= 0) {
			this.showLevel = showLevel;
		} else {
			throw new IllegalArgumentException("showLevel must be greater than or equal to 0");
		}
	}

	@Override
	public String toString() {
		return MessageFormat.format("TreeQuery[rootId: {0} | showLevel: {1} | sortName: {2} | | sortType: {3} | searchValue: {4}]", this.rootId, this.showLevel, this.sortName, this.sortType, this.searchValue);
	}
}
