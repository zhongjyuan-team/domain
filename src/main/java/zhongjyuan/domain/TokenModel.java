package zhongjyuan.domain;

import java.time.LocalDateTime;

/**
 * @className: TokenModel
 * @description: 令牌模型的类，扩展了 {@link AbstractAttributeModel} 类并实现了 {@link IAttributeModel} 接口。
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午2:02:39
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class TokenModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	private String token;

	private String ticket;

	private String refreshToken;

	private LocalDateTime expiration;

	/**
	 * 获取访问令牌
	 *
	 * @return 访问令牌
	 */
	public String getToken() {
		return token;
	}

	/**
	 * 设置访问令牌
	 *
	 * @param token 访问令牌
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * 获取票据
	 *
	 * @return 票据
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * 设置票据
	 *
	 * @param ticket 票据
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * 获取刷新令牌
	 *
	 * @return 刷新令牌
	 */
	public String getRefreshToken() {
		return refreshToken;
	}

	/**
	 * 设置刷新令牌
	 *
	 * @param refreshToken 刷新令牌
	 */
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	/**
	 * 获取过期时间
	 *
	 * @return 过期时间
	 */
	public LocalDateTime getExpiration() {
		return expiration;
	}

	/**
	 * 设置过期时间
	 *
	 * @param expiration 过期时间
	 */
	public void setExpiration(LocalDateTime expiration) {
		this.expiration = expiration;
	}
}
