package zhongjyuan.domain.relate;

import zhongjyuan.domain.IEnum;

/**
 * @className: RelateType
 * @description: 映射类型
 * @author: zhongjyuan
 * @date: 2022年3月23日 上午10:00:18
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum RelateType implements IEnum {

	/**
	 * @fields USER: 用户
	 */
	USER("user", "用户"),

	/**
	 * @fields EMPLOYEE: 员工
	 */
	EMPLOYEE("employee", "员工"),

	/**
	 * @fields STRUCTURE: 架构
	 */
	STRUCTURE("structure", "架构"),

	/**
	 * @fields POSITION_TYPE: 职
	 */
	POSITION_TYPE("positionType", "职类"),

	/**
	 * @fields POSITION_RANK: 职
	 */
	POSITION_RANK("positionRank", "职级"),

	/**
	 * @fields POSITION: 职位
	 */
	POSITION("position", "职位"),

	/**
	 * @fields STATION: 岗位
	 */
	STATION("station", "岗位"),

	/**
	 * @fields ROLE: 角色
	 */
	ROLE("role", "角色"),

	/**
	 * @fields ROLE_RANK: 角色等级
	 */
	ROLE_RANK("roleRank", "角色等级"),

	/**
	 * @fields MESSAGE: 消息
	 */
	MESSAGE("message", "消息"),

	/**
	 * @fields PROJECT: 项目
	 */
	PROJECT("project", "项目"),

	/**
	 * @fields STAGE: 分期
	 */
	STAGE("stage", "分期"),

	/**
	 * @fields LAND: 地
	 */
	LAND("land", "地块"),

	/**
	 * @fields BUILDING: 楼
	 */
	BUILDING("building", "楼栋"),

	/**
	 * @fields ROOM: 房间
	 */
	ROOM("room", "房间"),

	/**
	 * @fields PLAN: 计划
	 */
	PLAN("plan", "计划"),

	/**
	 * @fields SUBJECT: 科目
	 */
	SUBJECT("subject", "科目"),

	/**
	 * @fields CONTRACT: 合同
	 */
	CONTRACT("contract", "合同"),

	/**
	 * @fields SUPPLIER: 供应商
	 */
	SUPPLIER("supplier", "供应商"),

	/**
	 * @fields FLOW: 流程
	 */
	FLOW("flow", "流程"),

	/**
	 * @fields FORM: 表
	 */
	FORM("form", "表单"),

	/**
	 * @fields EVENT: 事件
	 */
	EVENT("event", "事件"),

	;

	/**
	 * @fields code: 编码
	 */
	private String code;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @title: RelateType
	 * @author: zhongjyuan
	 * @description: 映射类型
	 * @param code 编码
	 * @param name 名称
	 * @throws
	 */
	RelateType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}
}
