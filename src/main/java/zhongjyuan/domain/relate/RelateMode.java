package zhongjyuan.domain.relate;

import zhongjyuan.domain.IEnum;

/**
 * @className: RelateMode
 * @description: 映射模式
 * @author: zhongjyuan
 * @date: 2022年12月9日 上午11:31:59
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public enum RelateMode implements IEnum {

	/**
	 * @fields INPUT: 同步
	 */
	INPUT("IN", "同步"),

	/**
	 * @fields OUTPUT: 推送
	 */
	OUTPUT("OUT", "推送"),

	/**
	 * @fields CREATE: 创建(手动)
	 */
	CREATE("CREATE", "创建(手动)"),

	/**
	 * @fields UPDATE: 更新(手动)
	 */
	UPDATE("UPDATE", "更新(手动)"),

	/**
	 * @fields FEGION: 内部映射
	 */
	FEGION("FEGION", "内部映射");

	;

	/**
	 * @fields code: 编码
	 */
	private String code;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @title: RelateMode
	 * @author: zhongjyuan
	 * @description: 映射模式
	 * @param code 编码
	 * @param name 名称
	 * @throws
	 */
	RelateMode(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/**
	 * @title: getCode
	 * @author: zhongjyuan
	 * @description: 获取编码
	 * @return 编码
	 * @throws
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setCode
	 * @author: zhongjyuan
	 * @description: 设置编码
	 * @param code 编码
	 * @throws
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}
}
