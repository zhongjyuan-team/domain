package zhongjyuan.domain.relate;

import java.util.HashMap;
import java.util.Map;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;

/**
 * @className: RelateModel
 * @description: 关联对象
 * @author: zhongjyuan
 * @date: 2022年12月2日 上午11:23:46
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RelateModel extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	private String id;

	/**
	 * @fields type: 类型
	 */
	private String type;

	/**
	 * @fields typeName: 类型名称
	 */
	private String typeName;

	/**
	 * @fields mode: 模式
	 */
	private String mode;

	/**
	 * @fields modeName: 模式名称
	 */
	private String modeName;

	/**
	 * @fields sourceId: 来源主键
	 */
	private String sourceId;

	/**
	 * @fields sourceNo: 来源编号
	 */
	private String sourceNo;

	/**
	 * @fields sourceName: 来源名称
	 */
	private String sourceName;

	/**
	 * @fields targetId: 目标主键
	 */
	private String targetId;

	/**
	 * @fields targetNo: 目标编号
	 */
	private String targetNo;

	/**
	 * @fields targetName: 目标名称
	 */
	private String targetName;

	/**
	 * @fields businessId: 业务主键
	 */
	private String businessId;

	/**
	 * @fields businessNo: 业务编号
	 */
	private String businessNo;

	/**
	 * @fields businessName: 业务名称
	 */
	private String businessName;

	/**
	 * @fields success: 是否成功
	 */
	private Boolean success;

	/**
	 * @fields rowIndex: 行号
	 */
	private Integer rowIndex;

	/**
	 * @fields description: 描述
	 */
	private String description;

	/**
	 * @fields sourceExtends: 来源拓展属性Map集
	 */
	private Map<String, Object> sourceExtends;

	/**
	 * @fields targetExtends: 目标拓展属性Map集
	 */
	private Map<String, Object> targetExtends;

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @throws
	 */
	public RelateModel() {

	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @throws
	 */
	public RelateModel(String type, String businessNo) {
		setType(type);
		setBusinessNo(businessNo);
		setSuccess(true);
	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @param success 是否成功
	 * @throws
	 */
	public RelateModel(String type, String businessNo, Boolean success) {
		setType(type);
		setBusinessNo(businessNo);
		setSuccess(success);
	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @param sourceId 来源主键
	 * @throws
	 */
	public RelateModel(String type, String businessNo, String sourceId) {
		setType(type);
		setBusinessNo(businessNo);
		setSourceId(sourceId);
		setSuccess(true);
	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @param sourceId 来源主键
	 * @param success 是否成功
	 * @throws
	 */
	public RelateModel(String type, String businessNo, String sourceId, Boolean success) {
		setType(type);
		setBusinessNo(businessNo);
		setSourceId(sourceId);
		setSuccess(success);
	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @param sourceId 来源主键
	 * @param targetId 目标主键
	 * @throws
	 */
	public RelateModel(String type, String businessNo, String sourceId, String targetId) {
		setType(type);
		setBusinessNo(businessNo);
		setSourceId(sourceId);
		setTargetId(targetId);
		setSuccess(true);
	}

	/**
	 * @title Relate
	 * @author zhongjyuan
	 * @description 关联对象
	 * @param type 类型
	 * @param businessNo 业务编号
	 * @param sourceId 来源主键
	 * @param targetId 目标主键
	 * @param success 是否成功
	 * @throws
	 */
	public RelateModel(String type, String businessNo, String sourceId, String targetId, Boolean success) {
		setType(type);
		setBusinessNo(businessNo);
		setSourceId(sourceId);
		setTargetId(targetId);
		setSuccess(success);
	}

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getType
	 * @author: zhongjyuan
	 * @description: 获取类型
	 * @return 类型
	 * @throws
	 */
	public String getType() {
		return type;
	}

	/**
	 * @title: setType
	 * @author: zhongjyuan
	 * @description: 设置类型
	 * @param type 类型
	 * @throws
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @title: getTypeName
	 * @author: zhongjyuan
	 * @description: 获取类型名称
	 * @return 类型名称
	 * @throws
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @title: setTypeName
	 * @author: zhongjyuan
	 * @description: 设置类型名称
	 * @param typeName 类型名称
	 * @throws
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @title: getMode
	 * @author: zhongjyuan
	 * @description: 获取模式
	 * @return 模式
	 * @throws
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @title: setMode
	 * @author: zhongjyuan
	 * @description: 设置模式
	 * @param mode 模式
	 * @throws
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @title: getModeName
	 * @author: zhongjyuan
	 * @description: 获取模式名称
	 * @return 模式名称
	 * @throws
	 */
	public String getModeName() {
		return modeName;
	}

	/**
	 * @title: setModeName
	 * @author: zhongjyuan
	 * @description: 设置模式名称
	 * @param modeName 模式名称
	 * @throws
	 */
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	/**
	 * @title: getSourceId
	 * @author: zhongjyuan
	 * @description: 获取源唯一标识
	 * @return 源唯一标识
	 * @throws
	 */
	public String getSourceId() {
		return sourceId;
	}

	/**
	 * @title: setSourceId
	 * @author: zhongjyuan
	 * @description: 设置源唯一标识
	 * @param sourceId 源唯一标识
	 * @throws
	 */
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @title: getSourceNo
	 * @author: zhongjyuan
	 * @description: 获取源编号
	 * @return 源编号
	 * @throws
	 */
	public String getSourceNo() {
		return sourceNo;
	}

	/**
	 * @title: setSourceNo
	 * @author: zhongjyuan
	 * @description: 设置源编号
	 * @param sourceNo 源编号
	 * @throws
	 */
	public void setSourceNo(String sourceNo) {
		this.sourceNo = sourceNo;
	}

	/**
	 * @title: getSourceName
	 * @author: zhongjyuan
	 * @description: 获取源名称
	 * @return 源名称
	 * @throws
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @title: setSourceName
	 * @author: zhongjyuan
	 * @description: 设置源名称
	 * @param sourceName 源名称
	 * @throws
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @title: getTargetId
	 * @author: zhongjyuan
	 * @description: 获取目标唯一标识
	 * @return 目标唯一标识
	 * @throws
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * @title: setTargetId
	 * @author: zhongjyuan
	 * @description: 设置目标唯一标识
	 * @param targetId 目标唯一标识
	 * @throws
	 */
	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	/**
	 * @title: getTargetNo
	 * @author: zhongjyuan
	 * @description: 获取目标编号
	 * @return 目标编号
	 * @throws
	 */
	public String getTargetNo() {
		return targetNo;
	}

	/**
	 * @title: setTargetNo
	 * @author: zhongjyuan
	 * @description: 设置目标编号
	 * @param targetNo 目标编号
	 * @throws
	 */
	public void setTargetNo(String targetNo) {
		this.targetNo = targetNo;
	}

	/**
	 * @title: getTargetName
	 * @author: zhongjyuan
	 * @description: 获取目标名称
	 * @return 目标名称
	 * @throws
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * @title: setTargetName
	 * @author: zhongjyuan
	 * @description: 设置目标名称
	 * @param targetName 目标名称
	 * @throws
	 */
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	/**
	 * @title: getBusinessId
	 * @author: zhongjyuan
	 * @description: 获取业务唯一标识
	 * @return 业务唯一标识
	 * @throws
	 */
	public String getBusinessId() {
		return businessId;
	}

	/**
	 * @title: setBusinessId
	 * @author: zhongjyuan
	 * @description: 设置业务唯一标识
	 * @param businessId 业务唯一标识
	 * @throws
	 */
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	/**
	 * @title: getBusinessNo
	 * @author: zhongjyuan
	 * @description: 获取业务编号
	 * @return 业务编号
	 * @throws
	 */
	public String getBusinessNo() {
		return businessNo;
	}

	/**
	 * @title: setBusinessNo
	 * @author: zhongjyuan
	 * @description: 设置业务编号
	 * @param businessNo 业务编号
	 * @throws
	 */
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	/**
	 * @title: getBusinessName
	 * @author: zhongjyuan
	 * @description: 获取业务名称
	 * @return 业务名称
	 * @throws
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @title: setBusinessName
	 * @author: zhongjyuan
	 * @description: 设置业务名称
	 * @param businessName 业务名称
	 * @throws
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @title: getSuccess
	 * @author: zhongjyuan
	 * @description: 是否成功
	 * @return 是否成功
	 * @throws
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * @title: setSuccess
	 * @author: zhongjyuan
	 * @description: 设置是否成功
	 * @param success 是否成功
	 * @throws
	 */
	public void setSuccess(Boolean success) {
		this.success = success;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title: getDescription
	 * @author: zhongjyuan
	 * @description: 获取描述
	 * @return 描述
	 * @throws
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title: setDescription
	 * @author: zhongjyuan
	 * @description: 设置描述
	 * @param description 描述
	 * @throws
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @title: getSourceExtends
	 * @author: zhongjyuan
	 * @description: 获取源拓展属性Map集
	 * @return 源拓展属性Map集
	 * @throws
	 */
	public Map<String, Object> getSourceExtends() {
		return sourceExtends;
	}

	/**
	 * @title: getSourceExtendValue
	 * @author: zhongjyuan
	 * @description: 获取源拓展属性值
	 * @param key 键
	 * @return 源拓展属性值
	 * @throws
	 */
	public Object getSourceExtendValue(String key) {
		return sourceExtends.get(key);
	}

	/**
	 * @title: setSourceExtends
	 * @author: zhongjyuan
	 * @description: 设置源拓展属性Map集
	 * @param sourceExtends 源拓展属性Map集
	 * @throws
	 */
	public void setSourceExtends(Map<String, Object> sourceExtends) {
		this.sourceExtends = sourceExtends;
	}

	/**
	 * @title: setSourceExtends
	 * @author: zhongjyuan
	 * @description: 设置源拓展属性
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void setSourceExtends(String key, Object value) {
		if (this.sourceExtends == null || this.sourceExtends.size() < 1)
			this.sourceExtends = new HashMap<String, Object>();
		this.sourceExtends.put(key, value);
	}

	/**
	 * @title: getTargetExtends
	 * @author: zhongjyuan
	 * @description: 获取目标拓展属性Map集
	 * @return 目标拓展属性Map集
	 * @throws
	 */
	public Map<String, Object> getTargetExtends() {
		return targetExtends;
	}

	/**
	 * @title: getTargetExtendValue
	 * @author: zhongjyuan
	 * @description: 获取目标拓展属性值
	 * @param key 键
	 * @return 目标拓展属性值
	 * @throws
	 */
	public Object getTargetExtendValue(String key) {
		return targetExtends.get(key);
	}

	/**
	 * @title: setTargetExtends
	 * @author: zhongjyuan
	 * @description: 设置目标拓展属性Map集
	 * @param targetExtends 目标拓展属性Map集
	 * @throws
	 */
	public void setTargetExtends(Map<String, Object> targetExtends) {
		this.targetExtends = targetExtends;
	}

	/**
	 * @title: setTargetExtends
	 * @author: zhongjyuan
	 * @description: 设置目标拓展属性
	 * @param key 键
	 * @param value 值
	 * @throws
	 */
	public void setTargetExtends(String key, Object value) {
		if (this.targetExtends == null || this.targetExtends.size() < 1)
			this.targetExtends = new HashMap<String, Object>();
		this.targetExtends.put(key, value);
	}
}
