package zhongjyuan.domain.response;

import java.util.Collection;

import zhongjyuan.domain.IModel;

/**
 * @className: PageHeaderResult
 * @description: 动态分页结果对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午2:59:56
 * @param <T> 分页数据 - 泛型对象
 * @param <H> 分页表头 - 泛型对象
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PageHeaderResult<T extends IModel, H extends IModel> extends PageResult<T> {

	private static final long serialVersionUID = 1L;

	/**
	 * 表头
	 */
	protected H header;

	public PageHeaderResult() {

	}

	public PageHeaderResult(Collection<T> list) {
		this.list = list;
	}

	public PageHeaderResult(Collection<T> list, H header) {
		this.list = list;

		this.header = header;
	}

	public PageHeaderResult(Collection<T> list, H header, Long total) {
		this.list = list;

		this.header = header;

		this.total = total;
	}

	public PageHeaderResult(Collection<T> list, H header, Long total, Long size) {
		this.list = list;

		this.header = header;

		this.size = size;

		this.total = total;

		this.pages = this.total / this.size;
	}

	public PageHeaderResult(Collection<T> list, H header, Long total, Long size, Long index) {
		this.list = list;

		this.header = header;

		this.size = size;
		this.index = index;

		this.total = total;

		this.pages = this.total / this.size;
	}

	public PageHeaderResult<T, H> withHeader(H header) {
		this.header = header;
		return this;
	}

	public H getHeader() {
		return this.header;
	}
}
