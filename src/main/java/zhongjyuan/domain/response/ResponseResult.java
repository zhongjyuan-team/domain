package zhongjyuan.domain.response;

import java.text.MessageFormat;

import zhongjyuan.domain.AbstractModel;
import zhongjyuan.domain.IModel;
import zhongjyuan.domain.IResponseCode;
import zhongjyuan.domain.ResponseCode;

/**
 * @className: ResponseResult
 * @description: 响应结果类，用于封装接口返回结果。
 * @author: zhongjyuan
 * @date: 2023年11月20日 下午4:20:22
 * @param <T> 响应数据类型
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ResponseResult<T> extends AbstractModel implements IModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 响应时间
	 */
	protected Long time;

	/**
	 * 响应码
	 */
	protected Long code;

	/**
	 * 响应消息
	 */
	protected String message;

	/**
	 * 接口调用是否成功
	 */
	protected Boolean success;

	/**
	 * 响应数据
	 */
	protected T data;

	/**
	 * 状态码
	 */
	protected Integer status;

	/**
	 * 构造方法，初始化默认值
	 */
	public ResponseResult() {
		this.time = System.currentTimeMillis();
		this.success = false;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功
	 *
	 * @param success 接口调用是否成功
	 */
	public ResponseResult(Boolean success) {
		this.time = System.currentTimeMillis();
		this.success = success;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功和响应数据
	 *
	 * @param success 接口调用是否成功
	 * @param data    响应数据
	 */
	public ResponseResult(Boolean success, T data) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.data = data;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功和响应码、消息
	 *
	 * @param success      接口调用是否成功
	 * @param responseCode 响应码和消息
	 */
	public ResponseResult(Boolean success, IResponseCode responseCode) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.code = responseCode.getCode();
		this.message = responseCode.getMessage();
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功、响应码、消息
	 *
	 * @param success      接口调用是否成功
	 * @param responseCode 响应码和消息
	 * @param message      自定义消息
	 */
	public ResponseResult(Boolean success, IResponseCode responseCode, String message) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.code = responseCode.getCode();
		this.message = responseCode.getMessage();
		this.message = message;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功、响应码、消息、响应数据
	 *
	 * @param success      接口调用是否成功
	 * @param responseCode 响应码和消息
	 * @param data         响应数据
	 */
	public ResponseResult(Boolean success, IResponseCode responseCode, T data) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.code = responseCode.getCode();
		this.message = responseCode.getMessage();
		this.data = data;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功、响应码、消息
	 *
	 * @param success 接口调用是否成功
	 * @param code    响应码
	 * @param message 响应消息
	 */
	public ResponseResult(Boolean success, Long code, String message) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.code = code;
		this.message = message;
	}

	/**
	 * 构造方法，初始化默认值，并指定接口调用是否成功、响应码、消息、响应数据
	 *
	 * @param success 接口调用是否成功
	 * @param code    响应码
	 * @param message 响应消息
	 * @param data    响应数据
	 */
	public ResponseResult(Boolean success, Long code, String message, T data) {
		this.time = System.currentTimeMillis();
		this.success = success;
		this.code = code;
		this.message = message;
		this.data = data;
	}

	/**
	 * 设置响应码
	 *
	 * @param code 响应码
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withCode(Long code) {
		this.code = code;
		return this;
	}

	/**
	 * 设置响应消息
	 *
	 * @param message 响应消息
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withMessage(String message) {
		this.message = message;
		return this;
	}

	/**
	 * 设置接口调用是否成功
	 *
	 * @param success 接口调用是否成功
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withSuccess(Boolean success) {
		this.success = success;
		return this;
	}

	/**
	 * 设置响应数据
	 *
	 * @param data 响应数据
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withData(T data) {
		this.data = data;
		return this;
	}

	/**
	 * 设置状态码
	 *
	 * @param status 状态码
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withStatus(Integer status) {
		this.status = status;
		return this;
	}

	/**
	 * 设置响应码和消息
	 *
	 * @param responseCode 响应码和消息
	 * @return 当前ResponseResult对象
	 */
	public ResponseResult<T> withResponseCode(IResponseCode responseCode) {
		this.code = responseCode.getCode();
		this.message = responseCode.getMessage();
		return this;
	}

	/**
	 * 获取响应码。
	 *
	 * @return 响应码
	 */
	public Long getCode() {
		return this.code;
	}

	/**
	 * 获取响应消息。
	 *
	 * @return 响应消息
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * 判断接口调用是否成功。
	 *
	 * @return true表示成功，false表示失败
	 */
	public Boolean isSuccess() {
		return this.success;
	}

	/**
	 * 获取响应数据。
	 *
	 * @return 响应数据
	 */
	public T getData() {
		return this.data;
	}

	/**
	 * 构建一个成功的ResponseResult对象。
	 *
	 * @return 成功的ResponseResult对象
	 */
	public static ResponseResult<?> success() {
		return new ResponseResult<>(true, ResponseCode.SUCCEED);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定自定义响应码和消息。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @return 成功的ResponseResult对象
	 */
	public static ResponseResult<?> success(Long code, String message) {
		return new ResponseResult<>(true, code, message);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定响应码/消息。
	 *
	 * @param responseCode 响应码/消息
	 * @return 成功的ResponseResult对象
	 */
	public static ResponseResult<?> success(IResponseCode responseCode) {
		return new ResponseResult<>(true, responseCode);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定响应数据。
	 *
	 * @param data 响应数据
	 * @return 成功的ResponseResult对象
	 */
	public static <T> ResponseResult<T> success(T data) {
		return new ResponseResult<T>(true, ResponseCode.SUCCEED, data);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定自定义消息。
	 *
	 * @param message 自定义消息
	 * @return 成功的ResponseResult对象
	 */
	public static <T> ResponseResult<T> success(String message) {
		return new ResponseResult<T>(false, ResponseCode.SUCCEED, message);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定自定义响应码、消息和响应数据。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @param data    响应数据
	 * @return 成功的ResponseResult对象
	 */
	public static <T> ResponseResult<T> success(Long code, String message, T data) {
		return new ResponseResult<T>(true, code, message, data);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定响应码/消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param data         响应数据
	 * @return 成功的ResponseResult对象
	 */
	public static <T> ResponseResult<T> success(IResponseCode responseCode, T data) {
		return new ResponseResult<T>(true, responseCode, data);
	}

	/**
	 * 构建一个成功的ResponseResult对象，并指定响应码/消息、自定义消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param message      自定义消息
	 * @param data         响应数据
	 * @return 成功的ResponseResult对象
	 */
	public static <T> ResponseResult<T> success(IResponseCode responseCode, String message, T data) {
		return new ResponseResult<T>(false, responseCode.getCode(), message, data);
	}

	/**
	 * 构建一个失败的ResponseResult对象。
	 *
	 * @return 失败的ResponseResult对象
	 */
	public static ResponseResult<?> failure() {
		return new ResponseResult<>(false, ResponseCode.FAILURE);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定自定义响应码和消息。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @return 失败的ResponseResult对象
	 */
	public static ResponseResult<?> failure(Long code, String message) {
		return new ResponseResult<>(false, code, message);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定响应码/消息。
	 *
	 * @param responseCode 响应码/消息
	 * @return 失败的ResponseResult对象
	 */
	public static ResponseResult<?> failure(IResponseCode responseCode) {
		return new ResponseResult<>(false, responseCode);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定响应数据。
	 *
	 * @param data 响应数据
	 * @return 失败的ResponseResult对象
	 */
	public static <T> ResponseResult<T> failure(T data) {
		return new ResponseResult<T>(false, ResponseCode.FAILURE, data);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定自定义消息。
	 *
	 * @param message 自定义消息
	 * @return 失败的ResponseResult对象
	 */
	public static <T> ResponseResult<T> failure(String message) {
		return new ResponseResult<T>(false, ResponseCode.FAILURE, message);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定自定义响应码、消息和响应数据。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @param data    响应数据
	 * @return 失败的ResponseResult对象
	 */
	public static <T> ResponseResult<T> failure(Long code, String message, T data) {
		return new ResponseResult<T>(false, code, message, data);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定响应码/消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param data         响应数据
	 * @return 失败的ResponseResult对象
	 */
	public static <T> ResponseResult<T> failure(IResponseCode responseCode, T data) {
		return new ResponseResult<T>(false, responseCode, data);
	}

	/**
	 * 构建一个失败的ResponseResult对象，并指定响应码/消息、自定义消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param message      自定义消息
	 * @param data         响应数据
	 * @return 失败的ResponseResult对象
	 */
	public static <T> ResponseResult<T> failure(IResponseCode responseCode, String message, T data) {
		return new ResponseResult<T>(false, responseCode.getCode(), message, data);
	}

	/**
	 * 构建一个异常的ResponseResult对象。
	 *
	 * @return 异常的ResponseResult对象
	 */
	public static ResponseResult<?> error() {
		return new ResponseResult<>(false, ResponseCode.ERROR);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定自定义响应码和消息。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @return 异常的ResponseResult对象
	 */
	public static ResponseResult<?> error(Long code, String message) {
		return new ResponseResult<>(false, code, message);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定响应码/消息。
	 *
	 * @param responseCode 响应码/消息
	 * @return 异常的ResponseResult对象
	 */
	public static ResponseResult<?> error(IResponseCode responseCode) {
		return new ResponseResult<>(false, responseCode);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定响应数据。
	 *
	 * @param data 响应数据
	 * @return 异常的ResponseResult对象
	 */
	public static <T> ResponseResult<T> error(T data) {
		return new ResponseResult<T>(false, ResponseCode.ERROR, data);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定自定义消息。
	 *
	 * @param message 自定义消息
	 * @return 异常的ResponseResult对象
	 */
	public static <T> ResponseResult<T> error(String message) {
		return new ResponseResult<T>(false, ResponseCode.ERROR, message);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定自定义响应码、消息和响应数据。
	 *
	 * @param code    自定义响应码
	 * @param message 自定义消息
	 * @param data    响应数据
	 * @return 异常的ResponseResult对象
	 */
	public static <T> ResponseResult<T> error(Long code, String message, T data) {
		return new ResponseResult<T>(false, code, message, data);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定响应码/消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param data         响应数据
	 * @return 异常的ResponseResult对象
	 */
	public static <T> ResponseResult<T> error(IResponseCode responseCode, T data) {
		return new ResponseResult<T>(false, responseCode, data);
	}

	/**
	 * 构建一个异常的ResponseResult对象，并指定响应码/消息、自定义消息和响应数据。
	 *
	 * @param responseCode 响应码/消息
	 * @param message      自定义消息
	 * @param data         响应数据
	 * @return 异常的ResponseResult对象
	 */
	public static <T> ResponseResult<T> error(IResponseCode responseCode, String message, T data) {
		return new ResponseResult<T>(false, responseCode.getCode(), message, data);
	}

	@Override
	public String toString() {
		return MessageFormat.format("ResponseResult[time: {0} | success: {1} | code: {2} | message: {3}]", this.time, this.success, this.code, this.message);
	}
}
