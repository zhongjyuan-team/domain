package zhongjyuan.domain.response;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;

import zhongjyuan.domain.IModel;

/**
 * @className: PageResult
 * @description: 分页结果对象
 * @author: zhongjyuan
 * @date: 2022年6月10日 下午2:58:11
 * @param <T> 分页数据 - 泛型对象
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PageResult<T extends IModel> implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 当前页码，从1开始
	 */
	protected Long index;

	/**
	 * 每页数据数量
	 */
	protected Long size;

	/**
	 * 总页数
	 */
	protected Long pages;

	/**
	 * 总数据量
	 */
	protected Long total;

	/**
	 * 当前页数据列表
	 */
	protected Collection<T> list;

	/**
	 * 当前页第一条数据在整个数据集中的行号（从0开始）
	 */
	protected long startRow;

	/**
	 * 当前页最后一条数据在整个数据集中的行号（从0开始）
	 */
	protected long endRow;

	/**
	 * 上一页页码
	 */
	protected int prePage;

	/**
	 * 下一页页码
	 */
	protected int postPage;

	/**
	 * 是否为第一页
	 */
	protected boolean firstPage;

	/**
	 * 是否为最后一页
	 */
	protected boolean lastPage;

	/**
	 * 是否有上一页
	 */
	protected boolean hasPrePage;

	/**
	 * 是否有下一页
	 */
	protected boolean hasPostPage;

	/**
	 * 导航页数目
	 */
	protected int navigatePages;

	/**
	 * 所有导航页号
	 */
	protected int[] navigatePageNums;

	/**
	 * 航条上的第一页页码
	 */
	protected int navigateFirstPage;

	/**
	 * 导航条上的最后一页页码
	 */
	protected int navigateLastPage;

	public PageResult() {

	}

	public PageResult(Collection<T> list) {
		super();

		this.list = list;
	}

	public PageResult(Collection<T> list, Long total) {
		super();

		this.list = list;

		this.total = total;
	}

	public PageResult(Collection<T> list, Long total, Long size) {
		super();

		this.list = list;

		this.size = size;

		this.total = total;

		this.pages = this.total / this.size;
	}

	public PageResult(Collection<T> list, Long total, Long size, Long index) {
		super();

		this.list = list;

		this.size = size;
		this.index = index;

		this.total = total;

		this.pages = (this.total < this.size ? 1 : (long) Math.ceil(this.total / this.size));
	}

	public PageResult<T> withIndex(Long index) {
		this.index = index;
		return this;
	}

	public PageResult<T> withSize(Long size) {
		this.size = size;
		return this;
	}

	public PageResult<T> withTotal(Long total) {
		this.total = total;
		return this;
	}

	public PageResult<T> withPages(Long pages) {
		this.pages = pages;
		return this;
	}

	public PageResult<T> withList(Collection<T> list) {
		this.list = list;
		return this;
	}

	public Long getIndex() {
		return this.index;
	}

	public Long getSize() {
		return this.size;
	}

	public Long getPages() {
		return this.pages;
	}

	public Long getTotal() {
		return this.total;
	}

	public Collection<T> getList() {
		return this.list;
	}

	/**
	 * @title: getStartRow
	 * @author: zhongjyuan
	 * @description: 当前页第一条数据在整个数据集中的行号（从0开始）
	 * @return: 当前页第一条数据在整个数据集中的行号（从0开始）
	 * @throws
	 */
	public long getStartRow() {
		return startRow;
	}

	/**
	 * @title: setStartRow
	 * @author: zhongjyuan
	 * @description: 当前页第一条数据在整个数据集中的行号（从0开始）
	 * @param: startRow 当前页第一条数据在整个数据集中的行号（从0开始）
	 * @throws
	 */
	public void setStartRow(long startRow) {
		this.startRow = startRow;
	}

	/**
	 * @title: getEndRow
	 * @author: zhongjyuan
	 * @description: 当前页最后一条数据在整个数据集中的行号（从0开始）
	 * @return: 当前页最后一条数据在整个数据集中的行号（从0开始）
	 * @throws
	 */
	public long getEndRow() {
		return endRow;
	}

	/**
	 * @title: setEndRow
	 * @author: zhongjyuan
	 * @description: 当前页最后一条数据在整个数据集中的行号（从0开始）
	 * @param: endRow 当前页最后一条数据在整个数据集中的行号（从0开始）
	 * @throws
	 */
	public void setEndRow(long endRow) {
		this.endRow = endRow;
	}

	/**
	 * @title: getPrePage
	 * @author: zhongjyuan
	 * @description: 上一页页码
	 * @return: 上一页页码
	 * @throws
	 */
	public int getPrePage() {
		return prePage;
	}

	/**
	 * @title: setPrePage
	 * @author: zhongjyuan
	 * @description: 上一页页码
	 * @param: prePage 上一页页码
	 * @throws
	 */
	public void setPrePage(int prePage) {
		this.prePage = prePage;
	}

	/**
	 * @title: getPostPage
	 * @author: zhongjyuan
	 * @description: 下一页页码
	 * @return: 下一页页码
	 * @throws
	 */
	public int getPostPage() {
		return postPage;
	}

	/**
	 * @title: setPostPage
	 * @author: zhongjyuan
	 * @description: 下一页页码
	 * @param: postPage 下一页页码
	 * @throws
	 */
	public void setPostPage(int postPage) {
		this.postPage = postPage;
	}

	/**
	 * @title: isFirstPage
	 * @author: zhongjyuan
	 * @description: 是否为第一页
	 * @return: 是否为第一页
	 * @throws
	 */
	public boolean isFirstPage() {
		return firstPage;
	}

	/**
	 * @title: setFirstPage
	 * @author: zhongjyuan
	 * @description: 是否为第一页
	 * @param: firstPage 是否为第一页
	 * @throws
	 */
	public void setFirstPage(boolean firstPage) {
		this.firstPage = firstPage;
	}

	/**
	 * @title: isLastPage
	 * @author: zhongjyuan
	 * @description: 是否为最后一页
	 * @return: 是否为最后一页
	 * @throws
	 */
	public boolean isLastPage() {
		return lastPage;
	}

	/**
	 * @title: setLastPage
	 * @author: zhongjyuan
	 * @description: 是否为最后一页
	 * @param: lastPage 是否为最后一页
	 * @throws
	 */
	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}

	/**
	 * @title: isHasPrePage
	 * @author: zhongjyuan
	 * @description: 是否有上一页
	 * @return: 是否有上一页
	 * @throws
	 */
	public boolean isHasPrePage() {
		return hasPrePage;
	}

	/**
	 * @title: setHasPrePage
	 * @author: zhongjyuan
	 * @description: 是否有上一页
	 * @param: hasPrePage 是否有上一页
	 * @throws
	 */
	public void setHasPrePage(boolean hasPrePage) {
		this.hasPrePage = hasPrePage;
	}

	/**
	 * @title: isHasPostPage
	 * @author: zhongjyuan
	 * @description: 是否有下一页
	 * @return: 是否有下一页
	 * @throws
	 */
	public boolean isHasPostPage() {
		return hasPostPage;
	}

	/**
	 * @title: setHasPostPage
	 * @author: zhongjyuan
	 * @description: 是否有下一页
	 * @param: hasPostPage 是否有下一页
	 * @throws
	 */
	public void setHasPostPage(boolean hasPostPage) {
		this.hasPostPage = hasPostPage;
	}

	/**
	 * @title: getNavigatePages
	 * @author: zhongjyuan
	 * @description: 导航页数目
	 * @return: 导航页数目
	 * @throws
	 */
	public int getNavigatePages() {
		return navigatePages;
	}

	/**
	 * @title: setNavigatePages
	 * @author: zhongjyuan
	 * @description: 导航页数目
	 * @param: navigatePages 导航页数目
	 * @throws
	 */
	public void setNavigatePages(int navigatePages) {
		this.navigatePages = navigatePages;
	}

	/**
	 * @title: getNavigatepageNums
	 * @author: zhongjyuan
	 * @description: 所有导航页号
	 * @return: 所有导航页号
	 * @throws
	 */
	public int[] getNavigatepageNums() {
		return navigatePageNums;
	}

	/**
	 * @title: setNavigatepageNums
	 * @author: zhongjyuan
	 * @description: 所有导航页号
	 * @param: navigatePageNums 所有导航页号
	 * @throws
	 */
	public void setNavigatepageNums(int[] navigatePageNums) {
		this.navigatePageNums = navigatePageNums;
	}

	/**
	 * @title: getNavigateFirstPage
	 * @author: zhongjyuan
	 * @description: 航条上的第一页页码
	 * @return: 航条上的第一页页码
	 * @throws
	 */
	public int getNavigateFirstPage() {
		return navigateFirstPage;
	}

	/**
	 * @title: setNavigateFirstPage
	 * @author: zhongjyuan
	 * @description: 航条上的第一页页码
	 * @param: navigateFirstPage 航条上的第一页页码
	 * @throws
	 */
	public void setNavigateFirstPage(int navigateFirstPage) {
		this.navigateFirstPage = navigateFirstPage;
	}

	/**
	 * @title: getNavigateLastPage
	 * @author: zhongjyuan
	 * @description: 导航条上的最后一页页码
	 * @return: 导航条上的最后一页页码
	 * @throws
	 */
	public int getNavigateLastPage() {
		return navigateLastPage;
	}

	/**
	 * @title: setNavigateLastPage
	 * @author: zhongjyuan
	 * @description: 导航条上的最后一页页码
	 * @param: navigateLastPage 导航条上的最后一页页码
	 * @throws
	 */
	public void setNavigateLastPage(int navigateLastPage) {
		this.navigateLastPage = navigateLastPage;
	}

	@Override
	public String toString() {
		return MessageFormat.format("PageResult[index: {0} | size: {1} | pages: {2} | total: {3}]", this.index, this.size, this.pages, this.total);
	}
}
