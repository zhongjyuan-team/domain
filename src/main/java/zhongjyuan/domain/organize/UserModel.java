package zhongjyuan.domain.organize;

import java.time.LocalDateTime;

import zhongjyuan.domain.AbstractAttributeModel;
import zhongjyuan.domain.IAttributeModel;

/**
 * @className: UserModel
 * @description: 用户模型对象
 * @author: zhongjyuan
 * @date: 2022年11月22日 下午3:23:38
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class UserModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;

	/**
	 * @fields id: 主键
	 */
	private String id;

	/**
	 * @fields name: 名称
	 */
	private String name;

	/**
	 * @fields account: 账号
	 */
	private String account;

	/**
	 * @fields aliasName: 别名
	 */
	private String aliasName;

	/**
	 * @fields sex: 性别
	 */
	private String sex;

	/**
	 * @fields icon: 图标
	 */
	private String icon;

	/**
	 * @fields email: 邮箱
	 */
	private String email;

	/**
	 * @fields mobile: 手机号
	 */
	private String mobile;

	/**
	 * @fields telephone: 电话号
	 */
	private String telephone;

	/**
	 * @fields employeeId: 员工主键
	 */
	private String employeeId;

	/**
	 * @fields employeeNo: 员工编号
	 */
	private String employeeNo;

	/**
	 * @fields employeeName: 员工名称
	 */
	private String employeeName;

	/**
	 * @fields creatorId: 创建主键
	 */
	private String creatorId;

	/**
	 * @fields creatorName: 创建名称
	 */
	private String creatorName;

	/**
	 * @fields createTime: 创建时间
	 */
	private LocalDateTime createTime;

	/**
	 * @fields rowIndex: 行号
	 */
	private Integer rowIndex;

	/**
	 * @title: getId
	 * @author: zhongjyuan
	 * @description: 获取唯一标识
	 * @return 唯一标识
	 * @throws
	 */
	public String getId() {
		return id;
	}

	/**
	 * @title: setId
	 * @author: zhongjyuan
	 * @description: 设置唯一标识
	 * @param id 唯一标识
	 * @throws
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @title: getName
	 * @author: zhongjyuan
	 * @description: 获取名称
	 * @return 名称
	 * @throws
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title: setName
	 * @author: zhongjyuan
	 * @description: 设置名称
	 * @param name 名称
	 * @throws
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title: getAccount
	 * @author: zhongjyuan
	 * @description: 获取账号
	 * @return 账号
	 * @throws
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @title: setAccount
	 * @author: zhongjyuan
	 * @description: 设置账号
	 * @param account 账号
	 * @throws
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @title: getAliasName
	 * @author: zhongjyuan
	 * @description: 获取别名
	 * @return 别名
	 * @throws
	 */
	public String getAliasName() {
		return aliasName;
	}

	/**
	 * @title: setAliasName
	 * @author: zhongjyuan
	 * @description: 设置别名
	 * @param aliasName 别名
	 * @throws
	 */
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	/**
	 * @title: getSex
	 * @author: zhongjyuan
	 * @description: 获取性别
	 * @return 性别
	 * @throws
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @title: setSex
	 * @author: zhongjyuan
	 * @description: 设置性别
	 * @param sex 性别
	 * @throws
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @title: getIcon
	 * @author: zhongjyuan
	 * @description: 获取头像
	 * @return 头像
	 * @throws
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @title: setIcon
	 * @author: zhongjyuan
	 * @description: 设置头像
	 * @param icon 头像
	 * @throws
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @title: getEmail
	 * @author: zhongjyuan
	 * @description: 获取邮箱
	 * @return 邮箱
	 * @throws
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @title: setEmail
	 * @author: zhongjyuan
	 * @description: 设置邮箱
	 * @param email 邮箱
	 * @throws
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @title: getMobile
	 * @author: zhongjyuan
	 * @description: 获取手机号
	 * @return 手机号
	 * @throws
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @title: setMobile
	 * @author: zhongjyuan
	 * @description: 设置手机号
	 * @param mobile 手机号
	 * @throws
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @title: getTelephone
	 * @author: zhongjyuan
	 * @description: 获取电话号
	 * @return 电话号
	 * @throws
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @title: setTelephone
	 * @author: zhongjyuan
	 * @description: 设置电话号
	 * @param telephone 电话号
	 * @throws
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @title: getEmployeeId
	 * @author: zhongjyuan
	 * @description: 获取员工唯一标识
	 * @return 员工唯一标识
	 * @throws
	 */
	public String getEmployeeId() {
		return employeeId;
	}

	/**
	 * @title: setEmployeeId
	 * @author: zhongjyuan
	 * @description: 设置员工唯一标识
	 * @param employeeId 员工唯一标识
	 * @throws
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @title: getEmployeeNo
	 * @author: zhongjyuan
	 * @description: 获取员工编号
	 * @return 员工编号
	 * @throws
	 */
	public String getEmployeeNo() {
		return employeeNo;
	}

	/**
	 * @title: setEmployeeNo
	 * @author: zhongjyuan
	 * @description: 设置员工编号
	 * @param employeeNo 员工编号
	 * @throws
	 */
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	/**
	 * @title: getEmployeeName
	 * @author: zhongjyuan
	 * @description: 获取员工名称
	 * @return 员工名称
	 * @throws
	 */
	public String getEmployeeName() {
		return employeeName;
	}

	/**
	 * @title: setEmployeeName
	 * @author: zhongjyuan
	 * @description: 设置员工名称
	 * @param employeeName 员工名称
	 * @throws
	 */
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	/**
	 * @title: getCreatorId
	 * @author: zhongjyuan
	 * @description: 获取创建者唯一标识
	 * @return 创建者唯一标识
	 * @throws
	 */
	public String getCreatorId() {
		return creatorId;
	}

	/**
	 * @title: setCreatorId
	 * @author: zhongjyuan
	 * @description: 设置创建者唯一标识
	 * @param creatorId 创建者唯一标识
	 * @throws
	 */
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	/**
	 * @title: getCreatorName
	 * @author: zhongjyuan
	 * @description: 获取创建者名称
	 * @return 创建者名称
	 * @throws
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @title: setCreatorName
	 * @author: zhongjyuan
	 * @description: 设置创建者名称
	 * @param creatorName 创建者名称
	 * @throws
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @title: getCreateTime
	 * @author: zhongjyuan
	 * @description: 获取创建时间
	 * @return 创建时间
	 * @throws
	 */
	public LocalDateTime getCreateTime() {
		return createTime;
	}

	/**
	 * @title: setCreateTime
	 * @author: zhongjyuan
	 * @description: 设置创建时间
	 * @param createTime 创建时间
	 * @throws
	 */
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	/**
	 * @title: getRowIndex
	 * @author: zhongjyuan
	 * @description: 获取行号
	 * @return 行号
	 * @throws
	 */
	public Integer getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title: setRowIndex
	 * @author: zhongjyuan
	 * @description: 设置行号
	 * @param rowIndex 行号
	 * @throws
	 */
	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}
}
