package zhongjyuan.domain.organize;

import zhongjyuan.domain.AbstractAttributeModel;
import zhongjyuan.domain.IAttributeModel;

/**
 * @className: RoleModel
 * @description: 角色模型对象
 * @author: zhongjyuan
 * @date: 2023年10月11日 下午3:41:38
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class RoleModel extends AbstractAttributeModel implements IAttributeModel {

	private static final long serialVersionUID = 1L;
}
